#!/bin/sh

e2e_test() {
    sudo docker build ../coturn/ -t coturn
    sudo docker run --net=host --name=coturn -t coturn &
    dotnet run --project src/ &
    serverPID=$!
    sleep 3
    dotnet test tests/E2ETests/
    sudo docker rm -f coturn
    kill $serverPID
}

integration_test() {
    sudo docker build ../coturn/ -t coturn
    sudo docker run --net=host --name=coturn -t coturn &
    sleep 3
    dotnet test tests/IntegrationTests/
    sudo docker rm -f coturn
}

unit_test() {
    dotnet test tests/UnitTests/
}


if [ $# -eq 0 ];  then
    echo "\033[31mNo test selected.\033[0m"
    exit 1
elif [ "$1" = "all" ]; then
    if [ "$(id -u)" -ne 0 ]; then
        echo "\033[31mWhile using parameter all, please run script as root or as sudo.\033[0m"
        exit 1
    else
        unit_test
        integration_test
        e2e_test
    fi
elif [ "$1" = "e2e" ]; then
    if [ "$(id -u)" -ne 0 ]; then
        echo "\033[31mWhile using parameter e2e, please run script as root or as sudo.\033[0m"
        exit 1
    else
        e2e_test
    fi   
elif [ "$1" = "integration" ]; then
    if [ "$(id -u)" -ne 0 ]; then
        echo "\033[31mWhile using parameter integration, please run script as root or as sudo.\033[0m"
        exit 1
    else
        integration_test
    fi   
elif [ "$1" = "unit" ]; then
    unit_test
else
    echo "\033[31mWrong argument.\033[0m\nValid arguments are: \033[33mall e2e integration unit\033[0m"
    exit 1
fi