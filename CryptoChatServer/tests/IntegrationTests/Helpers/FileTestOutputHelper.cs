using System;
using System.IO;
using Xunit.Abstractions;

namespace IntegrationTests.Helpers;
public class FileTestOutputHelper : ITestOutputHelper
{
    private readonly string _filePath;

    public FileTestOutputHelper(string filePath)
    {
        _filePath = filePath;
    }

    public void WriteLine(string message)
    {
        File.AppendAllText(_filePath, message + Environment.NewLine);
    }

    public void WriteLine(string format, params object[] args)
    {
        File.AppendAllText(_filePath, string.Format(format, args) + Environment.NewLine);
    }
}