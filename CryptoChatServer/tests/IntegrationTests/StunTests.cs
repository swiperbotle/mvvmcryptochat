using StunTurnClientLibrary;
using Xunit;
namespace IntegrationTests.Tests
{
    public class StunTests
    {
        string _serverIp;
        string _clientIp;
        int _clientPort;
        int _serverPort;

        public StunTests(string? serverIp = null, string? clientIp = null)
        {
            _serverIp = serverIp ?? "127.0.0.1";
            _clientIp = clientIp ?? "0.0.0.0";
            _clientPort = 12345;
            _serverPort = 3478;
        }

        [Fact]
        public void StunServerTest1()
        {
            var stunClient = new StunClient(_clientIp, _clientPort, isFingerprint:true);
            var result = stunClient.StunRequest(_serverIp, (uint)_serverPort);
            Assert.Equal((uint)_clientPort, result.Item3);
        }
    }
}