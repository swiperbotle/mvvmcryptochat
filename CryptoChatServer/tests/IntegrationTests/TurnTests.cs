using System.Net;
using System.Net.Sockets;
using System.Runtime.ExceptionServices;
using System.Text;
using StunTurnClientLibrary;
using Xunit;
using Xunit.Abstractions;
using IntegrationTests.Helpers;

namespace IntegrationTests.Tests
{
    public class TurnTests
    {
        string _serverIp;
        uint _serverPort;
        string _clientIp;
        TurnClient _turnClient1;
        TurnClient _turnClient2;
        int _channelNumber;
        string _username;
        string _password;
        string _sendMsg;
        private ManualResetEvent _threadStartedEvent;
        private readonly FileTestOutputHelper _output;
        private readonly string _logFilePath = "test-output.log";
        public TurnTests(string? serverIp = null, string? clientIp = null)
        {
            _output = new FileTestOutputHelper(_logFilePath);
            _threadStartedEvent = new ManualResetEvent(false);
            _channelNumber = 0x4004;
            _username = "1709740831";
            _password = "MTIzNDU2Nzg5MA==";
            _sendMsg = "send";
            _serverIp = serverIp ?? "127.0.0.1";
            _clientIp = clientIp ?? "0.0.0.0";
            _serverPort = 3478;
            _turnClient1 = new TurnClient(_clientIp, 12345, password: _password, username: _username,
                                      isFingerprint: true);
            _turnClient2 = new TurnClient(_clientIp, 12346, password: _password, username: _username);
        }

        [Fact]
        public void TurnServerTest1()
        {
            SetUp();
            ExceptionDispatchInfo? capturedException = null;
            try
            {
                // Send();
                ChannelSend();
            }
            catch (Exception ex)
            {
                capturedException = ExceptionDispatchInfo.Capture(ex);
            }
            finally
            {
                capturedException?.Throw();
                Stop();
            }
        }
        void SetUp()
        {
            _turnClient1.TurnAllocateRequest(_serverIp, _serverPort, lifetime: 777, evenPort: true, dontFragment: true);
            _turnClient2.TurnAllocateRequest(_serverIp, _serverPort);
        }
        void Send()
        {
            var sendMsg = _sendMsg + "end";
            _turnClient1.TurnCreatePermission(_serverIp, _serverPort, _turnClient1.TransId, _turnClient2.RelayedIp, _turnClient2.RelayedPort);
            _turnClient2.TurnCreatePermission(_serverIp, _serverPort, _turnClient2.TransId, _turnClient1.RelayedIp, _turnClient1.RelayedPort);
            var receiveThread = new Thread(() => { ReceiveDataFromUdpPort(12346, _sendMsg); })
            {
                IsBackground = true
            };
            receiveThread.Start();
            _threadStartedEvent.WaitOne();
            _turnClient1.TurnSend(_serverIp, _serverPort, _turnClient1.TransId, _turnClient2.RelayedIp, _turnClient2.RelayedPort,
                                  Encoding.UTF8.GetBytes(sendMsg));
            receiveThread.Join();
        }
        void ChannelSend()
        {
            var sendMsg = _sendMsg + "end";
            _turnClient1.TurnCreatePermission(_serverIp, _serverPort, _turnClient1.TransId, _turnClient2.RelayedIp, _turnClient2.RelayedPort);
            _turnClient2.TurnCreatePermission(_serverIp, _serverPort, _turnClient2.TransId, _turnClient1.RelayedIp, _turnClient1.RelayedPort);
            _turnClient1.TurnChannelBind(_serverIp, _serverPort, _turnClient1.TransId, _turnClient2.RelayedIp,
                                  _turnClient2.RelayedPort, _channelNumber);
            _output.WriteLine("End Channel Bind");
            var receiveThread = new Thread(() => { ReceiveDataFromUdpPort(12346, _sendMsg); })
            {
                IsBackground = true
            };
            receiveThread.Start();
            _threadStartedEvent.WaitOne();
            _output.WriteLine("Start channel send");
            _turnClient1.TurnChannelSend(_serverIp, (int)_serverPort, _channelNumber, Encoding.UTF8.GetBytes(sendMsg));
            _output.WriteLine("End channel send");
            receiveThread.Join();
        }
        void Stop()
        {
            _turnClient1.TurnDeleteAllocation(_serverIp, _serverPort, _turnClient1.TransId!);
            _turnClient2.TurnDeleteAllocation(_serverIp, _serverPort, _turnClient2.TransId!);
        }

        byte[] ReceiveDataFromUdpPort(int port, string expectedMsg, int bufferSize = 1024)
        {
            byte[] receivingData = Array.Empty<byte>();
            var localEndpoint = new IPEndPoint(IPAddress.Parse(_clientIp), port);
            using (var udpSocket = new UdpClient(localEndpoint))
            {
                var endPoint = new IPEndPoint(IPAddress.Any, 0);
                _threadStartedEvent.Set();
                _output.WriteLine("Start receiving");
                var receivedData = udpSocket.Receive(ref endPoint);
                _output.WriteLine("End receiving");
                receivingData = receivingData.Concat(TurnClient.ParseData(receivedData)).ToArray();
                receivingData = RemoveTrailingNullBytes(receivingData);
                Assert.True(EndsWith(receivingData, new byte[] { 101, 110, 100 }));
                Assert.Equal(expectedMsg, ToString(receivingData[..^3]));
                return receivingData;
            }
        }
        byte[] RemoveTrailingNullBytes(byte[] data)
        {
            int lastNonNullIndex = data.Length;
            for (int i = data.Length - 1; i >= 0; i--)
            {
                if (data[i] != 0)
                {
                    break;
                }
                lastNonNullIndex = i;
            }
            return data[..lastNonNullIndex];
        }
        private bool EndsWith(byte[] array, byte[] suffix)
        {
            if (array.Length < suffix.Length)
                return false;
            for (int i = 0; i < suffix.Length; i++)
            {
                if (array[array.Length - suffix.Length + i] != suffix[i])
                    return false;
            }
            return true;
        }
        private string ToString(byte[] data)
        {
            return System.Text.Encoding.UTF8.GetString(data);
        }
    }
}