using Xunit;
using System.Diagnostics;
using System.Net.WebSockets;
using WebSocketServer.Helpers;
using E2ETests.Helpers;
using WebSocketServer.Enums;
using WebSocketServer.Models;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using System.Text.Json;
using System.Text;
using System.Runtime.ExceptionServices;
using E2ETests.Models;
using E2ETests.Fixtures;

namespace E2ETests.Tests;
[Collection("WebSocket collection")]
public class E2ETests : IClassFixture<WebSocketFixture>
{
    private readonly WebSocketFixture _wsFixture;
    public E2ETests(WebSocketFixture wsFixture)
    {
        _wsFixture = wsFixture;
    }

    [Fact]
    public async Task P2PConnectionTest()
    {
        var ws1 = new ClientWebSocket();
        var ws2 = new ClientWebSocket();
        CertificateHelper.InjectCertificate(ws1);
        CertificateHelper.InjectCertificate(ws2);
        var signalingServerIp = _wsFixture.SignalingServerIp;
        var signalinServerPort = _wsFixture.SignalingServerPort;
        Assert.Equal(WebSocketState.None, ws1.State);
        Assert.Equal(WebSocketState.None, ws2.State);
        await ws1.ConnectAsync(new Uri($"wss://{signalingServerIp}:{signalinServerPort}/ws"), CancellationToken.None);
        await ws2.ConnectAsync(new Uri($"wss://{signalingServerIp}:{signalinServerPort}/ws"), CancellationToken.None);
        var userName1 = _wsFixture.Username1;
        var userName2 = _wsFixture.Username2;
        var userPass1 = _wsFixture.UserPass1;
        var userPass2 = _wsFixture.UserPass2;
        var userModel1 = await LogIn(ws1, userName1, userPass1);
        var userModel2 = await LogIn(ws2, userName2, userPass2);
        ExceptionDispatchInfo? capturedException = null;
        try
        {
            var msgText = "hello";
            var msgId = (long)await CreateMessageId(ws1, userModel1.Id);
            var msg = new MessageModel { MessageText = msgText, MessageId = msgId };
            var respTask = RequestHelper.ConversationResponse(ws2);
            var reqTask = RequestHelper.ConversationRequest(ws1, userModel1.Id, userModel2.Id, msg);
            await reqTask;
            var respRes = await respTask;
            Assert.Equal(msgText, respRes.MessageText);
            Assert.Equal(msgId, respRes.MessageId);
        }
        catch (Exception ex)
        {
            capturedException = ExceptionDispatchInfo.Capture(ex);
        }
        finally
        {
            await LogOut(ws1, userModel1);
            await LogOut(ws2, userModel2);
        }
        capturedException?.Throw();
    }

    [Fact]
    public async Task UpdateUserAndSearchUserTest()
    {
        var ws1 = new ClientWebSocket();
        var ws2 = new ClientWebSocket();
        CertificateHelper.InjectCertificate(ws1);
        CertificateHelper.InjectCertificate(ws2);
        var signalingServerIp = _wsFixture.SignalingServerIp;
        var signalinServerPort = _wsFixture.SignalingServerPort;
        Assert.Equal(WebSocketState.None, ws1.State);
        Assert.Equal(WebSocketState.None, ws2.State);
        await ws1.ConnectAsync(new Uri($"wss://{signalingServerIp}:{signalinServerPort}/ws"), CancellationToken.None);
        await ws2.ConnectAsync(new Uri($"wss://{signalingServerIp}:{signalinServerPort}/ws"), CancellationToken.None);
        var userName1 = _wsFixture.Username1;
        var userName2 = _wsFixture.Username2;
        var userPass1 = _wsFixture.UserPass1;
        var userPass2 = _wsFixture.UserPass2;
        var userModel1 = await LogIn(ws1, userName1, userPass1);
        var userModel2 = await LogIn(ws2, userName2, userPass2);
        ExceptionDispatchInfo? capturedException = null;
        try
        {
            var updatedUsername = "Updated";
            var updatedUserModel1 = userModel1;
            updatedUserModel1.Username = updatedUsername;
            await UpdateUser(ws1, updatedUserModel1);
            var newUserModel1 = await SearchUser(ws2, userModel2.Id, updatedUsername);
            Assert.Equal(updatedUsername, newUserModel1.Username);
        }
        catch (Exception ex)
        {
            capturedException = ExceptionDispatchInfo.Capture(ex);
        }
        finally
        {
            await LogOut(ws1, userModel1);
            await LogOut(ws2, userModel2);
        }
        capturedException?.Throw();
    }


    static async Task<UserModel> LogIn(ClientWebSocket ws, string username, string password)
    {
        var userModel = new UserModel
        {
            Id = Guid.NewGuid(),
            Username = username.Substring(0, 8),
            Password = new HashPasswordHelper().HashPassword(password).Substring(0, 8),
            Nickname = "randomNick",
            Bio = null,
            Avatar = "b64avatar",
            IsPrivate = false,
        };
        var response = await RequestHelper.SignUpRequest(ws, userModel);
        Assert.False(CompareTypeFields(response[..4], (int)MessageType.Error_Response));
        Array.Clear(response, 0, response.Length);
        response = await RequestHelper.SignInRequest(ws, username, new HashPasswordHelper().HashPassword(password));
        Assert.False(CompareTypeFields(response[..4], (int)MessageType.Error_Response));
        return JsonSerializer.Deserialize<UserModel>(Encoding.UTF8.GetString(response[4..]))!;
    }

    static async Task LogOut(ClientWebSocket ws, UserModel userModel)
    {
        var response = await RequestHelper.RemoveRequest(ws, userModel.Id);
        Assert.False(CompareTypeFields(response[..4], (int)MessageType.Error_Response));
        Assert.True(ws.State == WebSocketState.Open);
        await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None);
    }

    static async Task<Int64?> CreateMessageId(ClientWebSocket clientWebSocket, Guid userId)
    {
        var response = await RequestHelper.GetMessageIdRequest(clientWebSocket, userId);
        Assert.Equal((int)MessageType.Get_MessageId_Success, BitConverter.ToInt32(response[..4], 0));
        return BitConverter.ToInt64(response[4..12]);
    }

    static async Task UpdateUser(ClientWebSocket clientWebSocket, UserModel userModel)
    {
        var response = await RequestHelper.UpdateRequest(clientWebSocket, userModel);
        Assert.Equal((int)MessageType.Update_Success_Response, BitConverter.ToInt32(response[..4]));
    }

    static async Task<UserModel> SearchUser(ClientWebSocket clientWebSocket, Guid userId, string username)
    {
        var response = await RequestHelper.SearchRequest(clientWebSocket, userId, username);
        Assert.Equal((int)MessageType.Search_Success_Response, BitConverter.ToInt32(response[..4]));
        return JsonSerializer.Deserialize<UserModel>(Encoding.UTF8.GetString(response[4..]))!;
    }


    static bool CompareTypeFields(byte[] givenType, int expectedType)
    {
        if (givenType.Length != 4)
            throw new ArgumentException("Wrong length of first argument byte[] giveType. Expected 4 byte array.");
        if (BitConverter.ToInt32(givenType) == expectedType)
            return true;
        return false;
    }
}