using System.Net.WebSockets;
using E2ETests.Helpers;
using Xunit;

namespace E2ETests.Fixtures;

public class WebSocketFixture : IDisposable
{
    public string StunServerIp { get; private set; }
    public string SignalingServerIp { get; private set; }
    public int SignalingServerPort { get; private set; }
    public string ClientIp { get; private set; }
    public int ClientPort1 { get; private set; }
    public int ClientPort2 { get; private set; }
    public string Username1 { get; private set; }
    public string Username2 { get; private set; }
    public string UserPass1 { get; private set; }
    public string UserPass2 { get; private set; }
    public WebSocketFixture()
    {
        StunServerIp = "127.0.0.1";
        SignalingServerIp = "127.0.0.1";
        SignalingServerPort = 443;
        ClientIp = "0.0.0.0";
        ClientPort1 = 12345;
        ClientPort2 = 12346;
        Username1 = Guid.NewGuid().ToString();
        Username2 = Guid.NewGuid().ToString();
        UserPass1 = "111";
        UserPass2 = "pass";
    }
    public void Dispose()
    {
    }
}
