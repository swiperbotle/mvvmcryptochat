using Xunit;
namespace E2ETests.Fixtures;
[CollectionDefinition("WebSocket collection")]
public class WebSocketCollection : ICollectionFixture<WebSocketFixture>
{
    // This class has no code, and is never created. Its purpose is simply
    // to be the place to apply [CollectionDefinition] and all the
    // ICollectionFixture<> interfaces.
}