using System.Net;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using WebSocketServer.Enums;
using WebSocketServer.Models;
using E2ETests.Models;
using Xunit;
using System.Net.Sockets;
using StunTurnClientLibrary;
namespace E2ETests.Helpers;
public static class RequestHelper
{
    static async Task<Tuple<byte[], int>> ReceiveAsync(ClientWebSocket clientWebSocket)
    {
        var buffer = new byte[1024 * 10];
        var result = await clientWebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
        return new Tuple<byte[], int>(buffer, result.Count);
    }

    public static async Task<byte[]> SignInRequest(ClientWebSocket clientWebSocket, string username, string password)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.SignIn_Request);
        var bytesUsername = Encoding.UTF8.GetBytes(username);
        var bytesPassword = Encoding.UTF8.GetBytes(password);
        Array.Resize(ref bytesUsername, 8);
        Array.Resize(ref bytesPassword, 8);
        var msg = msgType.Concat(bytesUsername).Concat(bytesPassword).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        return res.Item1[0..res.Item2];
    }

    public static async Task<byte[]> SignUpRequest(ClientWebSocket clientWebSocket, UserModel userModel)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.SignUp_Request);
        var jsonBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(userModel));
        var msg = msgType.Concat(jsonBytes).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        return res.Item1[0..res.Item2];
    }

    public static async Task<byte[]> RemoveRequest(ClientWebSocket clientWebSocket, Guid userId)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.Remove_Request);
        var idBytes = userId.ToByteArray();
        var msg = msgType.Concat(idBytes).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        return res.Item1[0..res.Item2];
    }

    public static async Task<byte[]> SearchRequest(ClientWebSocket clientWebSocket, Guid userId, string username)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.Search_Request);
        var idBytes = userId.ToByteArray();
        var msg = msgType.Concat(idBytes).Concat(Encoding.UTF8.GetBytes(username)).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        return res.Item1[0..res.Item2];
    }

    public static async Task<byte[]> GetMessageIdRequest(ClientWebSocket clientWebSocket, Guid userId)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.Get_MessageId_Request);
        var idBytes = userId.ToByteArray();
        var msg = msgType.Concat(idBytes).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        return res.Item1[0..res.Item2];
    }

    public static async Task<byte[]> UpdateRequest(ClientWebSocket clientWebSocket, UserModel userModel)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.Update_Request);
        var idBytes = userModel.Id.ToByteArray();
        var jsonBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(userModel));
        var msg = msgType.Concat(idBytes).Concat(jsonBytes).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        return res.Item1[0..res.Item2];
    }
    
    public static async Task ConversationRequest(ClientWebSocket clientWebSocket, Guid userId, Guid destId, MessageModel message, string connectionType = "STUN", string clientIp = "0.0.0.0", int clientPort = 12345, string username = "1709740831", string password = "MTIzNDU2Nzg5MA==", string stunServerIp = "127.0.0.1", uint stunServerPort = 3478)
    {
        var msgType = BitConverter.GetBytes((int)MessageType.Conversation_Request);
        var idBytes = userId.ToByteArray();
        var destBytes = destId.ToByteArray();
        var conTypeBytes = Encoding.UTF8.GetBytes(connectionType);
        string reflexiveIp = "";
        uint reflexivePort = 0;
        if (connectionType == "STUN")
        {
            var stunClient = new StunClient(clientIp, clientPort, isFingerprint: true);
            var result = stunClient.StunRequest(stunServerIp, stunServerPort);
            reflexiveIp = result.Item2!;
            reflexivePort = result.Item3;
        }
        else if (connectionType == "TURN")
        {
            var turnClient = new TurnClient(clientIp, clientPort, password: password, username: username,
                               isFingerprint: true);
            var result = turnClient.TurnAllocateRequest(stunServerIp, stunServerPort, lifetime: 777, evenPort: true, dontFragment: true);
            reflexiveIp = result.Item4!;
            reflexivePort = result.Item5;
        }
        var msg = msgType.Concat(idBytes).Concat(destBytes).Concat(conTypeBytes).Concat(IPAddress.Parse(reflexiveIp).GetAddressBytes()).Concat(BitConverter.GetBytes(reflexivePort)).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        var res = await ReceiveAsync(clientWebSocket);
        Assert.Equal((int)MessageType.Conversation_Success_Response, BitConverter.ToInt32(res.Item1[..4]));
        res = await ReceiveAsync(clientWebSocket);
        Assert.Equal((int)MessageType.Conversation_Accept_Response, BitConverter.ToInt32(res.Item1[..4]));
        var bufferMsg = res.Item1[..res.Item2];
        var reflexiveIp1 = new IPAddress(bufferMsg[40..44]).ToString();
        var reflexivePort1 = BitConverter.ToInt32(bufferMsg[44..48]);
        using (var conn1 = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
        {
            conn1.Bind(new IPEndPoint(IPAddress.Parse(clientIp), clientPort));
            var destinationEndPoint = new IPEndPoint(IPAddress.Parse(reflexiveIp1), reflexivePort1);
            await conn1.SendToAsync(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(message)), destinationEndPoint);
        }
    }

    public static async Task<MessageModel> ConversationResponse(ClientWebSocket clientWebSocket, int clientPort = 12346, string clientIp = "0.0.0.0", string username = "1709740831", string password = "MTIzNDU2Nzg5MA==", string stunServerIp = "127.0.0.1", uint stunServerPort = 3478)
    {
        var buffer = new ArraySegment<byte>(new byte[1024 * 10]);
        var result = await clientWebSocket.ReceiveAsync(buffer, CancellationToken.None);
        Assert.Equal((int)MessageType.Conversation_Request, BitConverter.ToInt32(buffer.Array![..4]));
        var inbound = buffer[..result.Count];
        var conTypeBytes = inbound[36..40];
        var msgType1 = BitConverter.GetBytes((int)MessageType.Conversation_Accept_Response);
        var connectionType = Encoding.UTF8.GetString(conTypeBytes);
        var idBytes = inbound[20..36];
        var destBytes = inbound[4..20];
        var reflexiveIpSelf = "";
        uint reflexivePortSelf = 0;
        if (connectionType == "STUN")
        {
            var stunClient = new StunClient(clientIp, clientPort, isFingerprint: true);
            var res1 = stunClient.StunRequest(stunServerIp, stunServerPort);
            reflexiveIpSelf = res1.Item2!;
            reflexivePortSelf = res1.Item3;
        }
        else if (connectionType == "TURN")
        {
            var turnClient = new TurnClient(clientIp, clientPort, password: password, username: username,
                               isFingerprint: true);
            var res1 = turnClient.TurnAllocateRequest(stunServerIp, stunServerPort, lifetime: 777, evenPort: true, dontFragment: true);
            reflexiveIpSelf = res1.Item4!;
            reflexivePortSelf = res1.Item5;
        }
        var msg = msgType1.Concat(idBytes).Concat(destBytes).Concat(conTypeBytes).Concat(IPAddress.Parse(reflexiveIpSelf).GetAddressBytes()).Concat(BitConverter.GetBytes(reflexivePortSelf)).ToArray();
        await clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
        using (var conn1 = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
        {
            var reflexiveIp1 = new IPAddress(buffer[40..44]).ToString();
            var reflexivePort1 = BitConverter.ToInt32(buffer[44..48]);
            conn1.Bind(new IPEndPoint(IPAddress.Parse(reflexiveIpSelf), (int)reflexivePortSelf));
            var bufferMsg = new byte[8192];
            IPEndPoint clientEndpoint = new IPEndPoint(IPAddress.Any, 0);
            var receivedLength = await conn1.ReceiveAsync(bufferMsg);
            return JsonSerializer.Deserialize<MessageModel>(Encoding.UTF8.GetString(bufferMsg[..receivedLength]))!;
        }
    }

}