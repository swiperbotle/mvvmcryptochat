

using System.Net.Security;
using System.Net.WebSockets;
using System.Security.Cryptography.X509Certificates;

namespace E2ETests.Helpers;
public static class CertificateHelper
{
    const string _certStr = @"-----BEGIN CERTIFICATE-----
MIIFqTCCA5GgAwIBAgIUSPWZ37e6ptg1zcgHab1XNG0nC4owDQYJKoZIhvcNAQEL
BQAwZDELMAkGA1UEBhMCVUExEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDEdMBsGA1UEAwwUc2lnbmFsaW5nX3Nl
cnZlci5jb20wHhcNMjQwMzI4MDcyNjM0WhcNMzQwMzI2MDcyNjM0WjBkMQswCQYD
VQQGEwJVQTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQg
V2lkZ2l0cyBQdHkgTHRkMR0wGwYDVQQDDBRzaWduYWxpbmdfc2VydmVyLmNvbTCC
AiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBALFEPiqjkux1SBOdIbTPdjTP
R/Oroni80wwEPnz5vbhpFA6MzZt/5z8JNx6stYlogc+L6Zm7iedYV1uMduO1mtW1
y0VkDOuoTzxToZYayDbF0aCVL5kwIE5otAj938YYaqVwQ2ktWz54kfHrFy7h9zzI
ceQVncOjPh/mSl6GQ7Rllw7C9kTGQ9mVaoKn73VGP/af9HQBUtkFSdnonavkTHtj
wouhen4dkWG+/qMp9ZG09renRI73qfqYA/EVMEbHDnWJpxb5QBkqh6ny+3VhAuvy
vzNaAx/koUgZ0b7QkKJH7StGUI11l6I2E/tsRHRbIEBRDyImE2W44OVzxvJwZmWD
BK47b+zkI8uXvKnq+wxqcR5OOcKVvuudbEiwOpkWorLOyv+Yqfbq2UkJg76IsyHW
+PyE3TpbMJcAiGMR55wHLm8IGmsoZ+2Kmeh1mtK1vQ3O57irpRiGarzhqHlBRBXt
RHwPUDKxxzYLX4EENCt5lXInNiiQRH+9uXbW8Aj00a6nlXBOpBd+V1hLBT4/flzY
E6lRS7Gx3TpaNw84G/TP92p+RX4GhpDC+OCCDwVtKexqZXlFj/gdePZIR0/+c6YZ
DpIOUvyfq2WnUJ1ZCTnjjVHwfGxcDb15V3Dr5kngiuTae5oSu6GKUOEBJ8/b7PpI
R+haa3FKuMxUWPWASKatAgMBAAGjUzBRMB0GA1UdDgQWBBQlBIdglzuIB6+t98+m
eUe7UcNQjjAfBgNVHSMEGDAWgBQlBIdglzuIB6+t98+meUe7UcNQjjAPBgNVHRMB
Af8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4ICAQBuikU1mYFLS8PdZ938jJGMAtz8
ZM6KpDcZJ1xMLs/5ohzsOdt64Qwd5VOj9Y8Wbu7DRhBMYKS3S0q3voVQVELQpuv2
gIDd0km1XNKVQ6oZhGJQVrcx2ftGQP57WDXkmvab4fgq3qxOwFVVwZUNfY+TKW53
7W+c3WhaHLuGUQhh8wCJy161MIWkZHaDUyzA1d5ZTJjyqbseWJC2Kx8sydQ4gtq8
4hh/Rj0mu2sO9JnXYILWQ45hIN98g+8vkBRcsW7jKc+jfZLg0lUYjMgXGu/bV3Ur
CLb/UJa0CE0mB8SXoZOAoqiOm1Isiv2uyPq+qX2G7zqAxf2hmphiyhgttHiFVsai
IYtHVOvSShKFwZHkaEYTlsNy4CSXjYJYsS9WP9wbUpj4cSvChTx9tjh8Avvvg27F
NYG+ytuv219W1OQTaxYrZvCLNaASqZN3XFKXUpi+n5PG29Lm95HJOaMZI86/adQK
PF7erAdAUnB3YZFUoyQYayueA/JgRiWmX+7fDvfTK/KboV0pswYxYuKJ6OYyRzBF
ndQPFRXiL4gVpIE7lHkR2wsD/9QeXwZ2tKEBfxed7tKO7KxZEm7X6jaAt+HCm1yI
iKYFIjRXcIq4JU3XXFHv75G/9Id6e9W4CEV4Wgh9GQeu3rNybdwQLQ9Llo8WBjs+
igc84acNGCuiFM7tQA==
-----END CERTIFICATE-----";

    public static void InjectCertificate(ClientWebSocket ws)
    {
        var certBlob = new ReadOnlySpan<Char>(_certStr.ToCharArray());
        var cert = X509Certificate2.CreateFromPem(certBlob);
        ws.Options.RemoteCertificateValidationCallback = ValidateCertificate;
        ws.Options.ClientCertificates.Add(cert);
    }
    
    private static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        var cert = new ReadOnlySpan<Char>(_certStr.ToCharArray());
        if (new X509Certificate2(certificate).Equals(X509Certificate2.CreateFromPem(cert)) && sslPolicyErrors.HasFlag(SslPolicyErrors.None))
            return true;
        return false;
    }
}