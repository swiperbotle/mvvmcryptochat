using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Moq;
using WebSocketServer.ORM.DTOs;

namespace UnitTests.Helpers;
public class FakesFactory
{
    public Mock<T> CreateMock<T>() where T : class
    {
        var mock = new Mock<T>();
        return mock;
    }

    public Mock<T> CreateMock<T>(FakesFactoryOptions options) where T : class
    {
        Mock<T> mock;
        if (options.Parameters != null)
        {
            mock = new Mock<T>(options.MockBehavior, options.Parameters)
            {
                CallBase = options.CallBase
            };
        }
        else
        {
            mock = new Mock<T>(options.MockBehavior)
            {
                CallBase = options.CallBase
            };
        }
        return mock;
    }

    public Mock<DbSet<T>> CreateMockDbSet<T>(List<T> sourceList) where T : class
    {
        var queryable = sourceList.AsQueryable();
        var mockSet = new Mock<DbSet<T>>();

        mockSet.As<IQueryable<T>>().Setup(m => m.Provider).Returns(new TestAsyncQueryProvider<T>(queryable.Provider));
        mockSet.As<IQueryable<T>>().Setup(m => m.Expression).Returns(queryable.Expression);
        mockSet.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(queryable.ElementType);
        mockSet.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => queryable.GetEnumerator());

        mockSet.As<IAsyncEnumerable<T>>()
            .Setup(m => m.GetAsyncEnumerator(It.IsAny<CancellationToken>()))
            .Returns(new TestAsyncEnumerator<T>(queryable.GetEnumerator()));

        mockSet.Setup(d => d.AddAsync(It.IsAny<T>(), It.IsAny<CancellationToken>())).Callback((T entity, CancellationToken token) => { sourceList.Add(entity); }).ReturnsAsync((T entity, CancellationToken token) => null);
        mockSet.Setup(d => d.Remove(It.IsAny<T>())).Callback<T>(e => sourceList.Remove(e));
        mockSet.Setup(d => d.Update(It.IsAny<T>())).Callback<T>(entity =>
    {
        var existingEntity = sourceList.FirstOrDefault(e => e.Equals(entity));
        if (existingEntity != null)
        {
            sourceList.Remove(existingEntity);
            sourceList.Add(entity);
        }
    });
        return mockSet;
    }
    public List<UserDTO> CreateFakeStorage()
    {
        return new List<UserDTO>(new[] {
        new UserDTO { Id = Guid.NewGuid(), Username = "John", Password = "111", Avatar = "b64Avatar", IsPrivate = false },
        new UserDTO { Id = Guid.NewGuid(), Username = "Doe", Password = "111", Avatar = "b64Avatar", IsPrivate = false},
        new UserDTO { Id = Guid.NewGuid(), Username = "Spam",  Password = "111", Avatar = "b64Avatar", IsPrivate = false} });
    }

    public string CreateFakeName()
    {
        return Guid.NewGuid().ToString();
    }
}