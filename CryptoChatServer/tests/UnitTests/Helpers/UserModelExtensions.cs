using AutoMapper;
using WebSocketServer.Mappers;
using WebSocketServer.Models;
using WebSocketServer.ORM.DTOs;

namespace UnitTest.Helpers;
public static class UserModelExtensions
{
    private static IMapper CreateMapper()
    {
        var config = new MapperConfiguration(cfg =>
       {
           cfg.AddProfile<UserToUserProfile>();
       });
        var mapper = config.CreateMapper();
        return mapper;
    }
    public static UserDTO ToUserDTO(this UserModel userModel)
    {
        return CreateMapper().Map<UserDTO>(userModel);
    }
    public static UserIncompleteModel ToUserIncompleteModel(this UserModel userModel)
    {
        return CreateMapper().Map<UserIncompleteModel>(userModel);
    }
}