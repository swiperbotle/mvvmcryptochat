using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using WebSocketServer.Mappers;
using WebSocketServer.ORM.Contexts;

namespace UnitTests.Helpers;

public class UsersRepositoryDependencyFactory
{
    public DbContextOptions CreateUsersDbContextOptions(string dbName)
    {
        var contextOptionsBuilder = new DbContextOptionsBuilder<UsersDbContext>();
        contextOptionsBuilder.UseInMemoryDatabase(dbName);
        contextOptionsBuilder.ConfigureWarnings(x=>x.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        return contextOptionsBuilder.Options;
    }
    public IMapper CreateUserToUserMapper()
    {
        return new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new UserToUserProfile());
        }).CreateMapper();
    }
}