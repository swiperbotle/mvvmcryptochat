using Moq;

namespace UnitTests.Helpers;
public class FakesFactoryOptions
{
    public bool CallBase { get; set; } = false;
    public object[]? Parameters { get; set; } = null;
    public MockBehavior MockBehavior { get; set; } = MockBehavior.Default;
}