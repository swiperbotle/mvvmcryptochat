using Xunit;
using Microsoft.EntityFrameworkCore;
using WebSocketServer.Repositories;
using WebSocketServer.ORM.Contexts;
using Microsoft.Extensions.Logging;
using WebSocketServer.Mappers;
using AutoMapper;
using WebSocketServer.Models;
using Moq;
using WebSocketServer.Interfaces.Repositories;
using UnitTests.Fixtures;
using WebSocketServer.ORM.DTOs;
using UnitTests.Helpers;
using UnitTest.Helpers;
using WebSocketServer.Enums;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.Json;
using System.Diagnostics;

namespace UnitTests.Tests;
public class UsersRepositoryTests : IClassFixture<UsersRepositoryTestsFixture>
{
    private readonly UsersRepositoryTestsFixture _fixture;
    private readonly FakesFactory _FFactory;
    private readonly UsersRepositoryDependencyFactory _URDFactory;
    public UsersRepositoryTests(UsersRepositoryTestsFixture fixture)
    {
        _fixture = fixture;
        _FFactory = _fixture.FFactory;
        _URDFactory = _fixture.URDFactory;
    }
    UserModel CreateValidUserModel()
    {
        return new UserModel
        {
            Id = Guid.NewGuid(),
            Username = "New",
            Password = "Password",
            Avatar = "b64avatar",
            IsPrivate = false
        };
    }
    IUsersRepository CreateUsersRepository(ILoggerFactory loggerFactory, IDbContextFactory<UsersDbContext> dbContextFactory, IMapper mapper)
    {
        return new UsersRepository(loggerFactory, dbContextFactory, mapper);
    }
    private void MockLoggerSetup(Mock<ILogger<UsersRepository>> mock)
    {
        mock.Setup(x => x.Log(
           It.IsAny<LogLevel>(),
           It.IsAny<EventId>(),
           It.IsAny<It.IsAnyType>(),
           It.IsAny<Exception>(),
           (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()))
           .Callback(new InvocationAction(invocation =>
           {
               var logLevel = (LogLevel)invocation.Arguments[0];
               var eventId = (EventId)invocation.Arguments[1];
               var state = invocation.Arguments[2];
               var exception = (Exception)invocation.Arguments[3];
               var formatter = invocation.Arguments[4];
               var invokeMethod = formatter.GetType().GetMethod("Invoke");
               var logMessage = (string)invokeMethod?.Invoke(formatter, new[] { state, exception });
               Trace.WriteLine($"{logLevel} - {logMessage}");
           }));
    }
    private void MockLoggerVerify(Mock<ILogger<UsersRepository>> mock)
    {
        mock.Verify(m => m.Log(
        It.IsAny<LogLevel>(),
        It.IsAny<EventId>(),
        It.IsAny<It.IsAnyType>(),
        It.IsAny<Exception>(),
        (Func<It.IsAnyType, Exception, string>)It.IsAny<object>()), Times.AtLeastOnce);
    }

    [Fact]
    public async Task AddUser_ValidUser_SuccessfullyAdded()
    {
        // Arrange
        var dbName = _FFactory.CreateFakeName();
        var newEntity = CreateValidUserModel();
        var fakeStorage = _FFactory.CreateFakeStorage();
        var iniitialSize = fakeStorage.Count();
        var dbContextOptions = _URDFactory.CreateUsersDbContextOptions(dbName);
        var mockDbContextFactory = _FFactory.CreateMock<UsersDbContextFactory>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbContext = _FFactory.CreateMock<UsersDbContext>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbSet = _FFactory.CreateMockDbSet(fakeStorage);
        mockDbContextFactory.Setup(factory => factory.CreateDbContext()).Returns(mockDbContext.Object);
        mockDbContext.Setup(m => m.Users).Returns(mockDbSet.Object);
        var mockLogger = _FFactory.CreateMock<ILogger<UsersRepository>>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        var mockLoggerFactory = _FFactory.CreateMock<ILoggerFactory>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        mockLoggerFactory.Setup(factory => factory.CreateLogger(It.IsAny<string>())).Returns(mockLogger.Object);
        MockLoggerSetup(mockLogger);
        var usersRepository = CreateUsersRepository(mockLoggerFactory.Object, mockDbContextFactory.Object, _fixture.URDFactory.CreateUserToUserMapper());
        // Act
        var resp = await usersRepository.AddUser(newEntity);
        // Assert
        mockDbSet.Verify(m => m.AddAsync(It.IsAny<UserDTO>(), It.IsAny<CancellationToken>()), Times.Once);
        MockLoggerVerify(mockLogger);
        mockDbContext.Verify(c => c.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
        Assert.Equal(iniitialSize + 1, fakeStorage.Count());
        Assert.NotNull(fakeStorage.FirstOrDefault(item => item.Id == newEntity.ToUserDTO().Id));
        Assert.Equal(ErrorType.Success, resp);
    }
    [Fact]
    public async Task AddUser_NonValidUsername_NotAdded()
    {
        // Arrange
        var dbName = _FFactory.CreateFakeName();
        var entity = CreateValidUserModel();
        entity.Username = null;
        var fakeStorage = _FFactory.CreateFakeStorage();
        var iniitialSize = fakeStorage.Count();
        var dbContextOptions = _URDFactory.CreateUsersDbContextOptions(dbName);
        var mockDbContextFactory = _FFactory.CreateMock<UsersDbContextFactory>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbContext = _FFactory.CreateMock<UsersDbContext>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbSet = _FFactory.CreateMockDbSet(fakeStorage);
        mockDbContextFactory.Setup(factory => factory.CreateDbContext()).Returns(mockDbContext.Object);
        mockDbContext.Setup(m => m.Users).Returns(mockDbSet.Object);
        var mockLogger = _FFactory.CreateMock<ILogger<UsersRepository>>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        var mockLoggerFactory = _FFactory.CreateMock<ILoggerFactory>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        MockLoggerSetup(mockLogger);
        mockLoggerFactory.Setup(factory => factory.CreateLogger(It.IsAny<string>())).Returns(mockLogger.Object);
        var usersRepository = CreateUsersRepository(mockLoggerFactory.Object, mockDbContextFactory.Object, _fixture.URDFactory.CreateUserToUserMapper());
        // Act
        var resp = await usersRepository.AddUser(entity);
        // Assert
        mockDbSet.Verify(m => m.AddAsync(It.IsAny<UserDTO>(), It.IsAny<CancellationToken>()), Times.Never);
        mockDbContext.Verify(c => c.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Never());
        MockLoggerVerify(mockLogger);
        Assert.Equal(iniitialSize, fakeStorage.Count());
        Assert.Null(fakeStorage.FirstOrDefault(item => item.Id == entity.ToUserDTO().Id));
        Assert.Equal(ErrorType.Wrong_Model_Error, resp);
    }
    [Fact]
    public async Task GetUser_RetrieveUserIncompleteModel_ObtainExistedUser()
    {
        // Arrange
        var dbName = _FFactory.CreateFakeName();
        var fakeStorage = _FFactory.CreateFakeStorage();
        var entity = CreateValidUserModel();
        fakeStorage.Add(entity.ToUserDTO());
        var dbContextOptions = _URDFactory.CreateUsersDbContextOptions(dbName);
        var mockDbContextFactory = _FFactory.CreateMock<UsersDbContextFactory>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbContext = _FFactory.CreateMock<UsersDbContext>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbSet = _FFactory.CreateMockDbSet(fakeStorage);
        mockDbContextFactory.Setup(factory => factory.CreateDbContext()).Returns(mockDbContext.Object);
        mockDbContext.Setup(m => m.Users).Returns(mockDbSet.Object);
        var mockLogger = _FFactory.CreateMock<ILogger<UsersRepository>>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        var mockLoggerFactory = _FFactory.CreateMock<ILoggerFactory>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        MockLoggerSetup(mockLogger);
        mockLoggerFactory.Setup(factory => factory.CreateLogger(It.IsAny<string>())).Returns(mockLogger.Object);
        var usersRepository = CreateUsersRepository(mockLoggerFactory.Object, mockDbContextFactory.Object, _fixture.URDFactory.CreateUserToUserMapper());
        // Act
        var resp = await usersRepository.GetUser(entity.Username);
        // Assert
        MockLoggerVerify(mockLogger);
        Assert.Equal(ErrorType.Success, resp.Item1);
        Assert.Equivalent(entity.ToUserIncompleteModel(), JsonSerializer.Deserialize<UserDTO>(Encoding.UTF8.GetString(resp.Item2)));
    }
    [Fact]
    public async Task RemoveUser_RemoveUser_UserSuccessfullyRemoved()
    {
        // Arrange
        var dbName = _FFactory.CreateFakeName();
        var fakeStorage = _FFactory.CreateFakeStorage();
        var iniitialSize = fakeStorage.Count();
        var entity = fakeStorage[0];
        var dbContextOptions = _URDFactory.CreateUsersDbContextOptions(dbName);
        var mockDbContextFactory = _FFactory.CreateMock<UsersDbContextFactory>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbContext = _FFactory.CreateMock<UsersDbContext>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbSet = _FFactory.CreateMockDbSet(fakeStorage);
        mockDbContextFactory.Setup(factory => factory.CreateDbContext()).Returns(mockDbContext.Object);
        mockDbContext.Setup(m => m.Users).Returns(mockDbSet.Object);
        var mockLogger = _FFactory.CreateMock<ILogger<UsersRepository>>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        var mockLoggerFactory = _FFactory.CreateMock<ILoggerFactory>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        MockLoggerSetup(mockLogger);
        mockLoggerFactory.Setup(factory => factory.CreateLogger(It.IsAny<string>())).Returns(mockLogger.Object);
        var usersRepository = CreateUsersRepository(mockLoggerFactory.Object, mockDbContextFactory.Object, _fixture.URDFactory.CreateUserToUserMapper());
        // Act
        var resp = await usersRepository.RemoveUser(entity.Id);
        // Assert
        mockDbSet.Verify(m => m.Remove(It.IsAny<UserDTO>()), Times.Once);
        mockDbContext.Verify(c => c.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
        MockLoggerVerify(mockLogger);
        Assert.Equal(ErrorType.Success, resp);
        Assert.Equal(iniitialSize - 1, fakeStorage.Count());
    }
    [Fact]
    public async Task UpdateUser_ChangeUser_UserSuccessfullyUpdated()
    {
        // Arrange
        var config = new MapperConfiguration(cfg =>
        {
            cfg.AddProfile<UserToUserProfile>();
        });
        var mapper = config.CreateMapper();
        var dbName = _FFactory.CreateFakeName();
        var fakeStorage = _FFactory.CreateFakeStorage();
        var entity = fakeStorage[0];
        entity.Password = "pass";
        var dbContextOptions = _URDFactory.CreateUsersDbContextOptions(dbName);
        var mockDbContextFactory = _FFactory.CreateMock<UsersDbContextFactory>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbContext = _FFactory.CreateMock<UsersDbContext>(new FakesFactoryOptions { CallBase = true, Parameters = new object[] { dbContextOptions } });
        var mockDbSet = _FFactory.CreateMockDbSet(fakeStorage);
        mockDbContextFactory.Setup(factory => factory.CreateDbContext()).Returns(mockDbContext.Object);
        mockDbContext.Setup(m => m.Users).Returns(mockDbSet.Object);
        var mockLogger = _FFactory.CreateMock<ILogger<UsersRepository>>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        var mockLoggerFactory = _FFactory.CreateMock<ILoggerFactory>(new FakesFactoryOptions { MockBehavior = MockBehavior.Loose });
        MockLoggerSetup(mockLogger);
        mockLoggerFactory.Setup(factory => factory.CreateLogger(It.IsAny<string>())).Returns(mockLogger.Object);
        var usersRepository = CreateUsersRepository(mockLoggerFactory.Object, mockDbContextFactory.Object, _fixture.URDFactory.CreateUserToUserMapper());
        // Act
        var resp = await usersRepository.UpdateUser(mapper.Map<UserModel>(entity));
        // Assert
        MockLoggerVerify(mockLogger);
        mockDbContext.Verify(c => c.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once());
        Assert.Equal(ErrorType.Success, resp);
        Assert.Equal(entity.Password, fakeStorage[0].Password);
    }
}