using UnitTests.Helpers;

namespace UnitTests.Fixtures;
public class UsersRepositoryTestsFixture : IDisposable
{
    public FakesFactory FFactory { get; set; }
    public UsersRepositoryDependencyFactory URDFactory{ get; set; }
    public UsersRepositoryTestsFixture()
    {
        FFactory = new FakesFactory();
        URDFactory = new UsersRepositoryDependencyFactory();
    }
    public void Dispose()
    {
    
    }
}