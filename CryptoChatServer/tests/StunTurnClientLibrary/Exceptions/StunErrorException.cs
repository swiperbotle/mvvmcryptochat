namespace StunTurnClientLibrary.Exceptions;
public class StunError : Exception
{
    public StunError(string message) : base(message) { }
}