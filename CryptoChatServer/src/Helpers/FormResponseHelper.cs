using WebSocketServer.Enums;

namespace WebSocketServer.Helpers
{
    public static class FormResponse
    {
        public static byte[] formResponse(MessageType msgType)
        {
            return BitConverter.GetBytes((int)msgType);
        }
        public static byte[] formResponse(MessageType msgType, Guid userId)
        {
            return BitConverter.GetBytes((int)msgType).Concat(userId.ToByteArray()).ToArray();
        }
        public static byte[] formResponse(MessageType msgType, ErrorType details)
        {
            return BitConverter.GetBytes((int)msgType).Concat(BitConverter.GetBytes((int)details)).ToArray();
        }
        public static byte[] formResponse(MessageType msgType, byte[] data)
        {
            return BitConverter.GetBytes((int)msgType).Concat(data).ToArray();
        }
        public static byte[] formResponse(byte[] data, int length)
        {
            return data[..length];
        }
    }
}