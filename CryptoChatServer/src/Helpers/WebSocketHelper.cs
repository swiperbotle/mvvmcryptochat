using System.Net.WebSockets;
using System.Text;
using WebSocketServer.Interfaces.Helpers;
namespace WebSocketServer.Helpers
{
    public class WebSocketHelper : IWebSocketHelper
    {
        public WebSocket connection;
        public WebSocketHelper(WebSocket connection)
        {
            this.connection = connection;
        }
 //TODO: specify WebSocketExcpetion
        public async Task ReceiveMessageAsync(WebSocket connection, Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            try
            {
                var buffer = new byte[1024 * 4];
                while (connection.State == WebSocketState.Open)
                {
                    var result = await connection.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    handleMessage(result, buffer);
                }
            }
            catch (WebSocketException)
            {
            }
        }

        public async Task ReceiveMessageAsync(Action<WebSocketReceiveResult, byte[]> handleMessage)
        {
            try
            {
                var buffer = new byte[1024 * 4];
                while (connection.State == WebSocketState.Open)
                {
                    var result = await connection.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    handleMessage(result, buffer);
                }
            }
            catch (WebSocketException)
            {
            }
        }

        public async Task<WebSocketReceiveResult> ReceiveMessageAsync()
        {
            var buffer = new byte[1024 * 4];
            var result = await connection.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            return result;
        }



        public async Task<WebSocketReceiveResult> ReceiveMessageAsync(WebSocket socket)
        {
            var buffer = new byte[1024 * 4];
            var result = await connection.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            return result;
        }


        public async Task SendMessageAsync(string message)
        {
            var bytes = Encoding.UTF8.GetBytes(message);
            if (connection.State == WebSocketState.Open)
            {
                var arraySegment = new ArraySegment<byte>(bytes, 0, bytes.Length);
                await connection.SendAsync(arraySegment, WebSocketMessageType.Text, true, CancellationToken.None);
            }
        }

        public async Task SendMessageAsync(byte[] message)
        {
            var bytes = message;
            if (connection.State == WebSocketState.Open)
            {
                var arraySegment = new ArraySegment<byte>(bytes, 0, bytes.Length);
                await connection.SendAsync(arraySegment, WebSocketMessageType.Binary, true, CancellationToken.None);
            }
        }
    }
}