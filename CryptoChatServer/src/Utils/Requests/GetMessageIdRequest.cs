using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Services;
namespace WebSocketServer.Utils.Requests
{
    public class GetMessageIdRequest : IBaseRequest
    {
        private readonly IAuthorizeService authorizeService;
        private readonly ILogger<IBaseRequest> logger;
        private readonly IUsersRepository usersRepository;
        private readonly MessageIdIssuerService _messageIdIssuerService;
        public GetMessageIdRequest(ILoggerFactory loggerFactory, IUsersRepository usersRepository, IAuthorizeService authorizeService, MessageIdIssuerService messageIdIssuerService)
        {
            this.authorizeService = authorizeService;
            this.logger = loggerFactory.CreateLogger<RemoveRequest>();
            this.usersRepository = usersRepository;
            _messageIdIssuerService = messageIdIssuerService;
        }
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
        {
            var userId = new Guid(data[4..20]);
            if (authorizeService.CheckAuthorization(userId))
            {
                logger.LogInformation("Get message Id");
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Get_MessageId_Success, BitConverter.GetBytes(_messageIdIssuerService.GiveNewMessageId())), Guid.Empty) };
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Not_Authorize_Error), Guid.Empty) };
        }
    }
}
