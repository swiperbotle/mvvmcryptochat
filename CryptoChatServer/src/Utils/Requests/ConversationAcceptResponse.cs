using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
namespace WebSocketServer.Utils.Requests
{
    public class ConversationAcceptResponse : IBaseRequest
    {
        private readonly IDictionary<Guid, HashSet<Guid>> _allowedSourcesDict;
        private readonly IAuthorizeService _authorizeService;
        private readonly ILogger<IBaseRequest> _logger;
        private readonly IConnectionsManager _connectionsManager;

        public ConversationAcceptResponse(IDictionary<Guid, HashSet<Guid>> allowedSourcesDict, ILoggerFactory loggerFactory, IAuthorizeService authorizeService, IConnectionsManager connectionsManager)
        {
            _allowedSourcesDict = allowedSourcesDict;
            _authorizeService = authorizeService;
            _logger = loggerFactory.CreateLogger<ConversationAcceptResponse>();
            _connectionsManager = connectionsManager;
        }
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            var userId = new Guid(data[4..20]);
            var destinationId = new Guid(data[20..36]);
            _logger.LogInformation("Conversation Response");
            if (!_authorizeService.CheckAuthorization(userId))
            {
                _logger.LogInformation("Conversation authorize failed");
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Not_Authorize_Error), Guid.Empty) };
            }
            if (!_connectionsManager.ItemExist(destinationId) || !_allowedSourcesDict.ContainsKey(destinationId))
            {
                _logger.LogInformation("Dictionary not exist");
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Bad_Request_Error), Guid.Empty) };
            }
            if (_allowedSourcesDict[destinationId].Contains(userId))
            {
                _allowedSourcesDict[destinationId].Remove(userId);
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(data, length), destinationId) };
            }
            _logger.LogInformation("userID not exist");
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Bad_Request_Error), Guid.Empty) };
        }
    }

}