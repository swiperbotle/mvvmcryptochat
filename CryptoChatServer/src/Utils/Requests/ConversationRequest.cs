using WebSocketServer.Collections;
using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
namespace WebSocketServer.Utils.Requests
{
    public class ConversationRequest : IBaseRequest
    {
        private readonly IDictionary<Guid, HashSet<Guid>> _allowedSourcesDict;
        private readonly IAuthorizeService _authorizeService;
        private readonly ILogger<IBaseRequest> _logger;
        private readonly IConnectionsManager _connectionsManager;

        public ConversationRequest(IDictionary<Guid, HashSet<Guid>> allowedSourcesDict, ILoggerFactory loggerFactory, IAuthorizeService authorizeService, IConnectionsManager connectionsManager)
        {
            _allowedSourcesDict = allowedSourcesDict;
            _authorizeService = authorizeService;
            _logger = loggerFactory.CreateLogger<ConversationRequest>();
            _connectionsManager = connectionsManager;
        }
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            var userId = new Guid(data[4..20]);
            var destinationId = new Guid(data[20..36]);
            _logger.LogInformation("Conversation Request");
            if (!_authorizeService.CheckAuthorization(userId))
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Not_Authorize_Error), Guid.Empty) };
            if (!_connectionsManager.ItemExist(destinationId))
            {
                Console.WriteLine($"{destinationId}");
                Console.WriteLine("Conversation NOT success first stage");
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Bad_Request_Error), Guid.Empty) };
            }
            if (_allowedSourcesDict.ContainsKey(userId))
            {
                _allowedSourcesDict[userId].Add(destinationId);
            }
            else
            {
                _allowedSourcesDict.Add(userId, new AllowedSourceItem { destinationId });
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(data, length), destinationId), new(FormResponse.formResponse(MessageType.Conversation_Success_Response), Guid.Empty) };
        }
    }

}