using System.Text;
using System.Text.Json;
using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Models;
namespace WebSocketServer.Utils.Requests
{
    public class SignInRequest : IBaseRequest
    {
        private readonly IAuthorizeService authorizeService;
        private readonly ILogger<IBaseRequest> logger;
        private readonly IUsersRepository usersRepository;
        public SignInRequest(ILoggerFactory loggerFactory, IUsersRepository usersRepository, IAuthorizeService authorizeService)
        {
            this.authorizeService = authorizeService;
            this.logger = loggerFactory.CreateLogger<SignInRequest>();
            this.usersRepository = usersRepository;
        }
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
        {
            logger.LogInformation("Sign In");
            var username = Encoding.UTF8.GetString(data[4..12]).Replace("\0", "");
            var password = Encoding.UTF8.GetString(data[12..20]).Replace("\0", "");
            var res = await usersRepository.GetUser(username, password);
            if (res.Item1 == ErrorType.Success)
            {
                authorizeService.UserId = JsonSerializer.Deserialize<UserModel>(Encoding.UTF8.GetString(res.Item2))!.Id;
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.SignIn_Success_Response, res.Item2), Guid.Empty) };
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, res.Item1), Guid.Empty) };
        }
    }
}