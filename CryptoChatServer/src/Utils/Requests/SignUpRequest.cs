using System.Text;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Helpers;
using WebSocketServer.Enums;
using System.Text.Json;
using WebSocketServer.Models;
namespace WebSocketServer.Utils.Requests
{
    public class SignUpRequest : IBaseRequest
    {
        private readonly ILogger<IBaseRequest> logger;
        private readonly IUsersRepository usersRepository;
        public SignUpRequest(ILoggerFactory loggerFactory, IUsersRepository usersRepository)
        {
            this.logger = loggerFactory.CreateLogger<SignUpRequest>();
            this.usersRepository = usersRepository;
        }
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
        {
            var userModel = JsonSerializer.Deserialize<UserModel>(Encoding.UTF8.GetString(data[4..length]));
             if (userModel == null)
            {
                logger.LogInformation("Bad create request");
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Bad_Request_Error), Guid.Empty) };
            }
            var errorType = await usersRepository.AddUser(userModel);
            if (errorType == ErrorType.Success)
            {
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.SignUp_Success_Response), Guid.Empty) };
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, errorType), Guid.Empty) };
        }
    }
}