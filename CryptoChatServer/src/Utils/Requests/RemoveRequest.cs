using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
namespace WebSocketServer.Utils.Requests
{
    public class RemoveRequest : IBaseRequest
    {
        private readonly IAuthorizeService authorizeService;
        private readonly ILogger<IBaseRequest> logger;
        private readonly IUsersRepository usersRepository;
        public RemoveRequest(ILoggerFactory loggerFactory, IUsersRepository usersRepository, IAuthorizeService authorizeService)
        {
            this.authorizeService = authorizeService;
            this.logger = loggerFactory.CreateLogger<RemoveRequest>();
            this.usersRepository = usersRepository;
        }
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
        {
            var userId = new Guid(data[4..20]);
            logger.LogInformation("Remove user");
            if (authorizeService.CheckAuthorization(userId))
            {
                var errorType = await usersRepository.RemoveUser(userId);
                if (errorType == ErrorType.Success)
                {
                    return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Remove_Success_Response), Guid.Empty) };
                }
                else
                {
                    return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, errorType), Guid.Empty) };
                }
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Not_Authorize_Error), Guid.Empty) };
        }
    }
}