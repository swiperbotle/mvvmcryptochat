using System.Text;
using System.Text.Json;
using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Models;
namespace WebSocketServer.Utils.Requests
{
    public class SearchRequest : IBaseRequest
    {
        private readonly IAuthorizeService _authorizeService;
        private readonly ILogger<IBaseRequest> _logger;
        private readonly IUsersRepository _usersRepository;
        public SearchRequest(IAuthorizeService authorizeService, ILoggerFactory loggerFactory, IUsersRepository usersRepository)
        {
            _authorizeService = authorizeService;
            _logger = loggerFactory.CreateLogger<SearchRequest>();
            _usersRepository = usersRepository;
        }
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
        {
            var userId = new Guid(data[4..20]);
            _logger.LogInformation("Search request");
            if (_authorizeService.CheckAuthorization(userId))
            {
                var username = Encoding.UTF8.GetString(data[20..28]).Replace("\0", "");
                var res = await _usersRepository.GetUser(username);
                if (res.Item1 == ErrorType.Success)
                {
                    return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Search_Success_Response, res.Item2), Guid.Empty) };
                }
                else
                {
                    return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, res.Item1), Guid.Empty) };
                }
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Not_Authorize_Error), Guid.Empty) };
        }
    }
}