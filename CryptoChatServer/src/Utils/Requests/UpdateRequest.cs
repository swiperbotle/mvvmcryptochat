using System.Text;
using System.Text.Json;
using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Models;
namespace WebSocketServer.Utils.Requests
{
    public class UpdateRequest : IBaseRequest
    {
        private readonly IAuthorizeService authorizeService;
        private readonly ILogger<IBaseRequest> logger;
        private readonly IUsersRepository usersRepository;
        public UpdateRequest(ILoggerFactory loggerFactory, IUsersRepository usersRepository, IAuthorizeService authorizeService)
        {
            this.authorizeService = authorizeService;
            this.logger = loggerFactory.CreateLogger<UpdateRequest>();
            this.usersRepository = usersRepository;
        }
        public async Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length)
        {
            logger.LogInformation("Updating existing user");
            var userModel = JsonSerializer.Deserialize<UserModel>(Encoding.UTF8.GetString(data[20..length]));
            logger.LogInformation($"{userModel?.Username}");
            if (userModel == null)
            {
                logger.LogInformation("Bad update request");
                return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Bad_Request_Error), Guid.Empty) };
            }
            if (authorizeService.CheckAuthorization(userModel.Id))
            {
                var errorType = await usersRepository.UpdateUser(userModel);
                if (errorType == ErrorType.Success)
                {
                    return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Update_Success_Response), Guid.Empty) };
                }
                else
                {
                    return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, errorType), Guid.Empty) };
                }
            }
            return new List<Tuple<byte[], Guid>> { new(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Not_Authorize_Error), Guid.Empty) };
        }
    }
}