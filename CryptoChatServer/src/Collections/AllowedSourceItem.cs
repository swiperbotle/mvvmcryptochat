namespace WebSocketServer.Collections
{
    public class AllowedSourceItem : HashSet<Guid>
    {
        public AllowedSourceItem() : base(new HashSet<Guid>()) { }
        public new void Add(Guid key)
        {
            base.Add(key);
            var timer = new Timer(state =>
            {
                if (Contains(key))
                {
                    Remove(key);
                }
            }, null, TimeSpan.FromSeconds(3), Timeout.InfiniteTimeSpan);
        }
    }
}