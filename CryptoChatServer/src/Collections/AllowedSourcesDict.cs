namespace WebSocketServer.Collections
{
    public class AllowedSourcesDict : Dictionary<Guid, HashSet<Guid>>
    {
        public AllowedSourcesDict() : base(new Dictionary<Guid, HashSet<Guid>>())
        { 
        }
    }
}