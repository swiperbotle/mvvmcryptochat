namespace WebSocketServer.Services
{
    public class MessageIdIssuerService
    {
        public Int64 MessageId { get; set; }
        string filePath = "MessageIdFile.txt";
        public MessageIdIssuerService()
        {
            if (File.Exists(filePath))
            {
                MessageId = Int64.Parse(File.ReadAllText(filePath));
            }
            else
            {
                MessageId = 0;
            }
        }

        public Int64 GiveNewMessageId()
        {
            ++MessageId;
            return MessageId;
        }
        public void OnExit()
        {
            File.WriteAllText(filePath, MessageId.ToString());
        }
    }
}