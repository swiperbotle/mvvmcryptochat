using WebSocketServer.Interfaces.Services;

namespace WebSocketServer.Services
{
    public class AuthorizeService : IAuthorizeService
    {
        public bool IsAuthorize { get; set; } = false;
        private Guid _userId;
        public Guid UserId { get => _userId; set { IsAuthorize = true; _userId = value; } }

        public bool CheckAuthorization(Guid userId)
        {
            return UserId == userId && IsAuthorize;
        }
    }
}