namespace WebSocketServer.Services
{
    public class ApplicationLifetimeService : IHostedService
    {
        private readonly ILogger<ApplicationLifetimeService> _logger;
        private readonly IHostApplicationLifetime _appLifetime;
        private readonly MessageIdIssuerService _msgIdIssuer;
        public ApplicationLifetimeService(IHostApplicationLifetime appLifetime, ILoggerFactory loggerFactory, MessageIdIssuerService msgIdIssuer)
        {
            _msgIdIssuer = msgIdIssuer;
            _logger = loggerFactory.CreateLogger<ApplicationLifetimeService>();
            _appLifetime = appLifetime;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Service is starting...");

            _appLifetime.ApplicationStopping.Register(OnStopping);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void OnStopping()
        {
            _logger.LogInformation("Application is stopping. Saving file...");
            _msgIdIssuer.OnExit();
        }
    }
}