using WebSocketServer.Interfaces.Services;
using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Enums;
using WebSocketServer.Utils.Requests;
using System.Text.Json;
using System.Text;
using WebSocketServer.Helpers;
namespace WebSocketServer.Services
{
    public class SignalingService : ISignalingService
    {
        public List<Tuple<byte[], Guid>> Responces { get; set; }
        private readonly Dictionary<MessageType, IBaseRequest> requests;
        private readonly ILogger<SignalingService> _logger;
        public SignalingService(ILoggerFactory loggerFactory, SignUpRequest signUpRequest, SignInRequest signInRequest, UpdateRequest updateRequest, RemoveRequest removeRequest, ConversationRequest conversationRequest, ConversationAcceptResponse conversationResponse, GetMessageIdRequest getMessageIdRequest, SearchRequest searchRequest)
        {
            this.requests = new Dictionary<MessageType, IBaseRequest>();
            this.requests[MessageType.SignUp_Request] = signUpRequest;
            this.requests[MessageType.SignIn_Request] = signInRequest;
            this.requests[MessageType.Update_Request] = updateRequest;
            this.requests[MessageType.Remove_Request] = removeRequest;
            this.requests[MessageType.Conversation_Request] = conversationRequest;
            this.requests[MessageType.Conversation_Accept_Response] = conversationResponse;
            this.requests[MessageType.Get_MessageId_Request] = getMessageIdRequest;
            this.requests[MessageType.Search_Request] = searchRequest;
            _logger = loggerFactory.CreateLogger<SignalingService>();
        }
        public async Task ValidateMessage(byte[] data, int length)
        {
            try
            {
                var request = requests[(MessageType)BitConverter.ToInt32(data[..4], 0)];
                Responces = await request.Handle(data, length);
            }
            catch (Exception ex)
            {
                if (ex is ArgumentNullException || ex is ArgumentOutOfRangeException || ex is FormatException || ex is JsonException || ex is NotSupportedException || ex is DecoderFallbackException || ex is OutOfMemoryException || ex is IndexOutOfRangeException || ex is ArgumentException || ex is OverflowException || ex is InvalidDataException || ex is NullReferenceException || ex is KeyNotFoundException)
                {
                    _logger.LogInformation("Bad Request");
                    Responces = new List<Tuple<byte[], Guid>> { new Tuple<byte[], Guid>(FormResponse.formResponse(MessageType.Error_Response, ErrorType.Bad_Request_Error), Guid.Empty) };
                    return;
                }
                _logger.LogError("Unhandled exception");
                throw;
            }
        }
    }
}