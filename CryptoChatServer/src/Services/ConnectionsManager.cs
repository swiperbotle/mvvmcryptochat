using System.Net.WebSockets;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Services;

namespace WebSocketServer.Services
{
    public class ConnectionsManager : IConnectionsManager
    {
        public Dictionary<Guid, WebSocketHelper> Connections { get; set; }
        public ConnectionsManager()
        {
            Connections = new Dictionary<Guid, WebSocketHelper>();
        }
        public void AddItem(Guid userId, WebSocketHelper webSocket)
        {
            Connections[userId] = webSocket;
        }
        public WebSocketHelper GetItem(Guid userId)
        {
            return Connections[userId];
        }
        public bool ItemExist(Guid userId)
        {
            return Connections.ContainsKey(userId);
        }
        public void RemoveItem(Guid userId)
        {
            Connections.Remove(userId);
        }
    }
}