using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WebSocketServer.Enums;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Contexts;
using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Models;
using WebSocketServer.ORM.Contexts;
using WebSocketServer.ORM.DTOs;

namespace WebSocketServer.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly UsersDbContext dbContext;
        private readonly ILogger<IUsersRepository> logger;
        private readonly IMapper mapper;
        public UsersRepository(ILoggerFactory loggerFactory, IDbContextFactory<UsersDbContext> dbContextFactory, IMapper mapper)
        {
            this.dbContext = dbContextFactory.CreateDbContext();
            this.logger = loggerFactory.CreateLogger<UsersRepository>();
            this.mapper = mapper;
            if (this.dbContext == null)
                throw new ArgumentNullException(nameof(this.dbContext));
            this.dbContext.Database.EnsureCreated();
            if (this.dbContext.Database.ProviderName!.Equals("Microsoft.EntityFrameworkCore.SqlServer", StringComparison.OrdinalIgnoreCase))
            {
                if (dbContext.Database.GetPendingMigrations().Any())
                {
                    dbContext.Database.Migrate();
                }
            }
        }
        ~UsersRepository()
        {
            this.Dispose();
        }
        public void Dispose()
        {
            dbContext.Dispose();
        }
        private bool ValidateModel(UserModel userModel)
        {
            var validationContext = new ValidationContext(userModel);
            var results = new List<ValidationResult>();
            return Validator.TryValidateObject(userModel, validationContext, results, true);
        }
        public async Task<ErrorType> AddUser(UserModel userModel)
        {
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ValidateModel(userModel))
                    {
                        logger.LogInformation($"Wrong input model.");
                        return ErrorType.Wrong_Model_Error;
                    }
                    var userDTO = await dbContext.Users.FirstOrDefaultAsync(u => u.Username == userModel!.Username);
                    if (userDTO != null)
                    {
                        logger.LogInformation($"User already exist: {userDTO.Username}");
                        return ErrorType.Login_Already_In_Use_Error;
                    }
                    else
                    {
                        userDTO = mapper.Map(userModel, userDTO);
                        await dbContext.Users.AddAsync(userDTO!);
                        await dbContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                        logger.LogInformation($"User {userDTO!.Username} addes successfully.");
                        return ErrorType.Success;
                    }
                }
                catch (DbUpdateException ex)
                {
                    await transaction.RollbackAsync();
                    logger.LogInformation($"Transaction rolled back due to error: + {ex.Message}");
                }
            }
            return ErrorType.Unknown_Server_Error;
        }
        public async Task<Tuple<ErrorType, byte[]>> GetUser(string username)
        {
            var userDTO = await dbContext.Users.FirstOrDefaultAsync(u => u.Username == username);
            if (userDTO != null)
            {
                logger.LogInformation($"User found: {username}");
                var userModel = mapper.Map<UserIncompleteModel>(userDTO);
                return new(ErrorType.Success, Encoding.UTF8.GetBytes(JsonSerializer.Serialize(userModel)));
            }
            else
            {
                logger.LogInformation($"User {username} not found.");
                return new(ErrorType.User_Not_Found_Error, new byte[] { 0 });
            }
        }

        public async Task<Tuple<ErrorType, byte[]>> GetUser(string username, string password)
        {
            var userDTO = await dbContext.Users.FirstOrDefaultAsync(u => u.Username == username && u.Password == password);
            logger.LogInformation($"Outbound Pass {password}");
            if (userDTO != null)
            {
                logger.LogInformation($"User found: {username}");
                var userModel = mapper.Map<UserModel>(userDTO);
                return new(ErrorType.Success, Encoding.UTF8.GetBytes(JsonSerializer.Serialize(userModel)));
            }
            else
            {
                logger.LogInformation($"User {username}not found.");
                return new(ErrorType.Wrong_Credentials_Error, new byte[] { 0 });
            }
        }

        public async Task<ErrorType> RemoveUser(Guid userId)
        {
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    var userDTO = await dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);
                    if (userDTO != null)
                    {
                        dbContext.Users.Remove(userDTO!);
                        await dbContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                        logger.LogInformation($"User was successfuly removed");
                        return ErrorType.Success;
                    }
                    else
                    {
                        logger.LogInformation($"User {userId} not found.");
                        return ErrorType.User_Not_Found_Error;
                    }
                }
                catch (DbUpdateException ex)
                {
                    await transaction.RollbackAsync();
                    logger.LogInformation("Transaction rolled back due to error: " + ex.Message);
                }
            }
            return ErrorType.Unknown_Server_Error;
        }
        public async Task<ErrorType> UpdateUser(UserModel userModel)
        {
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    if (!ValidateModel(userModel))
                    {
                        logger.LogInformation($"Wrong input model.");
                        return ErrorType.Wrong_Model_Error;
                    }
                    var userDTO = await dbContext.Users.FirstOrDefaultAsync(u => u.Id == userModel!.Id);
                    if (userDTO != null)
                    {
                        var existUsersDTO = await dbContext.Users.Where(user => user.Username == userModel.Username).ToListAsync();
                        if (existUsersDTO != null)
                        {
                            foreach (var userTemp in existUsersDTO)
                            {
                                if (userTemp.Id != userModel.Id)
                                {
                                    logger.LogInformation($"User with username{userDTO.Username} already exist");
                                    return ErrorType.Update_User_Already_Exist_Error;
                                }
                            }
                        }
                        userDTO = mapper.Map(userModel, userDTO);
                        await dbContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                        logger.LogInformation("User was successfully updated");
                        return ErrorType.Success;
                    }
                    else
                    {
                        logger.LogInformation($"User {userModel.Id} not found.");
                        return ErrorType.User_Not_Found_Error;
                    }
                }
                catch (DbUpdateException ex)
                {
                    await transaction.RollbackAsync();
                    logger.LogInformation("Transaction rolled back due to error: " + ex.Message);
                }
            }
            return ErrorType.Unknown_Server_Error;
        }
    }
}