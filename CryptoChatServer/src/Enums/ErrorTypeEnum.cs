namespace WebSocketServer.Enums
{
    public enum ErrorType : int
    {
        // reserved, used only internaly 
        Success = 0x1488,
        Unknown_Server_Error = 0xFFFF,
        Bad_Request_Error = 0xFFFFE,
        // SignUp_Error_Password_Missmatch = 0x1002,
        Login_Already_In_Use_Error = 0xE001,
        // SignUp_Error_To_Short_Password = 0x1004,
        Wrong_Credentials_Error = 0xE002,
        User_Not_Found_Error = 0xE003,
        Not_Authorize_Error = 0xE004,
        Update_User_Already_Exist_Error = 0xE113,
        Wrong_Model_Error = 0xE3452
    }
}