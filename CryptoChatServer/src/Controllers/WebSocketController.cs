using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Net.WebSockets;
using System.Text;
using WebSocketServer.Helpers;
using WebSocketServer.Interfaces.Helpers;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Services;
using WebSocketServer.ORM.Contexts;
using WebSocketServer.Enums;
namespace WebSocketServer.Controllers
{
    [Route("ws")]
    [ApiController]
    public class WebSocketController : Controller
    {
        private readonly ISignalingService _signalingService;
        private readonly ILogger<WebSocketController> _logger;
        private readonly IAuthorizeService _authorizeService;
        private readonly IConnectionsManager _connectionsManager;
        public WebSocketController(ISignalingService signalingService, ILoggerFactory loggerFactory, IAuthorizeService authorizeService, IConnectionsManager connectionsManager)
        {
            _logger = loggerFactory.CreateLogger<WebSocketController>();
            _signalingService = signalingService;
            _authorizeService = authorizeService;
            _connectionsManager = connectionsManager;
        }
        public async Task StartConnection()
        {
            _logger.LogInformation("Start connection");
            try
            {
                var context = ControllerContext.HttpContext;
                if (context.WebSockets.IsWebSocketRequest)
                {
                    using var ws = await context.WebSockets.AcceptWebSocketAsync();
                    var socket = new WebSocketHelper(ws);
                    await socket.ReceiveMessageAsync(ws,
                        async (result, buffer) =>
                        {
                            if (result.MessageType == WebSocketMessageType.Binary)
                            {
                                await _signalingService.ValidateMessage(buffer, result.Count);
                                if (!_authorizeService.IsAuthorize && (MessageType)BitConverter.ToInt32(buffer[..4]) != MessageType.SignUp_Request && (MessageType)BitConverter.ToInt32(buffer[..4]) != MessageType.SignIn_Request)
                                {
                                    _logger.LogInformation($"Unathorized");
                                    ws.Abort();
                                    return;
                                }
                                if (_authorizeService.IsAuthorize && ws != null && !_connectionsManager.ItemExist(_authorizeService.UserId))
                                {
                                    Console.WriteLine($"{_authorizeService.UserId}");
                                    _connectionsManager.AddItem(_authorizeService.UserId, socket);
                                }
                                foreach (var responce in _signalingService.Responces)
                                {
                                    if (responce.Item2 == Guid.Empty)
                                    {
                                        await socket.SendMessageAsync(responce.Item1);
                                    }
                                    else
                                    {
                                        await _connectionsManager.GetItem(responce.Item2).SendMessageAsync(responce.Item1);
                                    }
                                }
                            }
                            else if (result.MessageType == WebSocketMessageType.Close || ws.State == WebSocketState.Aborted)
                            {
                                _logger.LogInformation($"left the room");
                                _connectionsManager.RemoveItem(_authorizeService.UserId);
                                await ws.CloseAsync(result.CloseStatus!.Value, result.CloseStatusDescription, CancellationToken.None);
                            }
                            Array.Clear(buffer, 0, buffer.Length); ;
                        });
                }
                else
                {
                    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"Exception {ex} occured");
            }
            if (_connectionsManager.ItemExist(_authorizeService.UserId))
            {
                _connectionsManager.RemoveItem(_authorizeService.UserId);
            }
        }
    }
}