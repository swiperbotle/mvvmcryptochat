using Microsoft.AspNetCore.Mvc;
namespace WebSocketServer.Controllers
{
    [Route("/")]
    [ApiController]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return RedirectPermanent("https://example.com/new-page");
        }
    }
}