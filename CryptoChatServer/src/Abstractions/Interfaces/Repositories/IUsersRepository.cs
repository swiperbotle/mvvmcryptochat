using Microsoft.EntityFrameworkCore.Diagnostics;
using WebSocketServer.Models;
using WebSocketServer.Enums;

namespace WebSocketServer.Interfaces.Repositories
{
    public interface IUsersRepository : IDisposable
    {
        public Task<ErrorType> AddUser(UserModel userModel);
        public Task<ErrorType> UpdateUser(UserModel userModel);
        public Task<Tuple<ErrorType, byte[]>> GetUser(string username, string password);
        public Task<Tuple<ErrorType, byte[]>> GetUser(string username);
        public Task<ErrorType> RemoveUser(Guid userId);
    }
}