using System.Net.WebSockets;
using WebSocketServer.Helpers;

namespace WebSocketServer.Interfaces.Services
{
    public interface IConnectionsManager
    {
        public void AddItem(Guid userId, WebSocketHelper webSocket);
        public WebSocketHelper GetItem(Guid userId);
        public void RemoveItem(Guid userId);
        public Dictionary<Guid, WebSocketHelper> Connections { get; set; }
        public bool ItemExist(Guid userId);
    }
}