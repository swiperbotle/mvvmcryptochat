using WebSocketServer.Interfaces.Helpers;

namespace WebSocketServer.Interfaces.Services
{
    public interface ISignalingService
    {
        public Task ValidateMessage(byte[] data, int length);
        public List<Tuple<byte[], Guid>> Responces { get;  set; }
    }
}