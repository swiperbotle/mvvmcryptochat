namespace WebSocketServer.Interfaces.Services
{
    public interface IAuthorizeService
    {
        public bool IsAuthorize { get; set; }
        public Guid UserId { get; set; }
        public bool CheckAuthorization(Guid userId);
    }
}