using System.Net.WebSockets;
using WebSocketServer.Helpers;
namespace WebSocketServer.Interfaces.Helpers
{
    public interface IWebSocketHelper
    {
        public Task ReceiveMessageAsync(WebSocket connection, Action<WebSocketReceiveResult, byte[]> handleMessage);
        public Task<WebSocketReceiveResult> ReceiveMessageAsync(WebSocket socket);
        public Task SendMessageAsync(string message);
    }
}