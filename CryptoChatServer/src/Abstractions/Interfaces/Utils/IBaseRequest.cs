namespace WebSocketServer.Interfaces.Utils
{
    public interface IBaseRequest 
    {
        public Task<List<Tuple<byte[], Guid>>> Handle(byte[] data, int length);
    }
}