using Microsoft.EntityFrameworkCore;
using WebSocketServer.ORM.DTOs;

namespace WebSocketServer.Interfaces.Contexts
{
    public interface  IUsersDbContext : IDisposable
    {
        public DbSet<UserDTO> Users { get; set; }
    }
}