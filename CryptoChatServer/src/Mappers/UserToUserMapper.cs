using AutoMapper;
using WebSocketServer.ORM.DTOs;
using WebSocketServer.Models;
namespace WebSocketServer.Mappers
{

    public class UserToUserProfile : Profile
    {
        public UserToUserProfile()
        {
            CreateMap<UserModel, UserDTO>();
            CreateMap<UserDTO, UserModel>();
            CreateMap<UserModel, UserIncompleteModel>();
            CreateMap<UserIncompleteModel, UserModel>();
            CreateMap<UserDTO, UserIncompleteModel>();
        //     .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
        //     .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.Nickname))
        //     .ForMember(dest => dest.Bio, opt => opt.MapFrom(src => src.Bio))
        //     .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
        //     .ForMember(dest => dest.IsPrivate, opt => opt.MapFrom(src => src.IsPrivate));
        
            CreateMap<UserIncompleteModel, UserDTO>();
        //     .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
        //     .ForMember(dest => dest.Nickname, opt => opt.MapFrom(src => src.Nickname))
        //     .ForMember(dest => dest.Bio, opt => opt.MapFrom(src => src.Bio))
        //     .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
        //     .ForMember(dest => dest.IsPrivate, opt => opt.MapFrom(src => src.IsPrivate));
        }
    }
}
