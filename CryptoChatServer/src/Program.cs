using WebSocketServer.Extensions.BuilderExtensions;

var builder = WebApplication.CreateBuilder(args);
builder.AddDbContext().AddMappers().AddLoggers().AddRequests().AddServices().AddRepos().AddGlobals();

var app = builder.Build();
app.UseWebSockets();
app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.MapHealthChecks("/health").RequireHost("localhost:6969", "127.0.0.1:6969", "[::1]:6969");;
app.MapControllers();

await app.RunAsync();
