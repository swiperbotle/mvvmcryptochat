namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddLoggerHostBuilderExtension
    {
        public static WebApplicationBuilder AddLoggers(this WebApplicationBuilder builder)
        {
            builder.Services.AddLogging(logging =>
            {
                logging.ClearProviders();
                logging.AddConsole();
                logging.AddDebug();
            });
            return builder;
        }
    }
}