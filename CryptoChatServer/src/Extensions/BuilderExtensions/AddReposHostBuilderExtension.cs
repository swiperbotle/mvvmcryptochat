using WebSocketServer.Interfaces.Repositories;
using WebSocketServer.Repositories;

namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddReposHostBuilderExtension
    {
        public static WebApplicationBuilder AddRepos(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IUsersRepository, UsersRepository>();
            return builder;
        }
    }
}