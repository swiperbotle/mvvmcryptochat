using Microsoft.EntityFrameworkCore;
using WebSocketServer.Exceptions;
using WebSocketServer.ORM.Contexts;

namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddDbContextsHostBuilderExtension
    {
        public static WebApplicationBuilder AddDbContext(this WebApplicationBuilder builder)
        {
            string? connectionString = builder.Configuration.GetConnectionString("sqlite");
            if (connectionString == null)
            {
                throw new ConfigurationNotFoundException("appsettings.json not found");
            }
            builder.Services.AddDbContextFactory<UsersDbContext>(options => options.UseSqlite(connectionString));
            return builder;
        }
    }
}