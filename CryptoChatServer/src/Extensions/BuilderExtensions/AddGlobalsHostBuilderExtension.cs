using WebSocketServer.Collections;
using WebSocketServer.Interfaces.Services;
using WebSocketServer.Services;
namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddGlobalssHostBuilderExtension
    {
        public static WebApplicationBuilder AddGlobals(this WebApplicationBuilder builder)
        {
            builder.Services.AddSingleton<IConnectionsManager, ConnectionsManager>();
            builder.Services.AddSingleton<IDictionary<Guid, HashSet<Guid>>, AllowedSourcesDict>();
            return builder;
        }
    }
}