using WebSocketServer.Mappers;
namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddMappersHostBuilderExtension
    {
        public static WebApplicationBuilder AddMappers(this WebApplicationBuilder builder)
        {
            builder.Services.AddAutoMapper(typeof(UserToUserProfile));
            return builder;
        }
    }
}