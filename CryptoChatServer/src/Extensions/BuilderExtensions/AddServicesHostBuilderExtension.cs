using WebSocketServer.Interfaces.Services;
using WebSocketServer.Services;

namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddServicesHostBuilderExtension
    {
        public static WebApplicationBuilder AddServices(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IAuthorizeService, AuthorizeService>();
            builder.Services.AddScoped<ISignalingService, SignalingService>();
            builder.Services.AddSingleton<MessageIdIssuerService, MessageIdIssuerService>();
            builder.Services.AddHostedService<ApplicationLifetimeService>();
            builder.Services.AddControllers();
            builder.Services.AddHealthChecks();
            return builder;
        }
    }
}