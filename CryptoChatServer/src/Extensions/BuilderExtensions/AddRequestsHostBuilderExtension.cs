using WebSocketServer.Interfaces.Utils;
using WebSocketServer.Utils.Requests;

namespace WebSocketServer.Extensions.BuilderExtensions
{
    public static class AddRequestsHostBuilderExtension
    {
        public static WebApplicationBuilder AddRequests(this WebApplicationBuilder builder)
        {
            builder.Services.AddTransient<SignUpRequest, SignUpRequest>();
            builder.Services.AddTransient<SignInRequest, SignInRequest>();
            builder.Services.AddTransient<UpdateRequest, UpdateRequest>();
            builder.Services.AddTransient<RemoveRequest, RemoveRequest>();
            builder.Services.AddTransient<ConversationAcceptResponse, ConversationAcceptResponse>();
            builder.Services.AddTransient<ConversationRequest, ConversationRequest>();
            builder.Services.AddTransient<GetMessageIdRequest, GetMessageIdRequest>();
            builder.Services.AddTransient<SearchRequest, SearchRequest>();
            return builder;
        }
    }
}