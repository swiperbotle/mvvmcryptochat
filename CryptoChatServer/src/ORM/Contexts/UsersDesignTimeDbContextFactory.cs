using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace WebSocketServer.ORM.Contexts
{
    public class UsersDesignTimeDbContextFactory : IDesignTimeDbContextFactory<UsersDbContext>
    {
        public UsersDbContext CreateDbContext(string[]? args = null)
        {
            return new UsersDbContext(new DbContextOptionsBuilder().UseSqlite("Data Source=users.db").Options);
        }
    }
}
