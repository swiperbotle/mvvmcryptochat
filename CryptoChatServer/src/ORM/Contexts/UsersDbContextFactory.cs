using Microsoft.EntityFrameworkCore;
namespace WebSocketServer.ORM.Contexts
{
    public class UsersDbContextFactory : IDbContextFactory<UsersDbContext>

    {
        private readonly DbContextOptions _options;

        public UsersDbContextFactory(DbContextOptions options)
        {
            _options = options;
        }

        public virtual UsersDbContext CreateDbContext()
        {
            return new UsersDbContext(_options);
        }
    }
}