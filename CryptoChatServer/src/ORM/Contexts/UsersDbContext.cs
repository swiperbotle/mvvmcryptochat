using Microsoft.EntityFrameworkCore;
using WebSocketServer.ORM.DTOs;
using WebSocketServer.Interfaces.Contexts;
namespace WebSocketServer.ORM.Contexts
{
    public class UsersDbContext : DbContext, IUsersDbContext
    {
        public UsersDbContext(DbContextOptions options) : base(options)
        {
        }
        public virtual DbSet<UserDTO> Users { get; set; }
    }
}
