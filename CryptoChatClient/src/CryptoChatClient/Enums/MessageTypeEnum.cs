﻿namespace CryptoChatClient.Enums
{
    public enum MessageType : int
    {
        #region message_types
        #region requests
        SignUp_Request = 0xD000,
        SignIn_Request = 0xD001,
        Update_Request = 0xD002,
        Remove_Request = 0xD003,
        Conversation_Request = 0xD004,
        STUN_Connect_Request = 0xD005,
        TURN_Connect_Request = 0xD006,
        Get_MessageId_Request = 0xD007,
        Search_Request = 0xD008,
        #endregion
        #region responses
        Error_Response = 0xFFFF,
        SignUp_Success_Response = 0xD101,
        SignIn_Success_Response = 0xD102,
        Update_Success_Response = 0xD103,
        Remove_Success_Response = 0xD104,
        Conversation_Success_Response = 0xD105,
        Conversation_Accept_Response = 0xD106,
        Conversation_Decline_Response = 0xD107,
        Get_MessageId_Success = 0xD108,
        Search_Success_Response = 0xD109,
        #endregion
        #endregion
    }
}
