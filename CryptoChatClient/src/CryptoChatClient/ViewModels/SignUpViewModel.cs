﻿using CryptoChatClient.Commands.CommonCommands;
using CryptoChatClient.Commands.ComponentCommands.LogIn;
using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.POCOs;
using System;
using System.IO;
using System.Text.Json;

namespace CryptoChatClient.ViewModels
{
    public class SignUpViewModel : ViewModelBase
    {
        private readonly IListenerService _listener;
        private INavigationService? _navigation;
        public INavigationService Navigation
        {
            get => _navigation!;
            set
            {
                _navigation = value;
                OnPropertyChanged();
            }
        }
        public CommandBase NavigateToSignInCommand { get; set; }
        public AsyncCommandBase SignUpCommand { get; set; }
        public SignUpViewModel(INavigationService navigation, IListenerService listenerService)
        {
            _listener = listenerService;
            Navigation = navigation;
            NavigateToSignInCommand = new NavigateToCommand<SignInViewModel>(navigation);
            var appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CryptoChat");
            if (!Directory.Exists(appDataPath))
            {
                try
                {
                    Directory.CreateDirectory(appDataPath);
                }
                catch (Exception ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            }
            var serversPath = Path.Combine(appDataPath, "servers.json");
            var jsonData = File.ReadAllText(serversPath);
            var settingsServers = JsonSerializer.Deserialize<ServerSettings>(jsonData)!;
            var signalingServerIp = settingsServers.SignalingIp;
            var stunServerIp = settingsServers.StunIp;
            var stunServerPort = settingsServers.StunPort;
            SignUpCommand = new SignUpCommand(navigation, _listener, signalingServerIp, stunServerIp, stunServerPort);
        }

    }
}
