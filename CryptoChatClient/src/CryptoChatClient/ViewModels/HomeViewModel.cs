﻿using CryptoChatClient.Commands.CommonCommands;
using CryptoChatClient.Commands.ComponentCommands.Home;
using CryptoChatClient.Core;
using CryptoChatClient.Models;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CryptoChatClient.ViewModels
{
    public class HomeViewModel : ViewModelBase
    {

        public ChatViewModels<Guid, ChatObservableModel> Chats { get; set; }
        public UserViewModels<Guid, UserObservableModel> SearchResult { get; set; }
        public Dictionary<Guid, MessageModels> MessageChatModels { get; set; }
        Task _listenTask;

        // by MessageId
        public MessageViewModels<Int64, MessageObservableModel>? currentMessageViewModels;

        public MessageViewModels<Int64, MessageObservableModel>? CurrentMessageViewModels
        {
            get { return currentMessageViewModels; }
            set
            {
                if (currentMessageViewModels != value)
                {
                    currentMessageViewModels = value;
                    OnPropertyChanged(nameof(CurrentMessageViewModels));
                }
            }
        }

        private readonly IListenerService _listener;
        private INavigationService? _navigation;
        private readonly CryptoChatRepository _dbRepository;
        private Color? _colorPicked;
        public Color? ColorPicked
        {
            get => _colorPicked;
            set
            {
                _colorPicked = value;
                OnPropertyChanged(nameof(ColorPicked));
            }
        }
        private string _inputChangeUsername;
        public string InputChangeUsername
        {
            get => _inputChangeUsername;
            set
            {
                _inputChangeUsername = value;
                OnPropertyChanged(nameof(InputChangeUsername));
            }
        }
        private CurrentUserModel _currentUser;
        public CurrentUserModel CurrentUser { get => _currentUser; set { _currentUser = value; OnPropertyChanged(nameof(CurrentUser)); } }
        private string _searchInputText;
        private string _messageInputText;
        public string SearchInputText
        {
            get
            {
                return _searchInputText;
            }
            set
            {
                if (_searchInputText != value)
                {
                    _searchInputText = value;
                    OnPropertyChanged(nameof(SearchInputText));
                }
            }
        }
        public string MessageInputText
        {
            get
            {
                return _messageInputText;
            }
            set
            {
                if (_messageInputText != value)
                {
                    _messageInputText = value;
                    OnPropertyChanged(nameof(MessageInputText));
                }
            }
        }
        public INavigationService? Navigation
        {
            get => _navigation;
            set
            {
                _navigation = value;
                OnPropertyChanged();
            }
        }

        #region commands
        public AsyncCommandBase LogOutCommand { get; set; }
        public AsyncCommandBase SendMessageCommand { get; set; }
        public CommandBase SelectChatCommand { get; set; }
        public CommandBase DebugCommand { get; set; }
        public CommandBase CloseInfoMenuCommand { get; set; }
        public CommandBase OpenInfoMenuCommand { get; set; }
        public CommandBase CloseSettingsMenuCommand { get; set; }
        public CommandBase OpenSettingsMenuCommand { get; set; }
        public CommandBase OnScrollMessagesCommand { get; set; }
        public AsyncCommandBase SearchCommand { get; set; }
        public CommandBase CloseSearchMenuCommand { get; set; }
        public CommandBase OpenUserMenuPopupCommand { get; set; }
        public AsyncCommandBase AddNewChatCommand { get; set; }
        public AsyncCommandBase SavePasswordChangeCommand { get; set; }
        public AsyncCommandBase ApplyUserDeleteCommand { get; set; }
        public CommandBase OpenDeleteUserConfirmationCommand { get; set; }
        public CommandBase OpenChangePasswordCommand { get; set; }
        public CommandBase CloseWindowCommand { get; set; }
        public AsyncCommandBase SaveChangesCommand { get; set; }
        #endregion

        #region methods
        public HomeViewModel(INavigationService navigation, IListenerService listenerService, CryptoChatRepository dbRepository)
        {
            _listener = listenerService;
            _listenTask = _listener.ListenAsync(this);
            _dbRepository = dbRepository;
            this.CloseSearchMenuCommand = new CloseSearchMenuCommand();
            this.AddNewChatCommand = new AddNewChatCommand(this);
            this.OpenUserMenuPopupCommand = new OpenUserMenuPopupCommand();
            this.SearchResult = new UserViewModels<Guid, UserObservableModel>();
            this.MessageChatModels = new Dictionary<Guid, MessageModels>();
            this.CurrentMessageViewModels = new MessageViewModels<Int64, MessageObservableModel>();
            this.Chats = new ChatViewModels<Guid, ChatObservableModel>();
            this.Navigation = navigation;
            this.CurrentUser = new CurrentUserModel();
            this.LogOutCommand = new LogOutCommand(navigation, this, _dbRepository);
            this.SendMessageCommand = new SendMessageCommand(this, this.MessageChatModels, this.CurrentUser, _listener);
            this.SelectChatCommand = new SelectChatCommand(this, this.MessageChatModels);
            this.DebugCommand = new DebugCommand();
            this.OnScrollMessagesCommand = new OnScrollMessagesCommand(this, MessageChatModels);
            this.OpenInfoMenuCommand = new OpenInfoMenuCommand();
            this.CloseInfoMenuCommand = new CloseInfoMenuCommand();
            this.SearchCommand = new SearchCommand(this, _listener);
            this.CloseSettingsMenuCommand = new CloseSettingsMenuCommand();
            this.OpenSettingsMenuCommand = new OpenSettingsMenuCommand(this);
            this.SavePasswordChangeCommand = new SavePasswordChangeCommand(listenerService, this, dbRepository);
            this.ApplyUserDeleteCommand = new ApplyUserDeleteCommand(listenerService, this);
            this.OpenDeleteUserConfirmationCommand = new OpenDeleteUserConfirmationCommand(this);
            this.OpenChangePasswordCommand = new OpenChangePasswordCommand(this);
            this.CloseWindowCommand = new CloseWindowCommand();
            this.SaveChangesCommand = new SaveChangesCommand(listenerService, this, dbRepository);
        }

        public async Task Flush()
        {
            await _listener.CloseGracefully();
            await _listenTask;
        }
        #endregion
    }
}
