﻿using CryptoChatClient.Commands.CommonCommands;
using CryptoChatClient.Commands.ComponentCommands.Home;
using CryptoChatClient.Commands.ComponentCommands.LogIn;
using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.POCOs;
using CryptoChatClient.Repositories;
using System;
using System.IO;
using System.Text.Json;

namespace CryptoChatClient.ViewModels
{
    public class SignInViewModel : ViewModelBase
    {
        private readonly IListenerService _listener;
        private INavigationService? _navigation;
        private readonly CryptoChatRepository _dbRepository;
        private readonly string _defaultSignalingIp = "192.168.0.106";
        private readonly string _defaultStunIp = "192.168.0.107";
        private readonly int _defaultStunPort = 3478;
        public INavigationService? Navigation
        {
            get => _navigation!;
            set
            {
                _navigation = value;
                OnPropertyChanged();
            }
        }
        public AsyncCommandBase SignInCommand { get; set; }
        public AsyncCommandBase AutoLoginCommand { get; set; }
        public CommandBase NavigateToSignUpCommand { get; set; }
        public SignInViewModel(INavigationService navigation, IListenerService listenerService, CryptoChatRepository dbRepository)
        {
            var signalingServerIp = "";
            var stunServerIp = "";
            var stunServerPort = 0;
            _listener = listenerService;
            _dbRepository = dbRepository;
            Navigation = navigation;
            NavigateToSignUpCommand = new NavigateToCommand<SignUpViewModel>(navigation);
            var appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CryptoChat");
            if (!Directory.Exists(appDataPath))
            {
                try
                {
                    Directory.CreateDirectory(appDataPath);
                }
                catch (Exception ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            }
            var serversPath = Path.Combine(appDataPath, "servers.json");
            if (!File.Exists(serversPath))
            {
                try
                {
                    using (File.Create(serversPath)) { }
                    var servers = new ServerSettings
                    {
                        SignalingIp = _defaultSignalingIp,
                        StunIp = _defaultStunIp,
                        StunPort = _defaultStunPort
                    };
                    string json = JsonSerializer.Serialize(servers);
                    File.WriteAllText(serversPath, json);
                    signalingServerIp = _defaultSignalingIp;
                    stunServerIp = _defaultStunIp;
                    stunServerPort = _defaultStunPort;
                }
                catch (Exception ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            }
            else
            {
                var jsonData = File.ReadAllText(serversPath);
                var settingsServers = JsonSerializer.Deserialize<ServerSettings>(jsonData)!;
                signalingServerIp = settingsServers.SignalingIp;
                stunServerIp = settingsServers.StunIp;
                stunServerPort = settingsServers.StunPort;
            }
            SignInCommand = new SignInCommand(Navigation, _listener, _dbRepository, signalingServerIp, stunServerIp, stunServerPort);
            AutoLoginCommand = new AutoLoginCommand(_dbRepository, _listener, Navigation, signalingServerIp, stunServerIp, stunServerPort);
        }

    }
}
