﻿using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Linq;

namespace CryptoChatClient.ViewModels
{
    public class MessageViewModels<TKey, TValue> : ObservableDictionary<TKey, TValue> where TValue : MessageObservableModel
    {
        public Guid ChatId { get; set; }
        public new TValue this[TKey key]
        {
            get => base[key];
            set
            {
                var tmpVal = base[key];
                value.index = tmpVal.index;
                base[key] = value;
                OnPropertyChanged("Item[]");
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, tmpVal, value.index));
            }
        }

        public new void Add(TKey key, TValue value)
        {
            ++_indexCount;
            value.index = _indexCount;
            try
            {
                base.Add(key, value);
                OnPropertyChanged("Count");
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value));
            }
            catch (System.ArgumentException ex)
            {
                ShowErrorHelper.ShowError(ex);
            }
        }

        public new bool Remove(TKey key)
        {
            if (base.ContainsKey(key))
            {
                var value = base[key];
                var index = base[key].index;
                var isLast = (index == _indexCount);
                base.Remove(key);
                --_indexCount;
                OnPropertyChanged("Count");
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, value, index));
                if (!isLast)
                    RecalculateIndex(index);
                return true;
            }
            return false;
        }
        public void AddMultiple(OrderedDictionary currentMessageModel)
        {
            //int max_messages = 150;
            //int counter = 0;
            foreach (var pair in currentMessageModel.Cast<DictionaryEntry>())
            {
                this.Add((TKey)pair.Key, (TValue)pair.Value);
                //counter++;
                //if (counter == max_messages)
                //{
                //    break;
                //}
            }
        }
    }
}
