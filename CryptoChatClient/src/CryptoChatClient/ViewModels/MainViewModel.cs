﻿using CryptoChatClient.Commands.ComponentCommands.Window;
using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using System;
using System.IO;
namespace CryptoChatClient.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private INavigationService? _navigation;
        public INavigationService? Navigation
        {
            get => _navigation;
            set
            {
                _navigation = value;
                OnPropertyChanged();
            }

        }

        //public CommandBase? NavigateToIntroCommand { get; set; }
        public CommandBase? WindowMouseLeftButtonDownCommand { get; set; }
        public CommandBase? WindowMaximizeButtonCommand { get; set; }
        public CommandBase? WindowMinimizeButtonCommand { get; set; }
        public CommandBase? WindowCloseButtonCommand { get; set; }
        public CommandBase MainWindowInitCommand { get; set; }
        public MainViewModel(INavigationService navigation)
        {
            this.Navigation = navigation;
            this.WindowMouseLeftButtonDownCommand = new WindowMouseLeftButtonDownCommand();
            this.WindowMaximizeButtonCommand = new WindowMaximizeButtonCommand();
            this.WindowMinimizeButtonCommand = new WindowMinimizeButtonCommand();
            var appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "CryptoChat");
            if (!Directory.Exists(appDataPath))
            {
                try
                {
                    Directory.CreateDirectory(appDataPath);
                }
                catch (Exception ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            }
            var windowSettingsPath = Path.Combine(appDataPath, "window.json");
            if (!File.Exists(windowSettingsPath))
            {
                try
                {
                    File.Create(windowSettingsPath);
                }
                catch (Exception ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            }
            else
            {
                this.MainWindowInitCommand = new MainWindowInitCommand(windowSettingsPath);
            }
            this.WindowCloseButtonCommand = new WindowCloseButtonCommand(windowSettingsPath);
            Navigation.NavigateTo<SignInViewModel>();
            //NavigateToIntroCommand = new RelayCommand(o => { Navigation.NavigateTo<IntroViewModel>(); }, o => true);
        }
    }
}
