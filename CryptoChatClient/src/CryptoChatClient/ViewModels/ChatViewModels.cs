﻿using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using System.Collections.Specialized;
namespace CryptoChatClient.ViewModel
{
    public class ChatViewModels<TKey, TValue> : ObservableDictionary<TKey, TValue> where TValue : ChatObservableModel
    {
        public new TValue this[TKey key]
        {
            get => base[key];
            set
            {
                var tmpVal = base[key];
                value.index = tmpVal.index;
                base[key] = value;
                OnPropertyChanged("Item[]");
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, tmpVal, value.index));
            }
        }

        public new void Add(TKey key, TValue value)
        {
            ++_indexCount;
            value.index = _indexCount;
            try
            {
                base.Add(key, value);

            }
            catch (System.ArgumentException ex)
            {
                ShowErrorHelper.ShowError(ex);
                return;
            }
            OnPropertyChanged("Count");
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, value));
        }

        public new bool Remove(TKey key)
        {
            if (base.ContainsKey(key))
            {
                var value = base[key];
                var index = base[key].index;
                var isLast = (index == _indexCount);
                base.Remove(key);
                --_indexCount;
                OnPropertyChanged("Count");
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, value, index));
                if (!isLast)
                    RecalculateIndex(index);
                return true;
            }
            return false;
        }
    }
}