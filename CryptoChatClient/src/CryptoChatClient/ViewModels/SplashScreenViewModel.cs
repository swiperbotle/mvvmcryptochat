﻿using CryptoChatClient.Commands.ComponentCommands.Window;
using CryptoChatClient.Core;

namespace CryptoChatClient.ViewModels
{
    public class SplashScreenViewModel
    {
        public CommandBase WindowCloseWithDelayCommand { get; set; }

        public SplashScreenViewModel()
        {
            this.WindowCloseWithDelayCommand = new WindowCloseWithDelayCommand(2);
        }

    }
}
