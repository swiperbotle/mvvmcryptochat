﻿using CryptoChatClient.Core;
using System;
using System.Security.Cryptography;
using System.Text;

namespace CryptoChatClient.Helpers
{
    public class HashPasswordHelper : IHashPasswordHelper
    {
        private readonly byte[] salt = Encoding.UTF8.GetBytes("zeitgeist");
        private const int keySize = 64;
        private const int iterations = 350000;
        private readonly HashAlgorithmName hashAlgorithm = HashAlgorithmName.SHA512;
        public string HashPassword(string password)
        {
            var hash = Rfc2898DeriveBytes.Pbkdf2(
                Encoding.UTF8.GetBytes(password),
                salt,
                iterations,
                hashAlgorithm,
                keySize);
            return Convert.ToBase64String(hash);
        }
        public bool VerifyPassword(string password, string hashedPassword)
        {
            password = this.HashPassword(password);
            return password == hashedPassword ? true : false;
        }
    }
}
