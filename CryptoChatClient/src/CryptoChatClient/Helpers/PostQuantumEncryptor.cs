﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Pqc.Crypto.Crystals.Kyber;
using Org.BouncyCastle.Security;
using System;

namespace CryptoChatClient.Helpers
{
    public class PostQuantumEncryptor
    {
        public byte[]? Secret { get; set; }
        private readonly KyberPublicKeyParameters? _keyPub;
        private readonly KyberPrivateKeyParameters? _keyPriv;
        private readonly ISecretWithEncapsulation? encapsulatedSecret;
        public PostQuantumEncryptor()
        {
            var random = new SecureRandom();
            var keyGenParameters = new KyberKeyGenerationParameters(random, KyberParameters.kyber1024);
            var kyberKeyPairGenerator = new KyberKeyPairGenerator();
            kyberKeyPairGenerator.Init(keyGenParameters);
            var keyPair = kyberKeyPairGenerator.GenerateKeyPair();
            _keyPub = (KyberPublicKeyParameters)keyPair.Public;
            _keyPriv = (KyberPrivateKeyParameters)keyPair.Private;
        }
        public PostQuantumEncryptor(byte[]? pubKeyBytes)
        {
            if (pubKeyBytes == null) throw new ArgumentNullException(nameof(pubKeyBytes));
            var random = new SecureRandom();
            var kyberKemGenerator = new KyberKemGenerator(random);
            var pubKey = new KyberPublicKeyParameters(KyberParameters.kyber1024, pubKeyBytes);
            this.encapsulatedSecret = kyberKemGenerator.GenerateEncapsulated(pubKey);
            this.Secret = this.encapsulatedSecret.GetSecret();
        }
        public byte[]? GetCipherText()
        {
            return encapsulatedSecret != null ? this.encapsulatedSecret.GetEncapsulation() : null;
        }
        public byte[]? GetPublicKey()
        {
            return _keyPub != null ? _keyPub.GetEncoded() : null;
        }
        public bool ExctractSecret(byte[]? cipherText)
        {
            if (_keyPriv == null)
                return false;
            var kemExtractor = new KyberKemExtractor(_keyPriv);
            Secret = kemExtractor.ExtractSecret(cipherText);
            return true;
        }
    }
}
