﻿using System;
using System.Windows;
namespace CryptoChatClient.Helpers
{
    public static class ShowErrorHelper
    {
        public static void ShowError(Exception ex)
        {
            MessageBox.Show($"An error occurred: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void ShowDebug(string debugStr = "Debug info is printed")
        {
            MessageBox.Show($"{debugStr}");
        }
    }
}
