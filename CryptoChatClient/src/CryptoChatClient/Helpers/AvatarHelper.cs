﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace CryptoChatClient.Helpers
{
    public static class AvatarHelper
    {
        public static string CreateRandomBitMap()
        {
            int width = 300;
            int height = 200;
            var bitmap = new System.Drawing.Bitmap(width, height);
            var random = new Random();
            var randomColor = System.Windows.Media.Color.FromRgb((byte)random.Next(255), (byte)random.Next(255), (byte)random.Next(255));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var drawingColor = System.Drawing.Color.FromArgb(
             randomColor.R, randomColor.G, randomColor.B);
                    bitmap.SetPixel(x, y, drawingColor);
                }
            }
            return BitmapToBase64(bitmap);
        }
        public static string CreateBitmapFromColor(System.Windows.Media.Color color)
        {
            int width = 300;
            int height = 200;
            var bitmap = new System.Drawing.Bitmap(width, height);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    var drawingColor = System.Drawing.Color.FromArgb(
             color.R, color.G, color.B);
                    bitmap.SetPixel(x, y, drawingColor);
                }
            }
            return BitmapToBase64(bitmap);
        }
        public static ImageBrush InjectBitmap(string bitmapB64)
        {
            var bitmap = Base64ToBitmap(bitmapB64);
            var bitmapSource = ConvertBitmapToBitmapSource(bitmap);
            var imageBrush = new ImageBrush(bitmapSource);
            return imageBrush;
        }


        private static string BitmapToBase64(Bitmap bitmap)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bitmap.Save(memoryStream, ImageFormat.Png);
                byte[] imageBytes = memoryStream.ToArray();
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        private static Bitmap Base64ToBitmap(string base64String)
        {
            byte[] imageBytes = Convert.FromBase64String(base64String);
            using (MemoryStream memoryStream = new MemoryStream(imageBytes))
            {
                Bitmap bitmap = new Bitmap(memoryStream);
                return bitmap;
            }
        }
        private static BitmapSource ConvertBitmapToBitmapSource(Bitmap bitmap)
        {
            using (var memoryStream = new System.IO.MemoryStream())
            {
                bitmap.Save(memoryStream, ImageFormat.Png);
                memoryStream.Position = 0;
                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();
                bitmapImage.Freeze(); // Freeze the BitmapImage for cross-thread use
                return bitmapImage;
            }
        }
    }
}
