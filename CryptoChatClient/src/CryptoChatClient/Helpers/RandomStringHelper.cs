﻿using System;

namespace CryptoChatClient.Helpers
{
    public static class RandomStringHelper
    {
        public static string CreateRandomStr(int stringlen)
        {
            Random rand = new Random();
            int randValue;
            string str = "";
            char letter;
            for (int i = 0; i < stringlen; i++)
            {
                randValue = rand.Next(0, 26);
                letter = Convert.ToChar(randValue + 65);
                str = str + letter;
            }
            return str;
        }
    }
}
