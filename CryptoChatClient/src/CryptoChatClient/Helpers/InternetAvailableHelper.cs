﻿using System.Net.NetworkInformation;
using System.Threading.Tasks;

namespace CryptoChatClient.Helpers
{
    public static class InternetAvailableHelper
    {
        private const string TestAddress = "8.8.8.8"; // Google's public DNS server
        public static async Task<bool> IsInternetAvailable(string? ipAddress = null)
        {
            try
            {
                var testAdress = ipAddress ?? TestAddress;
                using (Ping ping = new Ping())
                {
                    PingReply reply = await ping.SendPingAsync(testAdress, 1000);
                    return reply.Status == IPStatus.Success;
                }
            }
            catch
            {
                return false;
            }
        }
    }
}
