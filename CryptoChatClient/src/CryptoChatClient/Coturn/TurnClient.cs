﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CryptoChatClient.Coturn
{
    public class TurnClient : StunClient
    {
        protected static readonly byte[] ALLOCATE_REQUEST = new byte[] { 0x00, 0x03 };
        protected static readonly byte[] ALLOCATE_RESPONSE = new byte[] { 0x01, 0x03 };
        protected static readonly byte[] ALLOCATE_ERROR_RESPONSE = new byte[] { 0x01, 0x13 };
        protected static readonly byte[] REFRESH_REQUEST = new byte[] { 0x00, 0x04 };
        protected static readonly byte[] REFRESH_RESPONSE = new byte[] { 0x01, 0x04 };
        protected static readonly byte[] SEND_INDICATION = new byte[] { 0x00, 0x16 };
        protected static readonly byte[] DATA_INDICATION = new byte[] { 0x00, 0x17 };
        protected static readonly byte[] CREATE_PERMISSION_REQUEST = new byte[] { 0x00, 0x08 };
        protected static readonly byte[] CREATE_PERMISSION_RESPONSE = new byte[] { 0x01, 0x08 };
        protected static readonly byte[] CHANNEL_BIND_REQUEST = new byte[] { 0x00, 0x09 };
        protected static readonly byte[] CHANNEL_BIND_RESPONSE = new byte[] { 0x01, 0x09 };

        // Turn message attributes
        protected static readonly byte[] CHANNEL_NUMBER = new byte[] { 0x00, 0x0c };
        protected static readonly byte[] LIFETIME = new byte[] { 0x00, 0x0d };
        protected static readonly byte[] BANDWIDTH = new byte[] { 0x00, 0x10 };
        protected static readonly byte[] XOR_PEER_ADDRESS = new byte[] { 0x00, 0x12 };
        protected static readonly byte[] DATA = new byte[] { 0x00, 0x13 };
        protected static readonly byte[] XOR_RELAYED_ADDRESS = new byte[] { 0x00, 0x16 };
        protected static readonly byte[] EVEN_PORT = new byte[] { 0x00, 0x18 };
        public static readonly byte[] REQUESTED_TRANSPORT = new byte[] { 0x00, 0x19 };
        protected static readonly byte[] DONT_FRAGMENT = new byte[] { 0x00, 0x1a };
        protected static readonly byte[] TIMER_VAL = new byte[] { 0x00, 0x21 };
        protected static readonly byte[] RESERVATION_TOKEN = new byte[] { 0x00, 0x22 };

        // transports
        protected static readonly byte[] UDP_TRANSPORT = new byte[] { 0x11, 0x00, 0x00, 0x00 };

        public string? RelayedIp { get; private set; }
        public uint RelayedPort { get; private set; }

        public static byte[] ParseData(byte[] recv)
        {
            int i = 20;
            byte[] data = Array.Empty<byte>();
            while (i < recv.Length)
            {
                byte[] tlChunk = recv.Skip(i).Take(4).ToArray();
                byte[] attributeType = tlChunk.Take(2).ToArray();
                int attributeLength = BitConverter.ToUInt16(tlChunk.Skip(2).Reverse().ToArray());
                byte[] attributeValue = recv.Skip(i + 4).Take(attributeLength).ToArray();
                if (attributeType.SequenceEqual(DATA))
                {
                    data = attributeValue;
                }
                i += attributeLength + 4;
            }
            return data;
        }

        public byte[] FormingLifetimeAttribute(int? lifetime)
        {
            if (lifetime.HasValue)
                return LIFETIME.Concat(new byte[] { 0x00, 0x04 }).Concat(BitConverter.GetBytes(lifetime.Value).Reverse()).ToArray();
            return Array.Empty<byte>();
        }

        public byte[] FormingRequestedTransportAttribute(byte[] transportValue)
        {
            return REQUESTED_TRANSPORT.Concat(BitConverter.GetBytes((ushort)transportValue.Length).Reverse()).Concat(transportValue).ToArray();
        }

        public byte[] FormingXorPeerAddressAttribute(string ip, uint port)
        {
            byte[] portBytes = BitConverter.GetBytes((ushort)(port ^ (MAGIC_COOKIE >> 16))).Reverse().ToArray();
            // fix that 
            byte[] ipBytes = BitConverter.GetBytes((BitConverter.ToUInt32(IPAddress.Parse(ip).GetAddressBytes().Reverse().ToArray())) ^ MAGIC_COOKIE).Reverse().ToArray();
            byte[] family = new byte[] { 0x00, 0x01 };
            byte[] value = family.Concat(portBytes).Concat(ipBytes).ToArray();
            return XOR_PEER_ADDRESS.Concat(BitConverter.GetBytes((ushort)value.Length).Reverse()).Concat(value).ToArray();
        }

        public byte[] FormingDataAttribute(byte[] data)
        {
            data = data.Concat(base.AddPadding(data.Length)).ToArray();
            return DATA.Concat(BitConverter.GetBytes((ushort)data.Length).Reverse()).Concat(data).ToArray();
        }

        public byte[] FormingChannelNumberAttribute(int channelNumber)
        {
            return CHANNEL_NUMBER.Concat(BitConverter.GetBytes((ushort)2).Reverse()).Concat(BitConverter.GetBytes((ushort)channelNumber).Reverse())
            .Concat(new byte[] { 0x00, 0x00 }).ToArray();
        }

        public byte[] FormingChannelMessage(int channelNumber, byte[] data)
        {
            return BitConverter.GetBytes((ushort)channelNumber).Reverse().Concat(BitConverter.GetBytes((ushort)data.Length).Reverse())
            .Concat(data).ToArray();
        }

        public byte[] FormingEvenPortAttribute(bool isSet)
        {
            if (isSet)
                return EVEN_PORT.Concat(BitConverter.GetBytes((ushort)0x1).Reverse()).Concat(new byte[] { 0x80 }).Concat(base.AddPadding(1)).ToArray();
            return Array.Empty<byte>();
        }

        public byte[] FormingDontFragmentAttribute(bool isSet)
        {
            if (isSet)
                return DONT_FRAGMENT.Concat(new byte[] { 0x00, 0x00 }).ToArray();
            return Array.Empty<byte>();
        }

        public TurnClient(string srcIp, int srcPort, bool isMgcCookie = true, bool isFingerprint = false,
                          string username = "", string password = "", string realmValue = "", byte[]? nonce = null)
            : base(srcIp, srcPort, isMgcCookie, isFingerprint, username, password, realmValue, nonce)
        {
        }

        public async Task<Tuple<byte[]?, EndPoint?>> TurnRawRequest(string dstIp, uint dstPort,
                                               byte[]? attributes = null,
                                               byte[]? msgType = null, byte[]? transId = null)
        {
            msgType = msgType ?? ALLOCATE_REQUEST;
            (byte[]? recv, EndPoint? addr) = await StunRequestRaw(dstIp, dstPort,
                                                                  attributes: attributes,
                                                                  msgType: msgType, transId: transId);
            if (recv != null && addr != null)
            {
                return new Tuple<byte[]?, EndPoint?>(recv, addr);
            }
            else
            {
                return new Tuple<byte[]?, EndPoint?>(null, null);
            }
        }

        public Tuple<string[]?, string?, uint, string?, uint> ProcessTurnAllocateResponse(byte[] recv)
        {
            List<string>? res = new List<string>();
            bool isXorMappedAddress = false;
            int i = 20;
            string? reflexiveIp = null;
            uint reflexivePort = 0;
            string? relayedIp = null;
            uint relayedPort = 0;
            while (i < recv.Length)
            {
                byte[] tlChunk = recv[i..(i + 4)];
                byte[] attributeType = tlChunk[..2];
                ushort attributeLength = BitConverter.ToUInt16(tlChunk[2..].Reverse().ToArray());
                byte[] attributeValue = recv[(i + 4)..(i + 4 + attributeLength)];
                if (attributeType.SequenceEqual(MAPPED) && !isXorMappedAddress)
                {
                    reflexivePort = BitConverter.ToUInt16(attributeValue[2..4].Reverse().ToArray());
                    reflexiveIp = new IPAddress(attributeValue[4..].Reverse().ToArray()).ToString();

                }
                else if (attributeType.SequenceEqual(XOR_MAPPED))
                {
                    (reflexiveIp, reflexivePort) = GetXORAddressFromBytes(attributeValue[4..], attributeValue[2..4]);
                    isXorMappedAddress = true;
                }
                else if (attributeType.SequenceEqual(SOFTWARE))
                {
                    res.Add($"Software information: {Encoding.UTF8.GetString(attributeValue)}");
                }
                else if (attributeType.SequenceEqual(XOR_RELAYED_ADDRESS))
                    (relayedIp, relayedPort) = GetXORAddressFromBytes(attributeValue[4..], attributeValue[2..4]);
                i += attributeLength + 4;
            }
            RelayedIp = relayedIp;
            RelayedPort = relayedPort;
            return new Tuple<string[]?, string?, uint, string?, uint>(res.ToArray(), reflexiveIp, reflexivePort, relayedIp, relayedPort);
        }

        public async Task<Tuple<string[]?, string?, uint, string?, uint>> TurnAllocateRequest(string dstIp, uint dstPort, int? lifetime = null,
                                  bool evenPort = false, bool dontFragment = false)
        {
            var turnRes = await TurnRawRequest(dstIp, dstPort, msgType: ALLOCATE_REQUEST, attributes:
                 FormingEvenPortAttribute(evenPort).Concat(FormingLifetimeAttribute(lifetime)).Concat(FormingDontFragmentAttribute(dontFragment)).ToArray());
            byte[] recv = turnRes.Item1!;
            ValidateHeaderResponse(recv[..20], ALLOCATE_ERROR_RESPONSE);
            bool isUnauthorized = false;
            string errorStr = "No errors found, try using turn allocate request raw with passing necessary attributes";
            int i = 20;
            while (i < recv.Length)
            {
                byte[] tlChunk = recv[i..(i + 4)];
                byte[] attributeType = tlChunk[..2];
                ushort attributeLength = BitConverter.ToUInt16(tlChunk[2..4].Reverse().ToArray());
                byte[] attributeValue = recv[(i + 4)..(i + 4 + attributeLength)];
                byte[] tlvChunk = recv[i..(i + 4 + attributeLength)];
                if (attributeType.SequenceEqual(ERROR))
                {
                    int errorClass = attributeValue[2];
                    int errorNumber = attributeValue[3];
                    int errorCode = errorClass * 100 + errorNumber;
                    string errorReason = Encoding.UTF8.GetString(attributeValue[4..]);
                    errorStr = $"Get error response. Error code {errorCode}. Cause: {errorReason}";
                    isUnauthorized = errorCode == 401;
                }
                else if (attributeType.SequenceEqual(NONCE))
                {
                    Nonce = tlvChunk;
                }
                else if (attributeType.SequenceEqual(REALM))
                {
                    RealmValue = Encoding.UTF8.GetString(attributeValue);
                    Realm = tlvChunk;
                    Realm = Realm.Concat(AddPadding(attributeLength)).ToArray();
                }
                i += attributeLength + 4 + (4 - attributeLength % 4) % 4;
            }
            if (!isUnauthorized)
                throw new StunError(errorStr);

            turnRes = await TurnRawRequest(dstIp, dstPort,
                FormingRequestedTransportAttribute(UDP_TRANSPORT).Concat(Nonce).Concat(Realm).Concat(FormingEvenPortAttribute(evenPort)).Concat(
                FormingLifetimeAttribute(lifetime)).Concat(FormingDontFragmentAttribute(dontFragment)).ToArray(),
                ALLOCATE_REQUEST);
            recv = turnRes.Item1!;
            ValidateResponse(recv, ALLOCATE_RESPONSE);
            return ProcessTurnAllocateResponse(recv);
        }

        public async Task<byte[]> TurnRefreshRequest(string dstAddress, uint dstPort, byte[] transId, int? lifetime = null,
                                   bool evenPort = false, bool dontFragment = false)
        {
            var turnRes = await TurnRawRequest(dstAddress, dstPort,
                                        CombineBytes(FormingEvenPortAttribute(evenPort), FormingLifetimeAttribute(lifetime),
                                        FormingDontFragmentAttribute(dontFragment)),
                                        REFRESH_REQUEST,
                                        transId);
            byte[] res = turnRes.Item1!;
            ValidateResponse(res, REFRESH_RESPONSE);
            return res;
        }

        public async Task<byte[]> TurnDeleteAllocation(string dstIp, uint dstPort, byte[] transId)
        {
            var turnRes = await TurnRawRequest(dstIp, dstPort,
                                        CombineBytes(FormingLifetimeAttribute(0), Realm, Nonce),
                                        REFRESH_REQUEST,
                                        transId);
            byte[] res = turnRes.Item1!;
            ValidateResponse(res, REFRESH_RESPONSE);
            return res;
        }

        public async Task<byte[]> TurnCreatePermission(string dstIp, uint dstPort, byte[] transId, string recipientIp,
                                           uint recipientPort)
        {
            var turnRes = await TurnRawRequest(dstIp, dstPort,
                                        CombineBytes(FormingXorPeerAddressAttribute(recipientIp, recipientPort), Realm, Nonce),
                                        CREATE_PERMISSION_REQUEST,
                                        transId);
            byte[] res = turnRes.Item1!;
            ValidateResponse(res, CREATE_PERMISSION_RESPONSE);
            return res;
        }

        public async Task TurnSend(string dstIp, uint dstPort, byte[] transId, string peerIp,
                             uint peerPort, byte[] data)
        {
            await TurnRawRequest(dstIp, dstPort,
                            CombineBytes(FormingXorPeerAddressAttribute(peerIp, peerPort), Realm, Nonce, FormingDataAttribute(data)),
                            SEND_INDICATION,
                            transId);
        }

        public async Task<byte[]> TurnChannelBind(string dstIp, uint dstPort, byte[] transId, string peerIp, uint peerPort,
                                  int channelNumber)
        {
            var turnRes = await TurnRawRequest(dstIp, dstPort,
                                        CombineBytes(FormingXorPeerAddressAttribute(peerIp, peerPort),
                                        FormingChannelNumberAttribute(channelNumber), Realm, Nonce),
                                        CHANNEL_BIND_REQUEST, transId);
            byte[] res = turnRes.Item1!;
            ValidateResponse(res, CHANNEL_BIND_RESPONSE);
            return res;
        }

        public void TurnChannelSend(string dstIp, int dstPort, int channelNumber, byte[] data)
        {
            using (var sock = new UdpClient())
            {
                sock.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                sock.Client.Bind(new IPEndPoint(IPAddress.Parse(SrcIp), SrcPort));
                sock.Send(data, data.Length, dstIp, dstPort);
            }
        }
    }
}