﻿using Force.Crc32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
namespace CryptoChatClient.Coturn
{
    public class StunError : Exception
    {
        public StunError(string message) : base(message) { }
    }
    public class StunClient
    {
        protected static readonly byte[] BIND_REQUEST_MSG = new byte[] { 0x00, 0x01 };
        protected static readonly byte[] BIND_RESPONSE_MSG = new byte[] { 0x01, 0x01 };
        protected static readonly byte[] BIND_INDICATION_MSG = new byte[] { 0x00, 0x11 };
        protected static readonly byte[] BIND_ERROR_RESPONSE_MSG = new byte[] { 0x01, 0x11 };

        protected static readonly byte[] RESERVED = new byte[] { 0x00, 0x00 };
        protected static readonly byte[] MAPPED = new byte[] { 0x00, 0x01 };
        protected static readonly byte[] RESPONSE_ADDRESS = new byte[] { 0x00, 0x02 };
        protected static readonly byte[] CHANGE_ADDRESS = new byte[] { 0x00, 0x03 };
        protected static readonly byte[] SOURCE_ADDRESS = new byte[] { 0x00, 0x04 };
        protected static readonly byte[] CHANGED_ADDRESS = new byte[] { 0x00, 0x05 };
        private static readonly byte[] USERNAME = new byte[] { 0x00, 0x06 };
        protected static readonly byte[] PASSWORD = new byte[] { 0x00, 0x07 };
        protected static readonly byte[] MESSAGE_INTEGRITY = new byte[] { 0x00, 0x08 };
        protected static readonly byte[] ERROR = new byte[] { 0x00, 0x09 };
        protected static readonly byte[] UNKNOWN_ATTRIBUTES = new byte[] { 0x00, 0x0A };
        protected static readonly byte[] REFLECTED_FROM = new byte[] { 0x00, 0x0B };
        protected static readonly byte[] REALM = new byte[] { 0x00, 0x14 };
        protected static readonly byte[] NONCE = new byte[] { 0x00, 0x15 };
        protected static readonly byte[] XOR_MAPPED = new byte[] { 0x00, 0x20 };

        protected static readonly byte[] SOFTWARE = new byte[] { 0x80, 0x22 };
        protected static readonly byte[] ALTERNATE_SERVER = new byte[] { 0x80, 0x23 };
        protected static readonly byte[] FINGERPRINT = new byte[] { 0x80, 0x28 };
        protected const uint MAGIC_COOKIE = 0x2112A442;
        protected byte[] GenerateRandomBytes(int length)
        {
            byte[] randomBytes = new byte[length];
            new Random().NextBytes(randomBytes);
            return randomBytes;
        }
        public (string, uint) GetXORAddressFromBytes(byte[] ip, byte[] port)
        {
            uint ipAddressValue = BitConverter.ToUInt32(ip.Reverse().ToArray()) ^ MAGIC_COOKIE;

            IPAddress ipAddress = new IPAddress(BitConverter.GetBytes(ipAddressValue).Reverse().ToArray());

            uint portValue = BitConverter.ToUInt16(port.Reverse().ToArray(), 0) ^ (MAGIC_COOKIE >> 16);

            return (ipAddress.ToString(), portValue);
        }
        public byte[] ToBigEndian(byte[] blob)
        {
            Array.Reverse(blob);
            return blob;
        }
        public byte[] ToBigEndian(ushort n)
        {
            byte[] blob = BitConverter.GetBytes(n);
            Array.Reverse(blob);
            return blob;
        }

        public byte[] ToBigEndian(uint n)
        {
            byte[] blob = BitConverter.GetBytes(n);
            Array.Reverse(blob);
            return blob;
        }

        public byte[] CombineBytes(params byte[][] blobs)
        {
            List<byte> res = new List<byte>();
            foreach (var bytes in blobs)
            {
                res.AddRange(bytes);
            }
            return res.ToArray();
        }
        public byte[] AddPadding(int attributeLength)
        {
            int paddingLength = (4 - attributeLength % 4) % 4;
            return new byte[paddingLength];
        }
        public byte[] FormingMessageIntegrity(byte[] messageIntegrityKey, byte[] message)
        {
            ushort integrityLen = 20;
            byte[] hmacSha1 = CalculateHmacSha1(messageIntegrityKey, message);
            return MESSAGE_INTEGRITY.Concat(BitConverter.GetBytes(integrityLen).Reverse()).Concat(hmacSha1).ToArray();
        }

        public byte[] FormingRealm(string realmValue)
        {
            byte[] realmBytes = Encoding.UTF8.GetBytes(realmValue);
            return REALM.Concat(BitConverter.GetBytes((ushort)realmBytes.Length).Reverse()).Concat(realmBytes).ToArray();
        }

        public byte[] FormingFingerprint(byte[] stunMessage)
        {
            uint crc32Value = Crc32Algorithm.Compute(stunMessage); ;
            uint xorValue = 0x5354554e;
            byte[] fingerprint = BitConverter.GetBytes(crc32Value ^ xorValue).Reverse().ToArray();
            return FINGERPRINT.Concat(BitConverter.GetBytes((ushort)fingerprint.Length).Reverse()).Concat(fingerprint).ToArray();
        }
        public byte[] CalculateHmacSha1(byte[] key, byte[] message)
        {
            using (HMACSHA1 hmac = new HMACSHA1(key))
            {
                return hmac.ComputeHash(message);
            }
        }
        public string SaslPrep(string input)
        {
            return input.Normalize(NormalizationForm.FormKC);
        }

        public string SrcIp { get; }
        public int SrcPort { get; }
        public bool IsMgcCookie { get; }
        public bool IsFingerprint { get; }
        public string? Username { get; }
        public string? Password { get; }
        public string? RealmValue { get; set; }
        public byte[]? Nonce { get; set; } = Array.Empty<byte>();

        public byte[]? MessageIntegrityKey { get; private set; }
        public byte[]? TransId { get; private set; }
        public byte[]? ReflexivePort { get; private set; }
        public byte[]? ReflexiveIp { get; private set; }
        public byte[]? Attributes { get; private set; }
        public byte[]? MagicCookieBytes { get; private set; }
        public byte[]? Realm { get; set; } = Array.Empty<byte>();


        public StunClient(string srcIp, int srcPort, bool isMgcCookie = true, bool isFingerprint = false,
                          string? username = "", string? password = "", string? realmValue = "", byte[]? nonce = null)
        {
            SrcIp = srcIp;
            SrcPort = srcPort;
            IsMgcCookie = isMgcCookie;
            IsFingerprint = isFingerprint;
            Username = string.IsNullOrEmpty(username) ? null : SaslPrep(username!);
            Password = string.IsNullOrEmpty(password) ? null : SaslPrep(password!);
            RealmValue = realmValue;
            Realm = string.IsNullOrEmpty(realmValue) ? null : FormingRealm(realmValue);
            Nonce = nonce;
        }

        public async Task<Tuple<byte[]?, EndPoint?>> StunRequestRaw(string dstIp, uint dstPort, byte[]? attributes = null, byte[]? msgType = null, byte[]? transId = null)
        {
            using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
            {
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                TransId = transId == null ? TransId : transId;
                socket.Bind(new IPEndPoint(IPAddress.Parse(SrcIp), SrcPort));
                attributes = attributes ?? Array.Empty<byte>();
                byte[] msgLen = new byte[2];
                if (string.IsNullOrEmpty(Password))
                {
                    msgLen = ToBigEndian(BitConverter.GetBytes((ushort)attributes.Length));
                }
                else if (!string.IsNullOrEmpty(Username))
                {
                    if (!string.IsNullOrEmpty(RealmValue))
                    {
                        if (MessageIntegrityKey == null)
                        {
                            MessageIntegrityKey = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes($"{Username}:{RealmValue}:{Password}"));
                        }
                        byte[] usernameBytes = Encoding.UTF8.GetBytes(Username);
                        byte[] usernameLengthBytes = BitConverter.GetBytes((ushort)usernameBytes.Length).Reverse().ToArray();
                        attributes = CombineBytes(attributes, USERNAME, usernameLengthBytes, usernameBytes, AddPadding(usernameBytes.Length));
                        msgLen = BitConverter.GetBytes((ushort)(attributes.Length + 24)).Reverse().ToArray();
                    }
                    else
                    {
                        msgLen = ToBigEndian((ushort)attributes.Length);
                    }
                }
                else
                {
                    MessageIntegrityKey = Encoding.UTF8.GetBytes(Password);
                    msgLen = ToBigEndian((ushort)attributes.Length);
                }

                if (IsMgcCookie)
                {
                    MagicCookieBytes = ToBigEndian(MAGIC_COOKIE);
                    if (TransId == null)
                        TransId = ToBigEndian(GenerateRandomBytes(12));
                }
                else
                {
                    MagicCookieBytes = Array.Empty<byte>();
                    if (TransId == null)
                        TransId = ToBigEndian(GenerateRandomBytes(16));
                }
                msgType = msgType == null ? BIND_REQUEST_MSG : msgType;
                byte[] message = CombineBytes(msgType, msgLen, MagicCookieBytes, TransId!, attributes);
                if (MessageIntegrityKey != null)
                {
                    var msgIntegrity = FormingMessageIntegrity(MessageIntegrityKey, message);
                    message = CombineBytes(message, msgIntegrity);
                }
                if (IsFingerprint)
                {
                    message = CombineBytes(message[..2], BitConverter.GetBytes((ushort)(BitConverter.ToUInt16(message[2..4].Reverse().ToArray()) + 8)).Reverse().ToArray(), message[4..]);
                    byte[] fingerprint = FormingFingerprint(message);
                    message = CombineBytes(message, fingerprint);
                }
                // foreach (var item in message[2..4])
                // {
                //     Console.Write("{0:D}", item);
                // }
                socket.SendTo(message, new IPEndPoint(IPAddress.Parse(dstIp), (int)dstPort));
                if (msgType!.SequenceEqual(new byte[] { 0x00, 0x16 }))
                {
                    return new Tuple<byte[]?, EndPoint?>(null, null);
                }
                byte[] buffer = new byte[4096];
                EndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
                var bytesReceived = await socket.ReceiveFromAsync(buffer, endPoint);
                byte[] receivedData = new byte[bytesReceived.ReceivedBytes];
                Array.Copy(buffer, receivedData, bytesReceived.ReceivedBytes);
                return new Tuple<byte[]?, EndPoint?>(receivedData, endPoint);
            }
        }
        public void ValidateHeaderResponse(byte[] recv, byte[] msgType)
        {
            if (!recv[..2].SequenceEqual(msgType))
                throw new StunError($"Error. Bad response: {BitConverter.ToString(recv[..2])}");
            if (IsMgcCookie)
            {
                if (!MagicCookieBytes!.SequenceEqual(recv[4..8]))
                    throw new StunError(
                        $"Error. Wrong format of stun response. Expected 0x2112A442(magic cookie), got {BitConverter.ToString(recv[4..8])}. See RFC 3489");
                if (!TransId!.SequenceEqual(recv[8..20]))
                    throw new StunError(
                         $"Error. Wrong ID of stun response. Expected ID {BitConverter.ToString(TransId!)}, got {BitConverter.ToString(recv[8..20])}");
            }
            else
            {
                if (!TransId!.SequenceEqual(recv[4..20]))
                    throw new StunError(
                       $"Error. Wrong ID of stun response. Expected ID {BitConverter.ToString(TransId!)}, got {BitConverter.ToString(recv[4..20])}");
            }
        }
        public void ValidateBodyResponse(byte[] recv)
        {
            int i = 0;
            while (i < recv.Length)
            {
                byte[] tlChunk = recv[i..(i + 4)];
                byte[] attributeType = tlChunk[..2];
                ushort attributeLength = BitConverter.ToUInt16(tlChunk[2..].Reverse().ToArray());
                byte[] attributeValue = recv[(i + 4)..(i + 4 + attributeLength)];
                if (attributeType.SequenceEqual(ERROR))
                {
                    int errorClass = attributeValue[2];
                    int errorNumber = attributeValue[3];
                    int errorCode = errorClass * 100 + errorNumber;
                    if (errorCode != 420)
                    {
                        string errorReason = Encoding.UTF8.GetString(attributeValue[4..]);
                        throw new StunError($"Error {errorCode}. {errorReason}");
                    }
                }
                else if (attributeType.SequenceEqual(UNKNOWN_ATTRIBUTES))
                {
                    List<string> unknownAttributes = new List<string>();
                    for (int j = 0; j < attributeValue.Length; j += 2)
                    {
                        unknownAttributes.Add($"0x{BitConverter.ToUInt16(attributeValue, j):X}");
                    }
                    throw new StunError($"Unknown attributes: {unknownAttributes}");
                }

                i += attributeLength + 4;
            }
        }


        public void ValidateResponse(byte[] recv, byte[] msgType, bool isStrict = false)
        {
            if (IsFingerprint)
            {
                if (isStrict && !recv.TakeLast(8).SequenceEqual(FormingFingerprint(recv.Take(recv.Length - 8).ToArray())))
                {
                    throw new StunError("Integrity error. Fingerprint mismatch");
                }

                if (MessageIntegrityKey != null)
                {
                    byte[] tmpRecv = recv.Take(2).Concat(BitConverter.GetBytes((ushort)(BitConverter.ToUInt16(recv[2..4].Reverse().ToArray()) - 8)).Reverse())
                    .Concat(recv[4..(recv.Length - 32)]).ToArray();
                    if (!FormingMessageIntegrity(MessageIntegrityKey, tmpRecv).SequenceEqual(recv[^32..^8]))
                    {
                        throw new StunError("Credentials error. Message integrity mismatch");
                    }
                }
            }
            else if (MessageIntegrityKey != null)
            {
                if (!FormingMessageIntegrity(MessageIntegrityKey, recv.Take(recv.Length - 24).ToArray()).SequenceEqual(recv.TakeLast(24)))
                {
                    throw new StunError("Credentials error. Message integrity mismatch");
                }
            }
            ValidateHeaderResponse(recv[..20], msgType);
            ValidateBodyResponse(recv[20..]);
        }

        public (string[]?, string, uint) ReadStunResponse(byte[] recv, bool isVerbose)
        {
            List<string> res = new List<string>();
            string? reflexiveIp = null;
            uint reflexivePort = 0;
            bool isXorMappedAddress = false;
            int i = 20;

            while (i < recv.Length)
            {
                byte[] tlChunk = recv[i..(i + 4)];
                byte[] attributeType = tlChunk[..2];
                int attributeLength = BitConverter.ToUInt16(tlChunk[2..].Reverse().ToArray());
                byte[] attributeValue = recv[(i + 4)..(i + 4 + attributeLength)];
                if (attributeType.SequenceEqual(MAPPED) && !isXorMappedAddress)
                {
                    reflexivePort = BitConverter.ToUInt16(attributeValue[2..4].Reverse().ToArray());
                    reflexiveIp = new IPAddress(attributeValue[4..].Reverse().ToArray()).ToString();

                }
                else if (attributeType.SequenceEqual(XOR_MAPPED))
                {
                    (reflexiveIp, reflexivePort) = GetXORAddressFromBytes(attributeValue[4..], attributeValue[2..4]);
                    isXorMappedAddress = true;
                }
                else if (attributeType.SequenceEqual(SOFTWARE))
                {
                    res.Add($"Software information: {Encoding.UTF8.GetString(attributeValue)}");
                }
                else if (attributeType.SequenceEqual(ALTERNATE_SERVER))
                {
                    res.Add($"Alternate server: {Encoding.UTF8.GetString(attributeValue)}");
                }
                else if (attributeType.SequenceEqual(REALM))
                {
                    res.Add($"Realm : {Encoding.UTF8.GetString(attributeValue)}");
                }
                else if (attributeType.SequenceEqual(NONCE))
                {
                    res.Add($"Nonce : {Encoding.UTF8.GetString(attributeValue)}");
                }
                i += attributeLength + 4;
            }
            return isVerbose ? (res.ToArray(), reflexiveIp!, reflexivePort) : (null, reflexiveIp!, reflexivePort);
        }

        public async Task<(string[]?, string?, uint)> StunRequest(string dstIp, uint dstPort, bool verbose = false)
        {
            var stunRes = await StunRequestRaw(dstIp, dstPort);
            var recv = stunRes.Item1;
            if (recv == null)
            {
                throw new StunError("recv is null");
            }
            ValidateResponse(recv, BIND_RESPONSE_MSG);
            return ReadStunResponse(recv, verbose);
        }
    }
}