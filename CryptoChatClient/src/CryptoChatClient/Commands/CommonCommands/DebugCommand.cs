﻿using CryptoChatClient.Core;
using CryptoChatClient.Helpers;

namespace CryptoChatClient.Commands.CommonCommands
{
    public class DebugCommand : CommandBase
    {
        public DebugCommand()
        {
        }

        public override void Execute(object? parameter)
        {
            //Type type = parameter.GetType();
            //MessageBox.Show($"{type}");
            ShowErrorHelper.ShowDebug("TestCommand");
        }
    }
}
