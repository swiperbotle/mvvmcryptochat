﻿using CryptoChatClient.Core;

namespace CryptoChatClient.Commands.CommonCommands
{
    internal class NavigateToCommand<TViewModelType> : CommandBase where TViewModelType : ViewModelBase
    {
        private readonly INavigationService _navigation;
        public NavigateToCommand(INavigationService navigation)
        {
            _navigation = navigation;
        }
        public override void Execute(object parameter)
        {
            _navigation.NavigateTo<TViewModelType>();
        }
    }
}
