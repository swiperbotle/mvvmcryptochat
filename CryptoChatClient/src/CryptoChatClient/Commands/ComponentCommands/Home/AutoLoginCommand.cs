﻿using CryptoChatClient.Core;
using CryptoChatClient.Enums;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class AutoLoginCommand : AsyncCommandBase
    {
        private readonly CryptoChatRepository _dbRepository;
        private readonly IListenerService _listener;
        private readonly INavigationService _navigation;
        private int _onceLoadFlag = 0;
        string _signalingServerIp;
        string _stunServerIp;
        int _stunServerPort;
        public AutoLoginCommand(CryptoChatRepository dbRepository, IListenerService listener, INavigationService navigation, string signalingServerIp, string stunServerIp, int stunServerPort)
        {
            _dbRepository = dbRepository;
            _listener = listener;
            _navigation = navigation;
            _signalingServerIp = signalingServerIp;
            _stunServerIp = stunServerIp;
            _stunServerPort = stunServerPort;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            if (_onceLoadFlag == 1)
                return;
            var currentUserModel = await _dbRepository.ReadUser();
            if (currentUserModel != null)
            {
                if (_listener.GetStatus() == WebSocketState.None || _listener.GetStatus() == WebSocketState.Closed || _listener.GetStatus() == WebSocketState.Aborted)
                    if (!await _listener.StartListen(_signalingServerIp, _stunServerIp, _stunServerPort))
                        return;
                var response = await _listener.SignInRequest(currentUserModel.Username, currentUserModel.Password);
                if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.SignIn_Success_Response)
                {
                    ++_onceLoadFlag;
                    _navigation.NavigateTo<HomeViewModel>(currentUserModel!);
                }
                else
                {
                    var notificationWindow = new NotificationWindow("Error", "Unknown error. Authorize manually.");
                    notificationWindow.Show();
                }
            }
        }
    }
}
