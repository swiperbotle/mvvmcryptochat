﻿using CryptoChatClient.Core;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class CloseSettingsMenuCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is UserControl userControl)
            {
                var menuCloseAnimation = (Storyboard)userControl.Resources["LeftPopUpCloseSettingsMenuAnimation"];
                menuCloseAnimation.Begin();
            }
        }
    }
}
