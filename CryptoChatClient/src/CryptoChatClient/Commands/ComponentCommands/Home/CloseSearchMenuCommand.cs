﻿using CryptoChatClient.Core;
using System.Windows.Controls.Primitives;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class CloseSearchMenuCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is Popup menu)
            {
                menu.IsOpen = !menu.IsOpen;
            }
        }
    }
}
