﻿using CryptoChatClient.Core;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModels;
using System.Threading.Tasks;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class LogOutCommand : AsyncCommandBase
    {
        private readonly INavigationService _navigationService;
        public HomeViewModel _homeViewModel;
        private readonly CryptoChatRepository _cryptoChatRepository;

        public LogOutCommand(INavigationService navigation, HomeViewModel homeViewModel, CryptoChatRepository cryptoChatRepository)
        {
            _navigationService = navigation;
            _homeViewModel = homeViewModel;
            _cryptoChatRepository = cryptoChatRepository;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            await _cryptoChatRepository.DeleteUser();
            await _homeViewModel.Flush();
            _navigationService.NavigateTo<SignInViewModel>();
        }
    }
}
