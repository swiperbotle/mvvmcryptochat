﻿using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class SelectChatCommand : CommandBase
    {
        public Dictionary<Guid, MessageModels> messageChatModels;
        public HomeViewModel homeViewModel;
        public SelectChatCommand(HomeViewModel homeViewModel, Dictionary<Guid, MessageModels> messageChatModels)
        {
            this.messageChatModels = messageChatModels;
            this.homeViewModel = homeViewModel;
        }

        public override void Execute(object? parameter)
        {
            var parameters = (object[])parameter;
            if (parameters[0] is Guid chatModelId)
            {
                try
                {
                    this.homeViewModel.CurrentMessageViewModels = new MessageViewModels<Int64, MessageObservableModel>();
                    this.homeViewModel.CurrentMessageViewModels.ChatId = chatModelId;
                    if (!messageChatModels.ContainsKey(chatModelId))
                        return;
                    this.homeViewModel.CurrentMessageViewModels.AddMultiple(messageChatModels[chatModelId]);
                    var lastScrollOffset = messageChatModels[chatModelId].LastScrollOffset;
                    if (parameters[1] is ListView listView)
                    {
                        var scrollViewer = FindElementInTree.FindVisualChild<ScrollViewer>(listView);
                        scrollViewer.ScrollToVerticalOffset(lastScrollOffset);
                    }
                }
                catch (Exception ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            }
        }
    }
}
