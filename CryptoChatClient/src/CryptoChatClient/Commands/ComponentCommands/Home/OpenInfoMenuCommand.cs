﻿using CryptoChatClient.Core;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class OpenInfoMenuCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is UserControl userControl)
            {
                var menuAnimation = (Storyboard)userControl.Resources["LeftPopUpOpenInfoMenuAnimation"];
                menuAnimation.Begin();
            }
        }
    }
}
