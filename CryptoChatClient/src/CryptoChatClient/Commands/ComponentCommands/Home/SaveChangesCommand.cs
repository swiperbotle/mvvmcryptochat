﻿using Microsoft.IdentityModel.Tokens;
using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModels;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class SaveChangesCommand : AsyncCommandBase
    {
        private readonly IListenerService _listenerService;
        private readonly HomeViewModel _homeViewModel;
        private readonly CryptoChatRepository _cryptoChatRepository;
        public SaveChangesCommand(IListenerService listenerService, HomeViewModel homeViewModel, CryptoChatRepository cryptoChatRepository)
        {
            _listenerService = listenerService;
            _homeViewModel = homeViewModel;
            _cryptoChatRepository = cryptoChatRepository;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            var userModelTemp = new CurrentUserModel(_homeViewModel.CurrentUser);
            if (!_homeViewModel.InputChangeUsername.IsNullOrEmpty())
            {
                userModelTemp.Username = _homeViewModel.InputChangeUsername;
            }
            if (_homeViewModel.ColorPicked != null)
            {
                userModelTemp.Avatar = AvatarHelper.CreateBitmapFromColor((Color)_homeViewModel.ColorPicked);
            }
            if (await _listenerService.UpdateRequest(userModelTemp))
            {
                _homeViewModel.CurrentUser = userModelTemp;
                await _cryptoChatRepository.UpdateUser(userModelTemp);
            }
        }
    }
}
