﻿using Microsoft.IdentityModel.Tokens;
using CryptoChatClient.Core;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using WpfWindow = System.Windows.Window;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class SavePasswordChangeCommand : AsyncCommandBase
    {
        private readonly IListenerService _listenerService;
        private readonly HomeViewModel _homeViewModel;
        private readonly CryptoChatRepository _cryptoChatRepository;
        public SavePasswordChangeCommand(IListenerService listenerService, HomeViewModel homeViewModel, CryptoChatRepository cryptoChatRepository)
        {
            _listenerService = listenerService;
            _homeViewModel = homeViewModel;
            _cryptoChatRepository = cryptoChatRepository;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            var parameters = (object[])parameter!;
            if (parameters[0] is PasswordBox passBox1 && parameters[1] is PasswordBox passBox2 && parameters[2] is PasswordBox passBox3 && parameters[3] is WpfWindow window)
            {
                var hashHelper = new HashPasswordHelper();
                var oldPass = hashHelper.HashPassword(passBox1.Password).Substring(0, 8);
                var newPass = hashHelper.HashPassword(passBox2.Password).Substring(0, 8);
                var newPassRepeat = hashHelper.HashPassword(passBox3.Password).Substring(0, 8);
                if (passBox1.Password.IsNullOrEmpty())
                {
                    passBox1.BorderBrush = new SolidColorBrush(Colors.Red);
                    var notWin = new NotificationWindow("Error", "Password can`t be null");
                    notWin.Show();
                    return;
                }
                if (passBox2.Password.IsNullOrEmpty())
                {
                    passBox2.BorderBrush = new SolidColorBrush(Colors.Red);
                    var notWin = new NotificationWindow("Error", "Password can`t be null");
                    notWin.Show();
                    return;
                }
                if (passBox3.Password.IsNullOrEmpty())
                {
                    passBox3.BorderBrush = new SolidColorBrush(Colors.Red);
                    var notWin = new NotificationWindow("Error", "Password can`t be null");
                    notWin.Show();
                    return;
                }
                if (oldPass != _homeViewModel.CurrentUser.Password)
                {
                    passBox1.BorderBrush = new SolidColorBrush(Colors.Red);
                    var notWin = new NotificationWindow("Error", "Incorecct password");
                    notWin.Show();
                    return;
                }
                if (newPass != newPassRepeat)
                {
                    passBox2.BorderBrush = new SolidColorBrush(Colors.Red);
                    passBox3.BorderBrush = new SolidColorBrush(Colors.Red);
                    var notWin = new NotificationWindow("Error", "Password mismatch.");
                    notWin.Show();
                    return;
                }
                var userModelTemp = new CurrentUserModel(_homeViewModel.CurrentUser);
                userModelTemp.Password = newPass;
                if (await _listenerService.UpdateRequest(userModelTemp))
                {
                    _homeViewModel.CurrentUser.Password = newPass;
                    await _cryptoChatRepository.UpdateUser(_homeViewModel.CurrentUser);
                    window.Close();
                }
            }
        }
    }
}
