﻿using CryptoChatClient.Core;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class OpenChangePasswordCommand : CommandBase
    {
        HomeViewModel _homeViewModel;
        public OpenChangePasswordCommand(HomeViewModel homeViewModel)
        {
            {
                _homeViewModel = homeViewModel;
            }
        }
        public override void Execute(object? parameter)
        {
            var changePassWindow = new ChangePasswordWindow(_homeViewModel);
            changePassWindow.Show();
        }
    }
}
