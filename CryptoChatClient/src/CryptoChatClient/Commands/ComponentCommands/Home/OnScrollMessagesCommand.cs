﻿using CryptoChatClient.Core;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class OnScrollMessagesCommand : CommandBase
    {
        HomeViewModel homeViewModel;
        public Dictionary<Guid, MessageModels> messageChatModels;
        public OnScrollMessagesCommand(HomeViewModel homeViewModel, Dictionary<Guid, MessageModels> messageChatModels)
        {
            this.homeViewModel = homeViewModel;
            this.messageChatModels = messageChatModels;
        }

        public override void Execute(object? parameter)
        {
            if (parameter is ScrollViewer scrollViewer)
            {
                if (this.homeViewModel.CurrentMessageViewModels != null)
                {
                    if (this.messageChatModels.ContainsKey(this.homeViewModel.CurrentMessageViewModels.ChatId))
                    {
                        var messageCurrentModels = this.messageChatModels[this.homeViewModel.CurrentMessageViewModels.ChatId];
                        messageCurrentModels.LastScrollOffset = (scrollViewer.VerticalOffset);
                    }
                }
            }

        }
    }
}

