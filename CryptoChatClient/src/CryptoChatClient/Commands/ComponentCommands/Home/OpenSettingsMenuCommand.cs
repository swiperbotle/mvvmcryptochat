﻿using CryptoChatClient.Core;
using CryptoChatClient.ViewModels;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class OpenSettingsMenuCommand : CommandBase
    {
        HomeViewModel _homeViewModel;
        public OpenSettingsMenuCommand(HomeViewModel homeViewModel)
        {
            _homeViewModel = homeViewModel;
        }
        public override void Execute(object? parameter)
        {
            if (parameter is UserControl userControl)
            {
                _homeViewModel.InputChangeUsername = _homeViewModel.CurrentUser.Username;
                var menuAnimation = (Storyboard)userControl.Resources["LeftPopUpOpenSettingsMenuAnimation"];
                menuAnimation.Begin();
            }
        }
    }
}
