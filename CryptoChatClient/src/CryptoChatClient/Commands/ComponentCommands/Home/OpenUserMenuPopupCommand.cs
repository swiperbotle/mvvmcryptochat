﻿using CryptoChatClient.Core;
using System.Windows.Controls.Primitives;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class OpenUserMenuPopupCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is Popup menu)
                if (!menu.IsOpen)
                    menu.IsOpen = !menu.IsOpen;
        }
    }
}
