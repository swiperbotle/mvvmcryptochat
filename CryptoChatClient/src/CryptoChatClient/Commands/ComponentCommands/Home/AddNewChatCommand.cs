﻿using CryptoChatClient.Core;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class AddNewChatCommand : AsyncCommandBase
    {
        private HomeViewModel _homeViewModel;
        public AddNewChatCommand(HomeViewModel homeViewModel)
        {
            _homeViewModel = homeViewModel;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            var parameters = (object[])parameter!;
            if (parameters[0] is UserObservableModel userModel && parameters[1] is Popup menu)
            {
                var chatModel = new ChatObservableModel();
                chatModel.ChatId = userModel.Id;
                chatModel.SenderName = userModel.Username;
                chatModel.Avatar = userModel.Avatar;
                chatModel.LastMessage = "";
                if (!_homeViewModel.Chats.ContainsKey(chatModel.ChatId))
                    _homeViewModel.Chats.Add(chatModel.ChatId, chatModel);
                menu.IsOpen = !menu.IsOpen;
            }
        }
    }
}
