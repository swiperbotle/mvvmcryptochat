﻿using CryptoChatClient.Core;
using CryptoChatClient.ViewModels;
using System.Threading.Tasks;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class ApplyUserDeleteCommand : AsyncCommandBase
    {
        private readonly IListenerService _listenerService;
        private readonly HomeViewModel _homeViewModel;
        public ApplyUserDeleteCommand(IListenerService listenerService, HomeViewModel homeViewModel)
        {
            _listenerService = listenerService;
            _homeViewModel = homeViewModel;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            await _listenerService.RemoveRequest(_homeViewModel.CurrentUser.Id);
            await _homeViewModel.LogOutCommand.ExecuteAsync(new object());
            _homeViewModel.CloseWindowCommand.Execute(parameter);
        }
    }
}
