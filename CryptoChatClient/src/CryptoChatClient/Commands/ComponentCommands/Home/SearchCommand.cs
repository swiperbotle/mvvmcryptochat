﻿using CryptoChatClient.Core;
using CryptoChatClient.Enums;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class SearchCommand : AsyncCommandBase
    {
        HomeViewModel _homeViewModel;
        private readonly IListenerService _listener;
        public SearchCommand(HomeViewModel homeViewModel, IListenerService listener)
        {

            _homeViewModel = homeViewModel;
            _listener = listener;
        }
        public override async Task ExecuteAsync(object? parameter)
        {
            var count = _homeViewModel.SearchResult.Count;
            for (int i = 0; i < count; ++i)
            {
                _homeViewModel.SearchResult.Remove(_homeViewModel.SearchResult.First().Key);
            }
            if (parameter is Popup menu)
            {
                if (_listener.GetStatus() == WebSocketState.None || _listener.GetStatus() == WebSocketState.Closed || _listener.GetStatus() == WebSocketState.Aborted)
                {
                    ShowErrorHelper.ShowDebug("No internet. Connect and try restart application.");
                    _homeViewModel.SearchInputText = "";
                    return;
                }
                var response = await _listener.SearchRequest(_homeViewModel.CurrentUser.Id, _homeViewModel.SearchInputText);
                if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.Search_Success_Response)
                {
                    var userModel = JsonSerializer.Deserialize<UserObservableModel>(Encoding.UTF8.GetString(response[4..]));
                    _homeViewModel.SearchResult.Add(userModel.Id, userModel);
                    if (!menu.IsOpen)
                        menu.IsOpen = !menu.IsOpen;
                }
                else if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.Error_Response)
                {
                    if ((BitConverter.ToInt32(response[4..8], 0) == (int)ErrorType.User_Not_Found_Error))
                    {
                        if (!menu.IsOpen)
                            menu.IsOpen = !menu.IsOpen;
                    }
                    else
                    {
                        var notificationWindow = new NotificationWindow("Error", "Unknown server error.");
                        notificationWindow.Show();
                    }
                }
                else
                {
                    var notificationWindow = new NotificationWindow("Error", "Bad server response.");
                    notificationWindow.Show();
                }
            }
            _homeViewModel.SearchInputText = "";
        }
    }
}
