﻿using CryptoChatClient.Core;
using CryptoChatClient.Enums;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Windows;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    class SendMessageCommand : AsyncCommandBase
    {

        public HomeViewModel _homeViewModel;
        public Dictionary<Guid, MessageModels> messageChatModels;
        private readonly IListenerService _listener;
        public CurrentUserModel currentUser;

        public SendMessageCommand(HomeViewModel homeViewModel, Dictionary<Guid, MessageModels> messageChatModels, CurrentUserModel currentUser, IListenerService listener)
        {
            this.messageChatModels = messageChatModels;
            this.currentUser = currentUser;
            this._homeViewModel = homeViewModel;
            _listener = listener;
        }
        public override async Task ExecuteAsync(object? parameter)

        {

            var messageText = this._homeViewModel.MessageInputText;
            MessageModels messageModels;
            MessageObservableModel messageModelInternal;
            var messageId = await CreateMessageId();
            if (messageId == null)
            {
                ShowErrorHelper.ShowDebug("Wrong response from get id request");
            }
            var chatId = this._homeViewModel.CurrentMessageViewModels.ChatId;
            if (chatId == new Guid())
                return;
            if (!messageChatModels.ContainsKey(chatId))
            {
                messageModels = new MessageModels();
                this.messageChatModels.Add(chatId, messageModels);
            }
            else
            {
                messageModels = messageChatModels[chatId];
            }
            // create message
            messageModelInternal = new MessageObservableModel { MessageId = (long)messageId, ChatId = chatId, MessageText = messageText, Avatar = _homeViewModel.CurrentUser.Avatar, Nickname = _homeViewModel.CurrentUser.Username, IsSelf = true };
            var messageModelExternal = new MessageObservableModel { MessageId = (long)messageId, ChatId = _homeViewModel.CurrentUser.Id, MessageText = messageText, Avatar = _homeViewModel.CurrentUser.Avatar, Nickname = _homeViewModel.CurrentUser.Username, IsSelf = false };
            // add in both view model and model
            int clientPort;
            var random = new Random();
            do
            {
                clientPort = random.Next(1024, 65535);
            } while (IsPortInUse(clientPort));
            if (!await _listener.ConversationRequest(_homeViewModel.CurrentUser.Id, chatId, messageModelExternal, clientPort: clientPort))
            {
                this._homeViewModel.MessageInputText = "";
                return;
            }
            messageModels.Add(messageId, messageModelInternal);
            this._homeViewModel.CurrentMessageViewModels.Add((long)messageId, messageModelInternal);
            this._homeViewModel.Chats[chatId].LastMessage = messageText;

            #region share_secret
            var encryptorA = new PostQuantumEncryptor();
            var encryptorB = new PostQuantumEncryptor(encryptorA.GetPublicKey());
            encryptorA.ExctractSecret(encryptorB.GetCipherText());
            #endregion


            #region ecnryption
            var plainText = "some";
            var KeyAES = encryptorA.Secret!;
            var IvAES = SHA1.HashData(KeyAES).Take(16).ToArray();
            var ciphertext = AES.Encrypt(plainText, KeyAES, IvAES);
            var encryptedText = Convert.ToBase64String(ciphertext);
            MessageBox.Show("Encrypted Text: " + encryptedText);
            // Decrypt
            var bytes = Convert.FromBase64String(encryptedText);
            var decryptedText = AES.Decrypt(bytes, KeyAES, IvAES);
            MessageBox.Show("Decrypted Text: " + decryptedText);
            #endregion
        }
        private async Task<Int64?> CreateMessageId()
        {
            var response = await _listener.GetMessageIdRequest(_homeViewModel.CurrentUser.Id);
            if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.Get_MessageId_Success)
            {
                return BitConverter.ToInt64(response[4..12]);
            }
            else
            {
                ShowErrorHelper.ShowDebug("Cannot add messages");
            }
            ShowErrorHelper.ShowDebug("Cannot add messages");

            return null;
        }
        private bool IsPortInUse(int port)
        {
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] udpEndPoints = ipGlobalProperties.GetActiveUdpListeners();

            return udpEndPoints.Any(p => p.Port == port);
        }
    }
}
