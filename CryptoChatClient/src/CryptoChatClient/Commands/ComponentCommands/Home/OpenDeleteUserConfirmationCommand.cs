﻿using CryptoChatClient.Core;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class OpenDeleteUserConfirmationCommand : CommandBase
    {
        HomeViewModel _homeViewModel;
        public OpenDeleteUserConfirmationCommand(HomeViewModel homeViewModel)
        {
            _homeViewModel = homeViewModel;
        }
        public override void Execute(object? parameter)
        {
            var confirmationWindow = new ConfirmWindow(_homeViewModel);
            confirmationWindow.Show();
        }
    }
}
