﻿using CryptoChatClient.Core;
using WpfWindow = System.Windows.Window;

namespace CryptoChatClient.Commands.ComponentCommands.Home
{
    public class CloseWindowCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is WpfWindow window)
                window.Close();
        }
    }
}
