﻿using Microsoft.IdentityModel.Tokens;
using CryptoChatClient.Core;
using CryptoChatClient.Enums;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
namespace CryptoChatClient.Commands.ComponentCommands.LogIn
{
    public class SignUpCommand : AsyncCommandBase
    {
        INavigationService _navigationService;
        IListenerService _listener;
        string _signalingServerIp;
        string _stunServerIp;
        int _stunServerPort;
        public SignUpCommand(INavigationService navigation, IListenerService listener, string signalingServerIp, string stunServerIp, int stunServerPort)
        {
            this._navigationService = navigation;
            _listener = listener;
            _signalingServerIp = signalingServerIp;
            _stunServerIp = stunServerIp;
            _stunServerPort = stunServerPort;
        }
        public async override Task ExecuteAsync(object? parameter)
        {
            try
            {
                var parameters = (object[])parameter!;
                if (parameters[1] is PasswordBox passwordBox1 && parameters[2] is PasswordBox passwordBox2 && parameters[0] is TextBox textBox1)
                {
                    if (passwordBox1.Password.IsNullOrEmpty() || passwordBox2.Password.IsNullOrEmpty() || textBox1.Text.IsNullOrEmpty())
                    {
                        if (passwordBox1.Password.IsNullOrEmpty())
                            passwordBox1.BorderBrush = new SolidColorBrush(Colors.Red);
                        if (passwordBox2.Password.IsNullOrEmpty())
                            passwordBox2.BorderBrush = new SolidColorBrush(Colors.Red);
                        if (textBox1.Text.IsNullOrEmpty())
                            textBox1.BorderBrush = new SolidColorBrush(Colors.Red);
                        var notificationWindow = new NotificationWindow("Error", "All fields must be filled.");
                        notificationWindow.Show();
                        return;
                    }
                    if (passwordBox1.Password != passwordBox2.Password)
                    {
                        passwordBox1.BorderBrush = new SolidColorBrush(Colors.Red);
                        passwordBox2.BorderBrush = new SolidColorBrush(Colors.Red);
                        var notificationWindow = new NotificationWindow("Error", "Password mismatched.");
                        notificationWindow.Show();
                        return;
                    }
                    else
                    {
                        if (!await InternetAvailableHelper.IsInternetAvailable())
                        {
                            var notificationWindow = new NotificationWindow("Error", "No Internet connection.");
                            notificationWindow.Show();
                            return;
                        }
                        if (_listener.GetStatus() == WebSocketState.None || _listener.GetStatus() == WebSocketState.Closed || _listener.GetStatus() == WebSocketState.Aborted)
                            if (!await _listener.StartListen(_signalingServerIp, _stunServerIp, _stunServerPort))
                                return;
                        var currentUserModel = new CurrentUserModel
                        {
                            Id = Guid.NewGuid(),
                            Username = textBox1.Text,
                            Password = new HashPasswordHelper().HashPassword(passwordBox1.Password).Substring(0, 8),
                            Nickname = RandomStringHelper.CreateRandomStr(20),
                            Bio = null,
                            Avatar = AvatarHelper.CreateRandomBitMap(),
                            IsPrivate = false,
                        };
                        var response = await _listener.SignUpRequest(currentUserModel);
                        if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.SignUp_Success_Response)
                        {
                            var notificationWindow = new NotificationWindow("Info", "Account successfully created. Now login.");
                            notificationWindow.Show();
                            _navigationService.NavigateTo<SignInViewModel>();
                        }
                        else if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.Error_Response)
                        {
                            if ((BitConverter.ToInt32(response[4..8], 0) == (int)ErrorType.Login_Already_In_Use_Error))
                            {
                                textBox1.BorderBrush = new SolidColorBrush(Colors.Red);
                                var notificationWindow = new NotificationWindow("Error", "Login already in use.");
                                notificationWindow.Show();
                            }
                            else
                            {
                                var notificationWindow = new NotificationWindow("Error", "Unknown server error.");
                                notificationWindow.Show();
                            }
                        }
                        else
                        {
                            var notificationWindow = new NotificationWindow("Error", "Bad server response.");
                            notificationWindow.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowErrorHelper.ShowError(ex);
            }
        }
    }
}
