﻿using Microsoft.IdentityModel.Tokens;
using CryptoChatClient.Core;
using CryptoChatClient.Enums;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System;
using System.Net.WebSockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
namespace CryptoChatClient.Commands.ComponentCommands.LogIn
{
    public class SignInCommand : AsyncCommandBase
    {
        IListenerService _listener;
        INavigationService _navigationService;
        CryptoChatRepository _dbRepository;
        string _signalingServerIp;
        string _stunServerIp;
        int _stunServerPort;
        public SignInCommand(INavigationService navigation, IListenerService listener, CryptoChatRepository dbRepository, string signalingServerIp, string stunServerIp, int stunServerPort)
        {
            _listener = listener;
            _dbRepository = dbRepository;
            this._navigationService = navigation;
            _signalingServerIp = signalingServerIp;
            _stunServerIp = stunServerIp;
            _stunServerPort = stunServerPort;
        }
        public async override Task ExecuteAsync(object? parameter)
        {
            try
            {
                var parameters = (object[])parameter!;
                if (parameters[0] is TextBox textBox && parameters[1] is PasswordBox passwordBox && parameters[2] is bool rememberMe)
                {
                    if (passwordBox.Password.IsNullOrEmpty() || textBox.Text.IsNullOrEmpty())
                    {
                        if (passwordBox.Password.IsNullOrEmpty())
                            passwordBox.BorderBrush = new SolidColorBrush(Colors.Red);
                        if (textBox.Text.IsNullOrEmpty())
                            textBox.BorderBrush = new SolidColorBrush(Colors.Red);
                        var notificationWindow = new NotificationWindow("Error", "All fields must be filled.");
                        notificationWindow.Show();
                        return;
                    }
                    else
                    {
                        if (!await InternetAvailableHelper.IsInternetAvailable())
                        {
                            var notificationWindow = new NotificationWindow("Error", "No Internet connection.");
                            notificationWindow.Show();
                            return;
                        }
                        if (_listener.GetStatus() == WebSocketState.None || _listener.GetStatus() == WebSocketState.Closed || _listener.GetStatus() == WebSocketState.Aborted)
                            if (!await _listener.StartListen(_signalingServerIp, _stunServerIp, _stunServerPort))
                                return;
                        var response = await _listener.SignInRequest(textBox.Text, new HashPasswordHelper().HashPassword(passwordBox.Password));
                        if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.SignIn_Success_Response)
                        {
                            var currentUserModel = JsonSerializer.Deserialize<CurrentUserModel>(Encoding.UTF8.GetString(response[4..]));
                            if (rememberMe)
                            {
                                await _dbRepository.CreateUser(currentUserModel!);
                            }
                            _navigationService.NavigateTo<HomeViewModel>(currentUserModel!);
                        }
                        else if (BitConverter.ToInt32(response[..4], 0) == (int)MessageType.Error_Response)
                        {
                            if ((BitConverter.ToInt32(response[4..8], 0) == (int)ErrorType.Wrong_Credentials_Error))
                            {
                                textBox.BorderBrush = new SolidColorBrush(Colors.Red);
                                passwordBox.BorderBrush = new SolidColorBrush(Colors.Red);
                                var notificationWindow = new NotificationWindow("Error", "Incorrect login or password. Try again.");
                                notificationWindow.Show();
                            }
                            else
                            {
                                var notificationWindow = new NotificationWindow("Error", "Unknown server error.");
                                notificationWindow.Show();
                            }
                        }
                        else
                        {
                            var notificationWindow = new NotificationWindow("Error", "Bad server response.");
                            notificationWindow.Show();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                ShowErrorHelper.ShowError(ex);
            }
        }
    }
}
