﻿using CryptoChatClient.Core;
using System.Windows;

namespace CryptoChatClient.Commands.ComponentCommands.Window
{
    class WindowMinimizeButtonCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is MainWindow mainWindow)
            {
                mainWindow.WindowState = WindowState.Minimized;
            }
        }
    }
}
