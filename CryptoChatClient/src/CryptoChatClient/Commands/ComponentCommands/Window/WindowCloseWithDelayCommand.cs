﻿using CryptoChatClient.Core;
using System;
using System.Windows.Threading;

namespace CryptoChatClient.Commands.ComponentCommands.Window
{
    class WindowCloseWithDelayCommand : CommandBase
    {
        private readonly int delay;
        public WindowCloseWithDelayCommand(int delay)
        {
            this.delay = delay;
        }
        public override void Execute(object? parameter)
        {
            if (parameter is System.Windows.Window window)
            {
                var timer = new DispatcherTimer
                {
                    Interval = TimeSpan.FromSeconds(this.delay),
                    Tag = window,
                };
                timer.Tick += Timer_Tick!;
                timer.Start();
            }
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (sender is DispatcherTimer timer)
            {
                timer.Stop();
                if (timer.Tag is System.Windows.Window window)
                {
                    window.Close();
                }
            }
        }
    }
}
