﻿using CryptoChatClient.Core;

namespace CryptoChatClient.Commands.ComponentCommands.Window
{
    class WindowMouseLeftButtonDownCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is MainWindow mainWindow)
            {
                mainWindow.DragMove();
            }
        }
    }
}
