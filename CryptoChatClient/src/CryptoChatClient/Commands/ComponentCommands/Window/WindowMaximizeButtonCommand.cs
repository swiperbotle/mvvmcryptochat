﻿using CryptoChatClient.Core;
using System.Windows;

namespace CryptoChatClient.Commands.ComponentCommands.Window
{
    class WindowMaximizeButtonCommand : CommandBase
    {
        public override void Execute(object? parameter)
        {
            if (parameter is MainWindow mainWindow)
            {
                if (mainWindow.WindowState == WindowState.Maximized)
                {
                    mainWindow.WindowState = WindowState.Normal;
                }
                else
                {
                    mainWindow.WindowState = WindowState.Maximized;
                }
            }
        }
    }
}
