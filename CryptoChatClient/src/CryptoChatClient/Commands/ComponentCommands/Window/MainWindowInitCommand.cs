﻿using CryptoChatClient.Core;
using CryptoChatClient.POCOs;
using System.IO;
using System.Text.Json;

namespace CryptoChatClient.Commands.ComponentCommands.Window
{
    public class MainWindowInitCommand : CommandBase
    {
        string jsonPath;
        public MainWindowInitCommand(string jsonPath)
        {
            this.jsonPath = jsonPath;
        }
        public override void Execute(object? parameter)
        {
            if (parameter is MainWindow mainWindow)
            {
                var jsonData = File.ReadAllText(this.jsonPath);
                var settings = JsonSerializer.Deserialize<WindowSettings>(jsonData)!;
                mainWindow.Width = settings.Width;
                mainWindow.Height = settings.Height;
                mainWindow.Left = settings.Left;
                mainWindow.Top = settings.Top;
            }
        }
    }
}
