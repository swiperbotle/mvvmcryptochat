﻿using CryptoChatClient.Core;
using System.IO;
using System.Text.Json;

namespace CryptoChatClient.Commands.ComponentCommands.Window
{
    class WindowCloseButtonCommand : CommandBase
    {
        string jsonPath;
        public WindowCloseButtonCommand(string jsonPath)
        {
            this.jsonPath = jsonPath;
        }
        public override void Execute(object? parameter)
        {
            if (parameter is MainWindow mainWindow)
            {
                var windowInfo = new
                {
                    Width = mainWindow.Width,
                    Height = mainWindow.Height,
                    Left = mainWindow.Left,
                    Top = mainWindow.Top,
                };
                var jsonData = JsonSerializer.Serialize(windowInfo);
                File.WriteAllText(this.jsonPath, jsonData);
                mainWindow.Close();
            }
        }
    }
}
