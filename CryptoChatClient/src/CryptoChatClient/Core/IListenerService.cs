﻿using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using System;
using System.Net.WebSockets;
using System.Threading.Tasks;

namespace CryptoChatClient.Core
{
    public interface IListenerService
    {
        public Task<bool> StartListen(string _signalingServerIp, string stunServerIp, int stunServerPort);
        public WebSocketState GetStatus();
        public void ForceClose();
        public Task CloseGracefully();
        public Task<byte[]> GetMessageIdRequest(Guid userId);
        public Task<byte[]> SignInRequest(string username, string password);
        public Task ListenAsync(HomeViewModel homeViewModel);
        public Task<byte[]> SignUpRequest(CurrentUserModel userModel);
        public Task<byte[]> SearchRequest(Guid userId, string username);
        public Task RemoveRequest(Guid userId);
        public Task<bool> UpdateRequest(CurrentUserModel userModel);
        public Task<bool> ConversationRequest(Guid userId, Guid destId, MessageObservableModel message, string connectionType = "STUN", string clientIp = "0.0.0.0", int clientPort = 12345, string username = "1709740831", string password = "MTIzNDU2Nzg5MA==");
    }
}
