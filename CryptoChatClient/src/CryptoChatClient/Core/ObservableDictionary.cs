﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace CryptoChatClient.Core
{
    public class ObservableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, INotifyCollectionChanged, INotifyPropertyChanged where TValue : ObservableModel
    {
        protected int _indexCount = -1;

        public event NotifyCollectionChangedEventHandler? CollectionChanged;
        public event PropertyChangedEventHandler? PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {

            CollectionChanged?.Invoke(this, e);
        }

        protected virtual void RecalculateIndex(int index)
        {
            Func<KeyValuePair<TKey, TValue>, bool> condition = pair => pair.Value.index > index;
            foreach (var pair in this)
            {
                if (condition(pair))
                {
                    --pair.Value.index;
                }
            }
        }
    }
}