﻿using CryptoChatClient.Models;

namespace CryptoChatClient.Core
{
    public interface INavigationService
    {
        ViewModelBase CurrentViewModel { get; }
        void NavigateTo<T>() where T : ViewModelBase;
        void NavigateTo<T>(CurrentUserModel currentUserModel) where T : ViewModelBase;
    }
}
