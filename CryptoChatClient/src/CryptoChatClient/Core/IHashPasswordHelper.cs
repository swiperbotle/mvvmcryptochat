﻿namespace CryptoChatClient.Core
{
    public interface IHashPasswordHelper
    {
        public string HashPassword(string password);
        public bool VerifyPassword(string password, string hashedPassword);
    }
}
