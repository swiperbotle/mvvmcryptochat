﻿using CryptoChatClient.Core;
using System;

namespace CryptoChatClient.Models
{
    public class MessageObservableModel : ObservableModel
    {
        #region required_fields
        public Guid ChatId { get; set; }
        public Int64 MessageId { get; set; }
        public string? Nickname { get; set; }
        public string? Avatar { get; set; }
        public string MessageText
        {
            get; set;
        }
        public string? ObtainTime { get; set; }
        #endregion

        #region optional_fields
        public string? ObtainStatus { get; set; }
        public string? ReplyMsg { get; set; }
        public string? ReplyNickname { get; set; }
        public string? Reaction { get; set; }
        public bool IsSelf { get; set; }
        #endregion

        public MessageObservableModel(Int64 messageId, Guid chatId, string messageText)
        {
            this.MessageId = messageId;
            this.ChatId = chatId;
            this.MessageText = messageText;
        }
        public MessageObservableModel()
        {

        }

    }
}
