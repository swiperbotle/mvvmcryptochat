﻿using CryptoChatClient.Core;
using System;

namespace CryptoChatClient.Models
{
    public class UserObservableModel : ObservableModel
    {
        public Guid Id { get; set; }
        public string? Username { get; set; }
        public string? Nickname { get; set; }
        public string? Bio { get; set; }
        public string? Avatar { get; set; }
        public bool? IsPrivate { get; set; }
    }
}
