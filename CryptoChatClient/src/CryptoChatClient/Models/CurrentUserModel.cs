﻿using CryptoChatClient.Core;
using System;

namespace CryptoChatClient.Models
{
    public class CurrentUserModel : ObservableObject
    {
        public Guid Id { get; set; }
        private string _username;
        public string Username
        {
            get
            {
                return _username;
            }
            set { _username = value; OnPropertyChanged(nameof(Username)); }
        }
        public string Password { get; set; }
        public string? Nickname { get; set; }
        public string? Bio { get; set; }
        private string _avatar;
        public string Avatar
        {
            get
            {
                return _avatar;
            }
            set { _avatar = value; OnPropertyChanged(nameof(Avatar)); }
        }
        public bool IsPrivate { get; set; }
        public CurrentUserModel(CurrentUserModel currentUserModel)
        {
            this.Id = currentUserModel.Id;
            this.Nickname = currentUserModel.Nickname;
            this.Bio = currentUserModel.Bio;
            this.Avatar = currentUserModel.Avatar;
            this.Username = currentUserModel.Username;
            this.Password = currentUserModel.Password;
            this.IsPrivate = currentUserModel.IsPrivate;
        }
        public CurrentUserModel()
        {

        }
    }
}
