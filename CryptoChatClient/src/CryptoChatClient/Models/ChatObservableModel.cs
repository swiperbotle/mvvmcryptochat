﻿using CryptoChatClient.Core;
using System;

namespace CryptoChatClient.Models
{
    public class ChatObservableModel : ObservableModel
    {
        #region required_fields
        //chat id = user id
        public Guid ChatId { get; set; }
        public int CountNew { get; set; }
        public Int64 MessageId { get; set; }
        #region NOT_DTO_PROPERTIES
        private string _lastMessage;
        public string? LastMessage
        {
            get => _lastMessage;
            set
            {
                if (_lastMessage != value)
                {
                    _lastMessage = value;
                    OnPropertyChanged(nameof(LastMessage));
                }
            }
        }
        #endregion
        public string? SenderName { get; set; }
        public string? PinMessage { get; set; }
        public string Avatar { get; set; }
        public double ScrollOffset { get; set; }
        public bool IsFavourite { get; set; }
        public bool IsPeople { get; set; }
        #endregion

        public ChatObservableModel(Guid chatId)
        {
            this.ChatId = chatId;
        }
        public ChatObservableModel()
        {

        }
    }
}
