﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace CryptoChatClient.UserControls
{
    /// <summary>
    /// Interaction logic for TextBoxCutsomCaret.xaml
    /// </summary>
    public partial class TextBoxCutsomCaret : UserControl
    {
        public TextBoxCutsomCaret()
        {
            InitializeComponent();
            this.CustomTextBox.SelectionChanged += (sender, e) => MoveCustomCaret();
            this.CustomTextBox.LostFocus += (sender, e) => Caret.Visibility = Visibility.Collapsed;
            this.CustomTextBox.GotFocus += (sender, e) => Caret.Visibility = Visibility.Visible;
        }
        private void MoveCustomCaret()
        {
            var caretLocation = CustomTextBox.GetRectFromCharacterIndex(CustomTextBox.CaretIndex).Location;

            if (!double.IsInfinity(caretLocation.X))
            {
                Canvas.SetLeft(Caret, caretLocation.X);
            }

            if (!double.IsInfinity(caretLocation.Y))
            {
                Canvas.SetTop(Caret, caretLocation.Y);
            }
        }
        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
            }
        }
    }
}