﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CryptoChatClient.Exceptions;
using CryptoChatClient.Helpers;
using CryptoChatClient.ORM.Contexts;
namespace CryptoChatClient.ORM.HostBuilders
{
    public static class AddDbContextHostBuilderExtension
    {
        public static IHostBuilder AddDbContext(this IHostBuilder hostBuilder)
        {
            hostBuilder.ConfigureServices((context, services) =>
            {
                try
                {
                    string? connectionString = context.Configuration.GetConnectionString("sqlite");
                    if (connectionString == null)
                    {
                        throw new ConfigurationNotFoundException("appsettings.json not found");
                    }
                    services.AddSingleton(new DbContextOptionsBuilder().UseSqlite(connectionString).Options);
                    services.AddSingleton<CryptoChatDbContextFactory>();
                }
                catch (ConfigurationNotFoundException ex)
                {
                    ShowErrorHelper.ShowError(ex);
                }
            });
            return hostBuilder;
        }
    }
}
