﻿using Microsoft.EntityFrameworkCore;

namespace CryptoChatClient.ORM.Contexts
{
    public class CryptoChatDbContextFactory : IDbContextFactory<CryptoChatDbContext>

    {
        private readonly DbContextOptions _options;

        public CryptoChatDbContextFactory(DbContextOptions options)
        {
            _options = options;
        }

        public CryptoChatDbContext CreateDbContext()
        {
            return new CryptoChatDbContext(_options);
        }
    }
}
