﻿using Microsoft.EntityFrameworkCore;
using CryptoChatClient.ORM.DTOs;

namespace CryptoChatClient.ORM.Contexts
{
    public class CryptoChatDbContext : DbContext
    {
        public CryptoChatDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<ChatDTO> Chats { get; set; }
        public DbSet<UserDTO> Users { get; set; }
        public DbSet<MessageDTO> Messages { get; set; }
    }
}
