﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace CryptoChatClient.ORM.Contexts
{
    public class CryptoChatDesignTimeDbContextFactory : IDesignTimeDbContextFactory<CryptoChatDbContext>
    {
        public CryptoChatDbContext CreateDbContext(string[]? args = null)
        {
            return new CryptoChatDbContext(new DbContextOptionsBuilder().UseSqlite("Data Source=CryptoChat.db").Options);
        }
    }
}
