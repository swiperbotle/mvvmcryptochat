﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CryptoChatClient.ORM.DTOs
{
    public class UserDTO
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        public string? Nickname { get; set; }
        public string? Bio { get; set; }
        [Required]
        public string Avatar { get; set; }
        [Required]
        public bool IsPrivate { get; set; }
    }
}
