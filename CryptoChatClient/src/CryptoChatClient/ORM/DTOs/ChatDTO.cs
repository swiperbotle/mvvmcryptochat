﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CryptoChatClient.ORM.DTOs
{
    public class ChatDTO
    {
        [Key]
        public Guid ChatId { get; set; }
        public IEnumerable<MessageDTO> Messages { get; set; } // navigation property
        public string? Nickname { get; set; }
        [Required]
        public int CountNew { get; set; }
        public int? MessageId { get; set; }
        public string? SenderName { get; set; }
        public string? PinMessage { get; set; }
        [Required]
        public string Avatar { get; set; }
        public double ScrollOffset { get; set; }
        public bool IsFavourite { get; set; }
        public bool IsPeople { get; set; }
    }
}
