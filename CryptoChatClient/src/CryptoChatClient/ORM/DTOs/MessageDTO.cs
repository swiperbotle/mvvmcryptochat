﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CryptoChatClient.ORM.DTOs
{
    public class MessageDTO
    {
        [Key]
        public Int64 MessageId { get; set; }
        public Guid ChatId { get; set; }
        public ChatDTO ChatDTO { get; set; } // navigation property
        public DateTime SendTime { get; set; }
        public string? ReplyMessage { get; set; }
        public int ReceiveStatus { get; set; }
        [Required]
        public string MessageText { get; set; }
    }
}
