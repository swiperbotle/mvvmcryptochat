﻿namespace CryptoChatClient.POCOs
{
    public class ServerSettings
    {
        public string SignalingIp { get; set; }
        public string StunIp { get; set; }
        public int StunPort { get; set; }
    }
}
