﻿using System;

namespace CryptoChatClient.Exceptions
{
    public class NoPrepareUpdateUserException : Exception
    {
        public NoPrepareUpdateUserException() : base("Error. PrepareUserModel not found in the db repository.")
        {
        }
        public NoPrepareUpdateUserException(string message) : base(message)
        {
        }

        public NoPrepareUpdateUserException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
