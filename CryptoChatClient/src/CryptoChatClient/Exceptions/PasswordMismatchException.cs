﻿using System;

namespace CryptoChatClient.Exceptions
{
    public class PasswordMismatchException : Exception
    {
        public PasswordMismatchException() : base("Error. Password mismatch.")
        {

        }
        public PasswordMismatchException(string message) : base(message)
        {
        }
        public PasswordMismatchException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
