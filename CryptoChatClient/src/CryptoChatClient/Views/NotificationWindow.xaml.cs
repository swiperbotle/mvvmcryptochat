﻿using System.Windows;

namespace CryptoChatClient.Views
{
    /// <summary>
    /// Interaction logic for NotificationWindow.xaml
    /// </summary>
    public partial class NotificationWindow : Window
    {
        public string DisplayHeader { get; set; }
        public string DisplayCause { get; set; }
        public NotificationWindow()
        {
            InitializeComponent();
        }

        public NotificationWindow(string header, string cause)
        {
            DisplayHeader = header;
            DisplayCause = cause;
            DataContext = this;
            InitializeComponent();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            var window = GetWindow(this);
            window.Close();
        }
    }
}
