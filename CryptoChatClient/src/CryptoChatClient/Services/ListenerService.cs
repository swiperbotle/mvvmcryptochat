﻿using CryptoChatClient.Core;
using CryptoChatClient.Coturn;
using CryptoChatClient.Enums;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.Repositories;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
namespace CryptoChatClient.Services
{
    public class ListenerService : IListenerService
    {
        string _signalingServerIp;
        ClientWebSocket _clientWebSocket;
        public const string certStr = @"-----BEGIN CERTIFICATE-----
MIIFqTCCA5GgAwIBAgIUSPWZ37e6ptg1zcgHab1XNG0nC4owDQYJKoZIhvcNAQEL
BQAwZDELMAkGA1UEBhMCVUExEzARBgNVBAgMClNvbWUtU3RhdGUxITAfBgNVBAoM
GEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDEdMBsGA1UEAwwUc2lnbmFsaW5nX3Nl
cnZlci5jb20wHhcNMjQwMzI4MDcyNjM0WhcNMzQwMzI2MDcyNjM0WjBkMQswCQYD
VQQGEwJVQTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQg
V2lkZ2l0cyBQdHkgTHRkMR0wGwYDVQQDDBRzaWduYWxpbmdfc2VydmVyLmNvbTCC
AiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBALFEPiqjkux1SBOdIbTPdjTP
R/Oroni80wwEPnz5vbhpFA6MzZt/5z8JNx6stYlogc+L6Zm7iedYV1uMduO1mtW1
y0VkDOuoTzxToZYayDbF0aCVL5kwIE5otAj938YYaqVwQ2ktWz54kfHrFy7h9zzI
ceQVncOjPh/mSl6GQ7Rllw7C9kTGQ9mVaoKn73VGP/af9HQBUtkFSdnonavkTHtj
wouhen4dkWG+/qMp9ZG09renRI73qfqYA/EVMEbHDnWJpxb5QBkqh6ny+3VhAuvy
vzNaAx/koUgZ0b7QkKJH7StGUI11l6I2E/tsRHRbIEBRDyImE2W44OVzxvJwZmWD
BK47b+zkI8uXvKnq+wxqcR5OOcKVvuudbEiwOpkWorLOyv+Yqfbq2UkJg76IsyHW
+PyE3TpbMJcAiGMR55wHLm8IGmsoZ+2Kmeh1mtK1vQ3O57irpRiGarzhqHlBRBXt
RHwPUDKxxzYLX4EENCt5lXInNiiQRH+9uXbW8Aj00a6nlXBOpBd+V1hLBT4/flzY
E6lRS7Gx3TpaNw84G/TP92p+RX4GhpDC+OCCDwVtKexqZXlFj/gdePZIR0/+c6YZ
DpIOUvyfq2WnUJ1ZCTnjjVHwfGxcDb15V3Dr5kngiuTae5oSu6GKUOEBJ8/b7PpI
R+haa3FKuMxUWPWASKatAgMBAAGjUzBRMB0GA1UdDgQWBBQlBIdglzuIB6+t98+m
eUe7UcNQjjAfBgNVHSMEGDAWgBQlBIdglzuIB6+t98+meUe7UcNQjjAPBgNVHRMB
Af8EBTADAQH/MA0GCSqGSIb3DQEBCwUAA4ICAQBuikU1mYFLS8PdZ938jJGMAtz8
ZM6KpDcZJ1xMLs/5ohzsOdt64Qwd5VOj9Y8Wbu7DRhBMYKS3S0q3voVQVELQpuv2
gIDd0km1XNKVQ6oZhGJQVrcx2ftGQP57WDXkmvab4fgq3qxOwFVVwZUNfY+TKW53
7W+c3WhaHLuGUQhh8wCJy161MIWkZHaDUyzA1d5ZTJjyqbseWJC2Kx8sydQ4gtq8
4hh/Rj0mu2sO9JnXYILWQ45hIN98g+8vkBRcsW7jKc+jfZLg0lUYjMgXGu/bV3Ur
CLb/UJa0CE0mB8SXoZOAoqiOm1Isiv2uyPq+qX2G7zqAxf2hmphiyhgttHiFVsai
IYtHVOvSShKFwZHkaEYTlsNy4CSXjYJYsS9WP9wbUpj4cSvChTx9tjh8Avvvg27F
NYG+ytuv219W1OQTaxYrZvCLNaASqZN3XFKXUpi+n5PG29Lm95HJOaMZI86/adQK
PF7erAdAUnB3YZFUoyQYayueA/JgRiWmX+7fDvfTK/KboV0pswYxYuKJ6OYyRzBF
ndQPFRXiL4gVpIE7lHkR2wsD/9QeXwZ2tKEBfxed7tKO7KxZEm7X6jaAt+HCm1yI
iKYFIjRXcIq4JU3XXFHv75G/9Id6e9W4CEV4Wgh9GQeu3rNybdwQLQ9Llo8WBjs+
igc84acNGCuiFM7tQA==
-----END CERTIFICATE-----";
        X509Certificate2 _certificate;
        private bool _isListening;
        private CancellationTokenSource _cancellationTokenSource;
        private readonly object _lock = new object();
        private TaskCompletionSource<(byte[], int)> _tcs;
        private TaskCompletionSource<bool> _deleteTcs;
        private TaskCompletionSource<bool> _updateTcs;
        private readonly object _tcsLock = new object();
        private readonly object _deleteTcsLock = new object();
        private readonly object _updateTcsLock = new object();
        string _username; string _password;
        string _stunServerIp; uint _stunServerPort;
        Dictionary<Guid, List<Tuple<string, int, MessageObservableModel>>> waitedMessagesToSend;
        private readonly object _awaitedMessagedLock = new object();
        private readonly CryptoChatRepository _cryptoChatRepository;
        HomeViewModel _homeViewModel;
        private bool IsListening()
        {
            lock (_lock)
            {
                return _isListening;
            }
        }

        private void SetListening(bool value)
        {
            lock (_lock)
            {
                _isListening = value;
            }
        }

        public ListenerService(CryptoChatRepository cryptoChatRepository, string username = "1709740831", string password = "MTIzNDU2Nzg5MA==", uint serverPort = 3478, string serverIp = "192.168.0.107")
        {
            _cryptoChatRepository = cryptoChatRepository;
            _username = username;
            _password = password;
            _cancellationTokenSource = new CancellationTokenSource();
            SetListening(false);
            var cert = new ReadOnlySpan<Char>(certStr.ToCharArray());
            _certificate = X509Certificate2.CreateFromPem(cert);
            _clientWebSocket = new ClientWebSocket();
            _clientWebSocket.Options.RemoteCertificateValidationCallback = ValidateCertificate;
            _clientWebSocket.Options.ClientCertificates.Add(_certificate);
            waitedMessagesToSend = new Dictionary<Guid, List<Tuple<string, int, MessageObservableModel>>>();
        }

        public WebSocketState GetStatus()
        {
            return _clientWebSocket.State;
        }

        private static bool ValidateCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            var cert = new ReadOnlySpan<Char>(ListenerService.certStr.ToCharArray());
            if (new X509Certificate2(certificate).Equals(X509Certificate2.CreateFromPem(cert)) && sslPolicyErrors.HasFlag(SslPolicyErrors.None))
                return true;
            return false;
        }
        public async Task<bool> StartListen(string signalingServerIp, string stunServerIp, int stunServerPort)
        {
            _stunServerIp = stunServerIp;
            _stunServerPort = (uint)stunServerPort;
            _clientWebSocket = new ClientWebSocket();
            _clientWebSocket.Options.RemoteCertificateValidationCallback = ValidateCertificate;
            _clientWebSocket.Options.ClientCertificates.Add(_certificate);
            if (signalingServerIp == null)
            {
                ShowErrorHelper.ShowDebug("Error. Ip address is null, change configuration in User/AppData/CryptoChat/settings.json.");
                return false;
            }
            _signalingServerIp = signalingServerIp;
            if (!await InternetAvailableHelper.IsInternetAvailable(_signalingServerIp))
            {
                ShowErrorHelper.ShowDebug("Error, Signaling server is unreacheable.");
                return false;
            }
            await _clientWebSocket.ConnectAsync(new Uri($"wss://{_signalingServerIp}:443/ws"), CancellationToken.None);
            if (_clientWebSocket.State == WebSocketState.Open)
                return true;
            return false;
        }

        ~ListenerService()
        {
            if (_clientWebSocket.State != WebSocketState.Closed)
                _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None);
            _clientWebSocket?.Dispose();
        }
        async Task<Tuple<byte[], int>> ReceiveAsync()
        {
            var buffer = new byte[1024 * 10];
            var result = await _clientWebSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            return new Tuple<byte[], int>(buffer, result.Count);
        }

        public void ForceClose()
        {
            _homeViewModel = null;
            SetListening(false);
            _cancellationTokenSource.Cancel();
            _clientWebSocket.Abort();
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async Task CloseGracefully()
        {
            _homeViewModel = null;
            SetListening(false);
            _cancellationTokenSource.Cancel();
            try
            {
                if (_clientWebSocket.State == WebSocketState.Open || _clientWebSocket.State == WebSocketState.CloseReceived)
                {
                    await _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None);
                }
            }
            catch (Exception ex)
            {
                ShowErrorHelper.ShowError(ex);
            }
            _cancellationTokenSource = new CancellationTokenSource();
        }

        public async Task ListenAsync(HomeViewModel homeViewModel)
        {
            _homeViewModel = homeViewModel;
            try
            {
                SetListening(true);
                while (IsListening())
                {
                    var buffer = new ArraySegment<byte>(new byte[1024 * 10]);
                    WebSocketReceiveResult result = null;

                    try
                    {
                        result = await _clientWebSocket.ReceiveAsync(buffer, _cancellationTokenSource.Token);
                    }
                    catch (OperationCanceledException)
                    {
                        break;
                    }
                    if (result.MessageType == WebSocketMessageType.Close)
                    {
                        SetListening(false);
                        await _clientWebSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None);
                    }
                    else
                    {
                        var msgType = BitConverter.ToInt32(buffer.Array[..4]);
                        var inbound = buffer[..result.Count];
                        if (msgType == (int)MessageType.Conversation_Request)
                        {
                            var conTypeBytes = inbound[36..40];
                            var msgType1 = BitConverter.GetBytes((int)MessageType.Conversation_Accept_Response);
                            var connectionType = Encoding.UTF8.GetString(conTypeBytes);
                            var idBytes = inbound[20..36];
                            var destBytes = inbound[4..20];
                            var reflexiveIpSelf = "";
                            uint reflexivePortSelf = 0;
                            int clientPort;
                            var random = new Random();
                            do
                            {
                                clientPort = random.Next(1024, 65535);
                            } while (IsPortInUse(clientPort));
                            if (connectionType == "STUN")
                            {
                                var stunClient = new StunClient("0.0.0.0", clientPort, isFingerprint: true);
                                var res1 = await stunClient.StunRequest(_stunServerIp, _stunServerPort);
                                reflexiveIpSelf = res1.Item2!;
                                reflexivePortSelf = res1.Item3;
                            }
                            else if (connectionType == "TURN")
                            {
                                // TODO:turnClient should be dedicated object in the User class with Dispose() method
                                var turnClient = new TurnClient("0.0.0.0", clientPort, password: _password, username: _username,
                                                   isFingerprint: true);
                                var res1 = await turnClient.TurnAllocateRequest(_stunServerIp, _stunServerPort, lifetime: 777, evenPort: true, dontFragment: true);
                                reflexiveIpSelf = res1.Item4!;
                                reflexivePortSelf = res1.Item5;
                            }
                            var msg = msgType1.Concat(idBytes).Concat(destBytes).Concat(conTypeBytes).Concat(IPAddress.Parse(reflexiveIpSelf).GetAddressBytes()).Concat(BitConverter.GetBytes(reflexivePortSelf)).ToArray();
                            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
                            using (var conn1 = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                            {
                                var reflexiveIp1 = new IPAddress(buffer[40..44]).ToString();
                                var reflexivePort1 = BitConverter.ToInt32(buffer[44..48]);
                                conn1.Bind(new IPEndPoint(IPAddress.Parse(reflexiveIpSelf), (int)reflexivePortSelf));
                                byte[] bufferMsg = new byte[8192];
                                IPEndPoint clientEndpoint = new IPEndPoint(IPAddress.Any, 0);
                                var receivedLength = await conn1.ReceiveAsync(bufferMsg);
                                var encryptor = new PostQuantumEncryptor(bufferMsg[..receivedLength]);
                                Array.Clear(bufferMsg, 0, bufferMsg.Length);
                                var receiveTask = conn1.ReceiveAsync(bufferMsg);
                                await conn1.SendToAsync(encryptor.GetCipherText()!, new IPEndPoint(IPAddress.Parse(reflexiveIp1), reflexivePort1));
                                var receivedData = await receiveTask;
                                var KeyAES = encryptor.Secret!;
                                var IvAES = SHA1.HashData(KeyAES).Take(16).ToArray();
                                var bytesEncrypted = Convert.FromBase64String(Encoding.UTF8.GetString(bufferMsg[..receivedData]));
                                var decryptedText = AES.Decrypt(bytesEncrypted, KeyAES, IvAES);
                                var messageModelInternal = JsonSerializer.Deserialize<MessageObservableModel>(decryptedText);
                                var chatId = messageModelInternal.ChatId;
                                MessageModels messageModels;
                                if (!_homeViewModel.MessageChatModels.ContainsKey(chatId))
                                {
                                    var chatModel = new ChatObservableModel();
                                    chatModel.ChatId = chatId;
                                    chatModel.SenderName = messageModelInternal.Nickname;
                                    chatModel.Avatar = messageModelInternal.Avatar;
                                    chatModel.LastMessage = messageModelInternal.MessageText;
                                    messageModels = new MessageModels
                                {
                                    { messageModelInternal.MessageId, messageModelInternal }
                                };
                                    _homeViewModel.MessageChatModels.Add(chatId, messageModels);
                                    _homeViewModel.Chats.Add(chatId, chatModel);
                                }
                                else
                                {
                                    _homeViewModel.MessageChatModels[chatId].Add(messageModelInternal.MessageId, messageModelInternal);
                                    _homeViewModel.Chats[chatId].LastMessage = messageModelInternal.MessageText;
                                }
                                if (_homeViewModel.CurrentMessageViewModels.ChatId == chatId)
                                {
                                    _homeViewModel.CurrentMessageViewModels.Add(messageModelInternal.MessageId, messageModelInternal);
                                }
                            }
                        }
                        else if (msgType == (int)MessageType.Conversation_Success_Response)
                        {

                        }
                        else if (msgType == (int)MessageType.Update_Success_Response)
                        {
                            lock (_updateTcsLock)
                            {
                                if (_updateTcs != null)
                                {
                                    var notificationWindow = new NotificationWindow("Update user successful", "");
                                    notificationWindow.Show();
                                    _updateTcs.SetResult(true);
                                    _updateTcs = null;
                                }
                            }
                        }
                        else if (msgType == (int)MessageType.Error_Response)
                        {
                            var errorType = BitConverter.ToInt32(buffer.Array[4..8]);
                            if (errorType == (int)ErrorType.Update_User_Already_Exist_Error)
                            {

                                lock (_updateTcsLock)
                                {
                                    if (_updateTcs != null)
                                    {
                                        var notificationWindow =
                                             new NotificationWindow("Update user error", "Username already taken.");
                                        notificationWindow.Show();
                                        _updateTcs.SetResult(false);
                                        _updateTcs = null;
                                    }
                                }
                            }
                        }
                        else if (msgType == (int)MessageType.Remove_Success_Response)
                        {
                            lock (_deleteTcsLock)
                            {
                                if (_deleteTcs != null)
                                {
                                    try
                                    {
                                        var notificationWindow = new NotificationWindow("Delete user successful", "");
                                        notificationWindow.Show();
                                        _deleteTcs.SetResult(true);
                                        _deleteTcs = null;
                                    }
                                    catch (Exception ex)
                                    {
                                        ShowErrorHelper.ShowError(ex);
                                    }
                                }
                            }
                        }
                        else if (msgType == (int)MessageType.Conversation_Accept_Response)
                        {
                            var reflexiveIp1 = new IPAddress(buffer[40..44]).ToString();
                            var reflexivePort1 = BitConverter.ToInt32(buffer[44..48]);
                            var chatId = new Guid(inbound[4..20]);
                            string? clientIp = "";
                            int clientPort = 0;
                            var newMessage = new MessageObservableModel();
                            lock (_awaitedMessagedLock)
                            {
                                clientIp = waitedMessagesToSend[chatId].FirstOrDefault().Item1;
                                clientPort = waitedMessagesToSend[chatId].FirstOrDefault().Item2;
                                newMessage = waitedMessagesToSend[chatId].FirstOrDefault().Item3;
                                waitedMessagesToSend[chatId].Remove(waitedMessagesToSend[chatId].FirstOrDefault());
                            }
                            using (var conn1 = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
                            {
                                conn1.Bind(new IPEndPoint(IPAddress.Parse(clientIp), clientPort));
                                var destinationEndPoint = new IPEndPoint(IPAddress.Parse(reflexiveIp1), reflexivePort1);
                                byte[] bufferMsg = new byte[8192];
                                var encryptor = new PostQuantumEncryptor();
                                var receiveTask = conn1.ReceiveAsync(bufferMsg);
                                await conn1.SendToAsync(encryptor.GetPublicKey()!, destinationEndPoint);
                                var cipherTextLength = await receiveTask;
                                encryptor.ExctractSecret(bufferMsg[..cipherTextLength]);
                                var KeyAES = encryptor.Secret!;
                                var IvAES = SHA1.HashData(KeyAES).Take(16).ToArray();
                                var cipherText = AES.Encrypt(JsonSerializer.Serialize<MessageObservableModel>(newMessage), KeyAES, IvAES);
                                var encryptedText = Convert.ToBase64String(cipherText);
                                await conn1.SendToAsync(Encoding.UTF8.GetBytes(encryptedText), destinationEndPoint);
                            }

                        }
                        else if (msgType == (int)MessageType.Conversation_Decline_Response)
                        {
                            var notificationWindow = new NotificationWindow("Error", "Peer rejected connection.");
                            notificationWindow.Show();
                        }
                        lock (_tcsLock)
                        {
                            if (_tcs != null)
                            {
                                try
                                {
                                    if (msgType == (int)MessageType.Get_MessageId_Success || msgType == (int)MessageType.Search_Success_Response || msgType == (int)MessageType.Error_Response)
                                    {
                                        _tcs.SetResult((buffer.Array, result.Count));
                                        _tcs = null;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    ShowErrorHelper.ShowError(ex);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ShowErrorHelper.ShowError(ex);
            }
        }
        private bool IsPortInUse(int port)
        {
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] udpEndPoints = ipGlobalProperties.GetActiveUdpListeners();

            return udpEndPoints.Any(p => p.Port == port);
        }
        public async Task<bool> ConversationRequest(Guid userId, Guid destId, MessageObservableModel message, string connectionType = "STUN", string clientIp = "0.0.0.0", int clientPort = 12345, string username = "1709740831", string password = "MTIzNDU2Nzg5MA==")
        {
            var msgType = BitConverter.GetBytes((int)MessageType.Conversation_Request);
            var idBytes = userId.ToByteArray();
            var destBytes = destId.ToByteArray();
            var conTypeBytes = Encoding.UTF8.GetBytes(connectionType);
            string reflexiveIp = "";
            uint reflexivePort = 0;
            if (!await InternetAvailableHelper.IsInternetAvailable(_stunServerIp))
            {
                var notificationWindow = new NotificationWindow("Error", "STUN server unreacheable. Check Internet connection or configure properly ip address in User/AppData/Rouming/CryptoChat/settings.json");
                notificationWindow.Show();
                return false;
            }
            if (connectionType == "STUN")
            {
                var stunClient = new StunClient(clientIp, clientPort, isFingerprint: true);
                var result = await stunClient.StunRequest(_stunServerIp, _stunServerPort);
                reflexiveIp = result.Item2!;
                reflexivePort = result.Item3;
            }
            else if (connectionType == "TURN")
            {
                // TODO: turnClient should be dedicated object in the User class with Dispose() method
                var turnClient = new TurnClient(clientIp, clientPort, password: password, username: username,
                                   isFingerprint: true);
                var result = await turnClient.TurnAllocateRequest(_stunServerIp, _stunServerPort, lifetime: 777, evenPort: true, dontFragment: true);
                reflexiveIp = result.Item4!;
                reflexivePort = result.Item5;
            }
            lock (_awaitedMessagedLock)
            {
                {
                    if (!waitedMessagesToSend.ContainsKey(destId))
                    {
                        waitedMessagesToSend.Add(destId, new List<Tuple<string, int, MessageObservableModel>> { new Tuple<string, int, MessageObservableModel>(reflexiveIp, (int)reflexivePort, message) });
                    }
                    else
                    {
                        waitedMessagesToSend[destId].Add(new Tuple<string, int, MessageObservableModel>(reflexiveIp, (int)reflexivePort, message));
                    }
                }
            }
            var msg = msgType.Concat(idBytes).Concat(destBytes).Concat(conTypeBytes).Concat(IPAddress.Parse(reflexiveIp).GetAddressBytes()).Concat(BitConverter.GetBytes(reflexivePort)).ToArray();
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            return true;
        }

        public async Task<byte[]> GetMessageIdRequest(Guid userId)
        {
            var msgType = BitConverter.GetBytes((int)MessageType.Get_MessageId_Request);
            var idBytes = userId.ToByteArray();
            var msg = msgType.Concat(idBytes).ToArray();
            lock (_tcsLock)
            {
                _tcs = new TaskCompletionSource<(byte[], int)>();
            }
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            var res = await _tcs.Task;
            return res.Item1[0..res.Item2];
        }

        public async Task<byte[]> SearchRequest(Guid userId, string username)
        {
            var msgType = BitConverter.GetBytes((int)MessageType.Search_Request);
            var idBytes = userId.ToByteArray();
            var msg = msgType.Concat(idBytes).Concat(Encoding.UTF8.GetBytes(username)).ToArray();
            lock (_tcsLock)
            {
                _tcs = new TaskCompletionSource<(byte[], int)>();
            }
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            var res = await _tcs.Task;
            return res.Item1[0..res.Item2];
        }

        public async Task<byte[]> SignInRequest(string username, string password)
        {
            var msgType = BitConverter.GetBytes((int)MessageType.SignIn_Request);
            var bytesUsername = Encoding.UTF8.GetBytes(username);
            var bytesPassword = Encoding.UTF8.GetBytes(password);
            Array.Resize(ref bytesUsername, 8);
            Array.Resize(ref bytesPassword, 8);
            var msg = msgType.Concat(bytesUsername).Concat(bytesPassword).ToArray();
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            var res = await ReceiveAsync();
            return res.Item1[0..res.Item2];
        }

        public async Task<byte[]> SignUpRequest(CurrentUserModel userModel)
        {
            var msgType = BitConverter.GetBytes((int)MessageType.SignUp_Request);
            var jsonBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(userModel));
            var msg = msgType.Concat(jsonBytes).ToArray();
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            var res = await ReceiveAsync();
            return res.Item1[0..res.Item2];
        }

        public async Task RemoveRequest(Guid userId)
        {
            var msgType = BitConverter.GetBytes((int)MessageType.Remove_Request);
            var idBytes = userId.ToByteArray();
            var msg = msgType.Concat(idBytes).ToArray();
            lock (_deleteTcsLock)
            {
                _deleteTcs = new TaskCompletionSource<bool>();
            }
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            await _deleteTcs.Task;
        }

        public async Task<bool> UpdateRequest(CurrentUserModel userModel)
        {
            var msgType = BitConverter.GetBytes((int)MessageType.Update_Request);
            var idBytes = userModel.Id.ToByteArray();
            var jsonBytes = Encoding.UTF8.GetBytes(JsonSerializer.Serialize(userModel));
            var msg = msgType.Concat(idBytes).Concat(jsonBytes).ToArray();
            lock (_updateTcsLock)
            {
                _updateTcs = new TaskCompletionSource<bool>();
            }
            await _clientWebSocket.SendAsync(new ArraySegment<byte>(msg), WebSocketMessageType.Binary, true, CancellationToken.None);
            return await _updateTcs.Task;
        }
    }
}
