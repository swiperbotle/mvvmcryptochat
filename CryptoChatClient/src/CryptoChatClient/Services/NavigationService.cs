﻿using CryptoChatClient.Core;
using CryptoChatClient.Models;
using CryptoChatClient.ViewModels;
using System;
namespace CryptoChatClient.Services
{
    public class NavigationService : ObservableObject, INavigationService
    {
        protected readonly Func<Type, ViewModelBase>? _viewModelFactory;
        protected ViewModelBase? _currentViewModel;
        public ViewModelBase? CurrentViewModel
        {
            get => _currentViewModel;
            set
            {
                _currentViewModel = value;
                OnPropertyChanged();
            }
        }
        public NavigationService(Func<Type, ViewModelBase> viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
        }
        public void NavigateTo<TViewModel>() where TViewModel : ViewModelBase
        {
            var viewModel = _viewModelFactory?.Invoke(typeof(TViewModel));
            CurrentViewModel = viewModel;
        }

        public void NavigateTo<TViewModel>(CurrentUserModel currentUserModel) where TViewModel : ViewModelBase
        {
            var viewModel = _viewModelFactory?.Invoke(typeof(TViewModel));
            CurrentViewModel = viewModel;
            if (CurrentViewModel is HomeViewModel homeViewModel)
            {
                homeViewModel.CurrentUser = currentUserModel;
            }
        }
    }
}
