﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using CryptoChatClient.Helpers;
using CryptoChatClient.Models;
using CryptoChatClient.ORM.Contexts;
using CryptoChatClient.ORM.DTOs;
using System;
using System.Threading.Tasks;

namespace CryptoChatClient.Repositories
{
    public class CryptoChatRepository
    {
        private readonly CryptoChatDbContext _dbContext;
        private readonly IMapper _mapper;
        public CryptoChatRepository(CryptoChatDbContextFactory dbContextFactory, IMapper mapper)
        {
            _dbContext = dbContextFactory.CreateDbContext();
            _mapper = mapper;
            _dbContext.Database.EnsureCreated();
            //_dbContext.Database.Migrate();
        }

        ~CryptoChatRepository()
        {
            this.Dispose();
        }
        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public async Task CreateUser(CurrentUserModel currentUserModel)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var userDTOtemp = await _dbContext.Users.FirstOrDefaultAsync();
                    if (userDTOtemp != null)
                    {
                        _dbContext.Users.Remove(userDTOtemp);
                    }
                    var userDTO = _mapper.Map<UserDTO>(currentUserModel);
                    await _dbContext.Users.AddAsync(userDTO!);
                    await _dbContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (DbUpdateException ex)
                {
                    await transaction.RollbackAsync();
                    ShowErrorHelper.ShowError(ex);
                }
            }
        }

        public async Task<CurrentUserModel?> ReadUser()
        {
            var userDTO = await _dbContext.Users.FirstOrDefaultAsync();
            var currentUserModel = _mapper.Map<CurrentUserModel>(userDTO);
            return currentUserModel;
        }
        public async Task UpdateUser(CurrentUserModel currentUserModel)
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var userDTOtemp = await _dbContext.Users.FirstOrDefaultAsync();
                    if (userDTOtemp == null)
                    {
                        return;
                    }
                    var userDTO = _mapper.Map<UserDTO>(currentUserModel);
                    await _dbContext.SaveChangesAsync();
                    await transaction.CommitAsync();
                }
                catch (DbUpdateException ex)
                {
                    await transaction.RollbackAsync();
                    ShowErrorHelper.ShowError(ex);
                }
            }
        }

        public async Task DeleteUser()
        {
            using (var transaction = _dbContext.Database.BeginTransaction())
            {
                try
                {
                    var userDTO = await _dbContext.Users.FirstOrDefaultAsync();
                    if (userDTO != null)
                    {
                        _dbContext.Users.Remove(userDTO);
                        await _dbContext.SaveChangesAsync();
                        await transaction.CommitAsync();
                    }
                }
                catch (DbUpdateException ex)
                {
                    await transaction.RollbackAsync();
                    ShowErrorHelper.ShowError(ex);
                }
            }
        }


    }
}
