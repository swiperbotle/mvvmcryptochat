﻿using CryptoChatClient.Helpers;
using System;
using System.Globalization;
using System.Windows.Data;

namespace CryptoChatClient.Converters
{
    public class StringToImageBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string avatarB64)
            {
                return AvatarHelper.InjectBitmap(avatarB64);
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
