﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace CryptoChatClient.Converters
{
    public class HalfHeightMarginConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is double height)
            {
                double halfHeight = height / 2 - 20;
                return new Thickness(0, halfHeight, 0, 0);
            }

            return new Thickness(0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
