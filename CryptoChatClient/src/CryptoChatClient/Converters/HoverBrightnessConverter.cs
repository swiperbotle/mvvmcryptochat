﻿namespace CryptoChatClient.Converters
{
    public class HoverBrightnessConverter : ColorBrightnessConverter
    {
        public HoverBrightnessConverter()
        {
            BrightnessFactor = 1.2;
        }
    }
}
