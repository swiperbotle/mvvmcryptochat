﻿namespace CryptoChatClient.Converters
{
    public class PressedBrightnessConverter : ColorBrightnessConverter
    {
        public PressedBrightnessConverter()
        {
            BrightnessFactor = 1.5;
        }
    }
}