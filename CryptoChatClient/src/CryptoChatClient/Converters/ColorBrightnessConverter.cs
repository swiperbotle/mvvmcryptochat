﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CryptoChatClient.Converters
{
    public class ColorBrightnessConverter : IValueConverter
    {
        public double BrightnessFactor { get; set; } = 1.2;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SolidColorBrush brush)
            {
                double factor = BrightnessFactor;
                var color = brush.Color;

                var r = (byte)Math.Min(color.R * factor, 255);
                var g = (byte)Math.Min(color.G * factor, 255);
                var b = (byte)Math.Min(color.B * factor, 255);
                return new SolidColorBrush(Color.FromRgb(r, g, b));
            }
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
