﻿using AutoMapper;
using CryptoChatClient.Models;
using CryptoChatClient.ORM.DTOs;


namespace CryptoChatClient.Mappers
{
    public class UserToUserMapper : Profile
    {
        public UserToUserMapper()
        {
            CreateMap<CurrentUserModel, UserDTO>();
            CreateMap<UserDTO, CurrentUserModel>();
        }
    }
}
