﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using CryptoChatClient.Core;
using CryptoChatClient.Mappers;
using CryptoChatClient.ORM.Contexts;
using CryptoChatClient.ORM.HostBuilders;
using CryptoChatClient.Repositories;
using CryptoChatClient.Services;
using CryptoChatClient.ViewModels;
using CryptoChatClient.Views;
using System;
using System.Windows;
namespace CryptoChatClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IHost? _host;
        public App()
        {
            _host = Host.CreateDefaultBuilder()
               .AddDbContext()
               .ConfigureServices((context, services) =>
               {
                   services.AddAutoMapper(typeof(UserToUserMapper));
                   services.AddSingleton<SplashScreenViewModel>();
                   services.AddSingleton<SignUpViewModel>();
                   services.AddSingleton<MainViewModel>();
                   services.AddTransient<HomeViewModel>();
                   services.AddSingleton<SignInViewModel>();
                   services.AddSingleton<INavigationService, NavigationService>();
                   services.AddSingleton<IListenerService, ListenerService>();
                   services.AddSingleton<CryptoChatRepository, CryptoChatRepository>();
                   // ViewModelFactory
                   services.AddSingleton<Func<Type, ViewModelBase>>(provider => viewModelType => (ViewModelBase)provider.GetRequiredService(viewModelType));
                   services.AddSingleton<MainWindow>(provider => new MainWindow
                   {
                       DataContext = provider.GetRequiredService<MainViewModel>()
                   });
                   services.AddSingleton<SplashScreenWindow>(provider => new SplashScreenWindow
                   {
                       DataContext = provider.GetRequiredService<SplashScreenViewModel>()
                   });
               })
               .Build();
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            _host?.Start();
            CryptoChatDbContextFactory cryptoChatDbContextFactory =
                _host.Services.GetRequiredService<CryptoChatDbContextFactory>();
            //using (CryptoChatDbContext context = cryptoChatDbContextFactory.CreateDbContext())
            //{
            //    context.Database.Migrate();
            //}
            var splashScreenWindow = _host.Services.GetRequiredService<SplashScreenWindow>();
            MainWindow = _host.Services.GetRequiredService<MainWindow>();
            splashScreenWindow.Closed += (sender, e) =>
            {
                MainWindow.Show();
            };
            splashScreenWindow.Show();
            base.OnStartup(e);
        }
        protected override void OnExit(ExitEventArgs e)
        {
            _host?.StopAsync();
            _host?.Dispose();
            base.OnExit(e);
        }
    }
}
